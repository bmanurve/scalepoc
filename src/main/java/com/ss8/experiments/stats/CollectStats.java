package com.ss8.experiments.stats;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.concurrent.TimeUnit;

public class CollectStats {
	
	public static void main(String args[]) {
        String s;
        Process p;
        try {
            /*p = Runtime.getRuntime().exec("pgrep -f start.jar");
            BufferedReader br = new BufferedReader(
                new InputStreamReader(p.getInputStream()));
            while ((s = br.readLine()) != null) {
            	System.out.println("line: " + s);
            }
            p.waitFor();
            System.out.println ("exit: " + p.exitValue());
            p.destroy();*/
            //Process p2 = Runtime.getRuntime().exec("sar -P ALL 1 5 | grep Average");
        	String[] cmd = {
        			"/bin/sh",
        			"-c",
        			"sar -P ALL 1 5 | grep Average"
        			};
        	Process p2 = Runtime.getRuntime().exec(cmd);
            p2.waitFor(10000, TimeUnit.MILLISECONDS);
            BufferedReader br2 = new BufferedReader(
                    new InputStreamReader(p2.getInputStream()));
            while ((s = br2.readLine()) != null) {
            	System.out.println("line: " + s);
            }
            //Runtime.getRuntime().
            System.out.println ("exit: " + p2.exitValue());
            p2.destroy();
        } catch (Exception e) {
        	e.printStackTrace();
        }
    }
}
