package com.ss8.experiments;

import com.ss8.experiments.indexer.QueueLoader;

public class ElasticSearchLoadGenerator {
	public static void main(String[] args) {
		try {
			QueueLoader loader = new QueueLoader(false);
			loader.startIngest();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
