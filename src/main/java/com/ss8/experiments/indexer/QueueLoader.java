package com.ss8.experiments.indexer;

import java.util.Date;
import java.util.UUID;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

import org.apache.solr.client.solrj.impl.CloudSolrClient;

import com.fasterxml.uuid.Generators;

public class QueueLoader {
	
	public static AtomicLong atomicLong;
	
	static {
		CloudSolrClient server = null;
		try {
			/*server = SolrUtils.getSolrServer();
			SolrQuery query = new SolrQuery();
			query.setQuery("*:*");
			query.addSort("idSort", ORDER.desc);
			query.addField("id");
			QueryResponse response = server.query(query);
			SolrDocumentList docList = response.getResults();*/
			long id = 1;
			/*if (docList != null && docList.size() > 0) {
				id = (long) docList.get(0).getFieldValue("id");
			}*/
			atomicLong = new AtomicLong(id);
			System.out.println("Id -----------> " + id);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private BlockingQueue<Document> ingestQueue;
	
	private BlockingQueue<Document> indexerQueue;
	
	private Thread receiverThread;
	
	private Thread senderThread;
	
	private boolean exit = false;
	
	private ExecutorService ingestExecutor = Executors.newFixedThreadPool(5);
	
	private ScheduledExecutorService cassandraDatabaseExecutor = Executors.newScheduledThreadPool(10);
	
	private ScheduledExecutorService indexExecutor = Executors.newScheduledThreadPool(5);
	
	private ScheduledExecutorService queriesExecutor = Executors.newScheduledThreadPool(100);
	
	private ScheduledExecutorService mysqlExecutor = Executors.newScheduledThreadPool(10);
	
	private boolean isSolr = false;
	
	public QueueLoader(final boolean isSolr) {
		this.isSolr = isSolr;
	}

	public void startIngest() {
		/*try {
			Thread.sleep(120000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			Thread.sleep(180000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		this.receiverThread = new Thread(createReceiver());
		this.receiverThread.start();
		
		this.indexerQueue = new ArrayBlockingQueue<Document>(100000);
		for (int i = 0; i < 50; i++) {
			if (this.isSolr) {
				indexExecutor.scheduleAtFixedRate(new SolrCassandraIndexer(this.indexerQueue), i + 100, 1000, TimeUnit.MILLISECONDS);
			} else {
				indexExecutor.scheduleAtFixedRate(new ElasticSearchIndexer(this.indexerQueue), i + 100, 1000, TimeUnit.MILLISECONDS);
			}
		}
	}
	
	public static long getNextId() {
		UUID uuid = Generators.timeBasedGenerator().generate();
		return uuid.timestamp();
	}
	
	private Runnable createReceiver() {
		Runnable receiver = new Runnable() {

			@Override
			public void run() {
				while (!exit) {
					// Create document messages to ingest.
					for (int i = 1; i <= 50; i++) {
			            Runnable ingester = new IngesterThread(i, indexerQueue, false);
			            ingestExecutor.execute(ingester);
			            try {
							Thread.sleep(15);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
			          }
					try {
						Thread.sleep(500);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					
					if (indexerQueue.size() > 1000) {
						System.out.println(new Date() + " QueueLoader :: Queue size execeeds 1000 and the current queue size is - " + indexerQueue.size());
					}
				}
			}
		};
		return receiver;
	}
}
