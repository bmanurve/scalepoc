package com.ss8.experiments.indexer;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonCreator;

public class FileXfer extends Document {
	
	private String password;
	
	private String mode;
	
	private int method;
	
	private int code;

	@JsonCreator
	public FileXfer(long id, String name, int type, String provider,
			String title, Date date, String contentUrl, String contentEncoding,
			String contentType, String serverIP, String clientIP,
			int relevance, String correlation, int capture, int clientPort,
			int serverPort, int timezoneOffset, String threats,
			String enrichedData, Date trashedTime, String userAgent,
			int contentSize, String transport, int direction, String subType,
			Date ingestDate, String globalIdentificationNumber,
			String versionNumber, String geofence, String appSessionID,
			long intercept, String interceptName, String interceptDesc,
			long soi, String soiName, String subject, String locations,
			String tap, String genre, String lang, String password,
			String mode, int method, int code) {
		super(id, name, type, provider, title, date, contentUrl,
				contentEncoding, contentType, serverIP, clientIP, relevance,
				correlation, capture, clientPort, serverPort, timezoneOffset,
				threats, enrichedData, trashedTime, userAgent, contentSize,
				transport, direction, subType, ingestDate,
				globalIdentificationNumber, versionNumber, geofence,
				appSessionID, intercept, interceptName, interceptDesc, soi,
				soiName, subject, locations, tap, genre, lang);
		this.password = password;
		this.mode = mode;
		this.method = method;
		this.code = code;
	}

	public FileXfer(long id) {
		super(id);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the mode
	 */
	public String getMode() {
		return mode;
	}

	/**
	 * @param mode the mode to set
	 */
	public void setMode(String mode) {
		this.mode = mode;
	}

	/**
	 * @return the method
	 */
	public int getMethod() {
		return method;
	}

	/**
	 * @param method the method to set
	 */
	public void setMethod(int method) {
		this.method = method;
	}

	/**
	 * @return the code
	 */
	public int getCode() {
		return code;
	}

	/**
	 * @param code the code to set
	 */
	public void setCode(int code) {
		this.code = code;
	}
}
