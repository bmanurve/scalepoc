package com.ss8.experiments.indexer;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonCreator;

public class Certificate extends Document {
	
	private String sessionId;
	
	private String serverName;
	
	private String version;
	
	private String certificateMD5;
	
	private String serialNumber;
	
	private Date validityStart;
	
	private Date validityEnd;
	
	private String commonName;
	
	private String alternateNames;
	
	private String issuer;
	
	@JsonCreator
	public Certificate(long id, String name, int type, String provider,
			String title, Date date, String contentUrl, String contentEncoding,
			String contentType, String serverIP, String clientIP,
			int relevance, String correlation, int capture, int clientPort,
			int serverPort, int timezoneOffset, String threats,
			String enrichedData, Date trashedTime, String userAgent,
			int contentSize, String transport, int direction, String subType,
			Date ingestDate, String globalIdentificationNumber,
			String versionNumber, String geofence, String appSessionID,
			long intercept, String interceptName, String interceptDesc,
			long soi, String soiName, String subject, String locations,
			String tap, String genre, String lang, String sessionId,
			String serverName, String version, String certificateMD5,
			String serialNumber, Date validityStart, Date validityEnd,
			String commonName, String alternateNames, String issuer) {
		super(id, name, type, provider, title, date, contentUrl,
				contentEncoding, contentType, serverIP, clientIP, relevance,
				correlation, capture, clientPort, serverPort, timezoneOffset,
				threats, enrichedData, trashedTime, userAgent, contentSize,
				transport, direction, subType, ingestDate,
				globalIdentificationNumber, versionNumber, geofence,
				appSessionID, intercept, interceptName, interceptDesc, soi,
				soiName, subject, locations, tap, genre, lang);
		this.sessionId = sessionId;
		this.serverName = serverName;
		this.version = version;
		this.certificateMD5 = certificateMD5;
		this.serialNumber = serialNumber;
		this.validityStart = validityStart;
		this.validityEnd = validityEnd;
		this.commonName = commonName;
		this.alternateNames = alternateNames;
		this.issuer = issuer;
	}

	public Certificate(long id) {
		super(id);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the sessionId
	 */
	public String getSessionId() {
		return sessionId;
	}

	/**
	 * @param sessionId the sessionId to set
	 */
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	/**
	 * @return the serverName
	 */
	public String getServerName() {
		return serverName;
	}

	/**
	 * @param serverName the serverName to set
	 */
	public void setServerName(String serverName) {
		this.serverName = serverName;
	}

	/**
	 * @return the version
	 */
	public String getVersion() {
		return version;
	}

	/**
	 * @param version the version to set
	 */
	public void setVersion(String version) {
		this.version = version;
	}

	/**
	 * @return the certificateMD5
	 */
	public String getCertificateMD5() {
		return certificateMD5;
	}

	/**
	 * @param certificateMD5 the certificateMD5 to set
	 */
	public void setCertificateMD5(String certificateMD5) {
		this.certificateMD5 = certificateMD5;
	}

	/**
	 * @return the serialNumber
	 */
	public String getSerialNumber() {
		return serialNumber;
	}

	/**
	 * @param serialNumber the serialNumber to set
	 */
	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	/**
	 * @return the validityStart
	 */
	public Date getValidityStart() {
		return validityStart;
	}

	/**
	 * @param validityStart the validityStart to set
	 */
	public void setValidityStart(Date validityStart) {
		this.validityStart = validityStart;
	}

	/**
	 * @return the validityEnd
	 */
	public Date getValidityEnd() {
		return validityEnd;
	}

	/**
	 * @param validityEnd the validityEnd to set
	 */
	public void setValidityEnd(Date validityEnd) {
		this.validityEnd = validityEnd;
	}

	/**
	 * @return the commonName
	 */
	public String getCommonName() {
		return commonName;
	}

	/**
	 * @param commonName the commonName to set
	 */
	public void setCommonName(String commonName) {
		this.commonName = commonName;
	}

	/**
	 * @return the alternateNames
	 */
	public String getAlternateNames() {
		return alternateNames;
	}

	/**
	 * @param alternateNames the alternateNames to set
	 */
	public void setAlternateNames(String alternateNames) {
		this.alternateNames = alternateNames;
	}

	/**
	 * @return the issuer
	 */
	public String getIssuer() {
		return issuer;
	}

	/**
	 * @param issuer the issuer to set
	 */
	public void setIssuer(String issuer) {
		this.issuer = issuer;
	}
}
