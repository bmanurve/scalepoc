package com.ss8.experiments.indexer;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonCreator;

public class Inbox extends Document {
	
	private String msgSenderLogin;
	
	private String msgSenderAlias;
	
	private String msgSubject;
	
	private Date msgDate;
	
	private String msgReceiverLogin;
	
	private String msgReceiverAlias;
	
	private String folderName;
	
	@JsonCreator
	public Inbox(long id, String name, int type, String provider, String title,
			Date date, String contentUrl, String contentEncoding,
			String contentType, String serverIP, String clientIP,
			int relevance, String correlation, int capture, int clientPort,
			int serverPort, int timezoneOffset, String threats,
			String enrichedData, Date trashedTime, String userAgent,
			int contentSize, String transport, int direction, String subType,
			Date ingestDate, String globalIdentificationNumber,
			String versionNumber, String geofence, String appSessionID,
			long intercept, String interceptName, String interceptDesc,
			long soi, String soiName, String subject, String locations,
			String tap, String genre, String lang, String msgSenderLogin,
			String msgSenderAlias, String msgSubject, Date msgDate,
			String msgReceiverLogin, String msgReceiverAlias, String folderName) {
		super(id, name, type, provider, title, date, contentUrl,
				contentEncoding, contentType, serverIP, clientIP, relevance,
				correlation, capture, clientPort, serverPort, timezoneOffset,
				threats, enrichedData, trashedTime, userAgent, contentSize,
				transport, direction, subType, ingestDate,
				globalIdentificationNumber, versionNumber, geofence,
				appSessionID, intercept, interceptName, interceptDesc, soi,
				soiName, subject, locations, tap, genre, lang);
		this.msgSenderLogin = msgSenderLogin;
		this.msgSenderAlias = msgSenderAlias;
		this.msgSubject = msgSubject;
		this.msgDate = msgDate;
		this.msgReceiverLogin = msgReceiverLogin;
		this.msgReceiverAlias = msgReceiverAlias;
		this.folderName = folderName;
	}

	public Inbox(long id) {
		super(id);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the msgSenderLogin
	 */
	public String getMsgSenderLogin() {
		return msgSenderLogin;
	}

	/**
	 * @param msgSenderLogin the msgSenderLogin to set
	 */
	public void setMsgSenderLogin(String msgSenderLogin) {
		this.msgSenderLogin = msgSenderLogin;
	}

	/**
	 * @return the msgSenderAlias
	 */
	public String getMsgSenderAlias() {
		return msgSenderAlias;
	}

	/**
	 * @param msgSenderAlias the msgSenderAlias to set
	 */
	public void setMsgSenderAlias(String msgSenderAlias) {
		this.msgSenderAlias = msgSenderAlias;
	}

	/**
	 * @return the msgSubject
	 */
	public String getMsgSubject() {
		return msgSubject;
	}

	/**
	 * @param msgSubject the msgSubject to set
	 */
	public void setMsgSubject(String msgSubject) {
		this.msgSubject = msgSubject;
	}

	/**
	 * @return the msgDate
	 */
	public Date getMsgDate() {
		return msgDate;
	}

	/**
	 * @param msgDate the msgDate to set
	 */
	public void setMsgDate(Date msgDate) {
		this.msgDate = msgDate;
	}

	/**
	 * @return the msgReceiverAlias
	 */
	public String getMsgReceiverAlias() {
		return msgReceiverAlias;
	}

	/**
	 * @param msgReceiverAlias the msgReceiverAlias to set
	 */
	public void setMsgReceiverAlias(String msgReceiverAlias) {
		this.msgReceiverAlias = msgReceiverAlias;
	}

	/**
	 * @return the msgReceiverLogin
	 */
	public String getMsgReceiverLogin() {
		return msgReceiverLogin;
	}

	/**
	 * @param msgReceiverLogin the msgReceiverLogin to set
	 */
	public void setMsgReceiverLogin(String msgReceiverLogin) {
		this.msgReceiverLogin = msgReceiverLogin;
	}

	/**
	 * @return the folderName
	 */
	public String getFolderName() {
		return folderName;
	}

	/**
	 * @param folderName the folderName to set
	 */
	public void setFolderName(String folderName) {
		this.folderName = folderName;
	}
}
