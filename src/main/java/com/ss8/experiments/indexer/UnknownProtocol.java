package com.ss8.experiments.indexer;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonCreator;

public class UnknownProtocol extends Document {
	
	private String protocolStack;
	
	private int pps;
	
	private int avgPktSizeForSec;
	
	private int totalChunks;

	@JsonCreator
	public UnknownProtocol(long id, String name, int type, String provider,
			String title, Date date, String contentUrl, String contentEncoding,
			String contentType, String serverIP, String clientIP,
			int relevance, String correlation, int capture, int clientPort,
			int serverPort, int timezoneOffset, String threats,
			String enrichedData, Date trashedTime, String userAgent,
			int contentSize, String transport, int direction, String subType,
			Date ingestDate, String globalIdentificationNumber,
			String versionNumber, String geofence, String appSessionID,
			long intercept, String interceptName, String interceptDesc,
			long soi, String soiName, String subject, String locations,
			String tap, String genre, String lang, String protocolStack,
			int pps, int avgPktSizeForSec, int totalChunks) {
		super(id, name, type, provider, title, date, contentUrl,
				contentEncoding, contentType, serverIP, clientIP, relevance,
				correlation, capture, clientPort, serverPort, timezoneOffset,
				threats, enrichedData, trashedTime, userAgent, contentSize,
				transport, direction, subType, ingestDate,
				globalIdentificationNumber, versionNumber, geofence,
				appSessionID, intercept, interceptName, interceptDesc, soi,
				soiName, subject, locations, tap, genre, lang);
		this.protocolStack = protocolStack;
		this.pps = pps;
		this.avgPktSizeForSec = avgPktSizeForSec;
		this.totalChunks = totalChunks;
	}

	public UnknownProtocol(long id) {
		super(id);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the protocolStack
	 */
	public String getProtocolStack() {
		return protocolStack;
	}

	/**
	 * @param protocolStack the protocolStack to set
	 */
	public void setProtocolStack(String protocolStack) {
		this.protocolStack = protocolStack;
	}

	/**
	 * @return the pps
	 */
	public int getPps() {
		return pps;
	}

	/**
	 * @param pps the pps to set
	 */
	public void setPps(int pps) {
		this.pps = pps;
	}

	/**
	 * @return the avgPktSizeForSec
	 */
	public int getAvgPktSizeForSec() {
		return avgPktSizeForSec;
	}

	/**
	 * @param avgPktSizeForSec the avgPktSizeForSec to set
	 */
	public void setAvgPktSizeForSec(int avgPktSizeForSec) {
		this.avgPktSizeForSec = avgPktSizeForSec;
	}

	/**
	 * @return the totalChunks
	 */
	public int getTotalChunks() {
		return totalChunks;
	}

	/**
	 * @param totalChunks the totalChunks to set
	 */
	public void setTotalChunks(int totalChunks) {
		this.totalChunks = totalChunks;
	}
}
