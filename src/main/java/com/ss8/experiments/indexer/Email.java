package com.ss8.experiments.indexer;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonCreator;

public class Email extends Document {
	
	private String messageId;
	
	private String action;
	
	private String to;
	
	private String cc;
	
	private String bcc;
	
	private String replyto;
	
	private int draft;
	
	private Date sent;
	
	private long duplicateId;
	
	private String emailHash;
	
	private int duplicateCount;
	
	private int attachmentCount;
	
	private String subjectEncoding;
	
	private String rcptTo;

	@JsonCreator
	public Email(long id, String name, int type, String provider, String title,
			Date date, String contentUrl, String contentEncoding,
			String contentType, String serverIP, String clientIP,
			int relevance, String correlation, int capture, int clientPort,
			int serverPort, int timezoneOffset, String threats,
			String enrichedData, Date trashedTime, String userAgent,
			int contentSize, String transport, int direction, String subType,
			Date ingestDate, String globalIdentificationNumber,
			String versionNumber, String geofence, String appSessionID,
			long intercept, String interceptName, String interceptDesc,
			long soi, String soiName, String subject, String locations,
			String tap, String genre, String lang, String messageId,
			String action, String to, String cc, String bcc, String replyto,
			int draft, Date sent, long duplicateId, String emailHash,
			int duplicateCount, int attachmentCount, String subjectEncoding,
			String rcptTo) {
		super(id, name, type, provider, title, date, contentUrl,
				contentEncoding, contentType, serverIP, clientIP, relevance,
				correlation, capture, clientPort, serverPort, timezoneOffset,
				threats, enrichedData, trashedTime, userAgent, contentSize,
				transport, direction, subType, ingestDate,
				globalIdentificationNumber, versionNumber, geofence,
				appSessionID, intercept, interceptName, interceptDesc, soi,
				soiName, subject, locations, tap, genre, lang);
		this.messageId = messageId;
		this.action = action;
		this.to = to;
		this.cc = cc;
		this.bcc = bcc;
		this.replyto = replyto;
		this.draft = draft;
		this.sent = sent;
		this.duplicateId = duplicateId;
		this.emailHash = emailHash;
		this.duplicateCount = duplicateCount;
		this.attachmentCount = attachmentCount;
		this.subjectEncoding = subjectEncoding;
		this.rcptTo = rcptTo;
	}

	public Email(long id) {
		super(id);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the messageId
	 */
	public String getMessageId() {
		return messageId;
	}

	/**
	 * @param messageId the messageId to set
	 */
	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}

	/**
	 * @return the action
	 */
	public String getAction() {
		return action;
	}

	/**
	 * @param action the action to set
	 */
	public void setAction(String action) {
		this.action = action;
	}

	/**
	 * @return the to
	 */
	public String getTo() {
		return to;
	}

	/**
	 * @param to the to to set
	 */
	public void setTo(String to) {
		this.to = to;
	}

	/**
	 * @return the cc
	 */
	public String getCc() {
		return cc;
	}

	/**
	 * @param cc the cc to set
	 */
	public void setCc(String cc) {
		this.cc = cc;
	}

	/**
	 * @return the bcc
	 */
	public String getBcc() {
		return bcc;
	}

	/**
	 * @param bcc the bcc to set
	 */
	public void setBcc(String bcc) {
		this.bcc = bcc;
	}

	/**
	 * @return the replyto
	 */
	public String getReplyto() {
		return replyto;
	}

	/**
	 * @param replyto the replyto to set
	 */
	public void setReplyto(String replyto) {
		this.replyto = replyto;
	}

	/**
	 * @return the draft
	 */
	public int getDraft() {
		return draft;
	}

	/**
	 * @param draft the draft to set
	 */
	public void setDraft(int draft) {
		this.draft = draft;
	}

	/**
	 * @return the sent
	 */
	public Date getSent() {
		return sent;
	}

	/**
	 * @param sent the sent to set
	 */
	public void setSent(Date sent) {
		this.sent = sent;
	}

	/**
	 * @return the duplicateId
	 */
	public long getDuplicateId() {
		return duplicateId;
	}

	/**
	 * @param duplicateId the duplicateId to set
	 */
	public void setDuplicateId(long duplicateId) {
		this.duplicateId = duplicateId;
	}

	/**
	 * @return the emailHash
	 */
	public String getEmailHash() {
		return emailHash;
	}

	/**
	 * @param emailHash the emailHash to set
	 */
	public void setEmailHash(String emailHash) {
		this.emailHash = emailHash;
	}

	/**
	 * @return the duplicateCount
	 */
	public int getDuplicateCount() {
		return duplicateCount;
	}

	/**
	 * @param duplicateCount the duplicateCount to set
	 */
	public void setDuplicateCount(int duplicateCount) {
		this.duplicateCount = duplicateCount;
	}

	/**
	 * @return the attachmentCount
	 */
	public int getAttachmentCount() {
		return attachmentCount;
	}

	/**
	 * @param attachmentCount the attachmentCount to set
	 */
	public void setAttachmentCount(int attachmentCount) {
		this.attachmentCount = attachmentCount;
	}

	/**
	 * @return the subjectEncoding
	 */
	public String getSubjectEncoding() {
		return subjectEncoding;
	}

	/**
	 * @param subjectEncoding the subjectEncoding to set
	 */
	public void setSubjectEncoding(String subjectEncoding) {
		this.subjectEncoding = subjectEncoding;
	}

	/**
	 * @return the rcptTo
	 */
	public String getRcptTo() {
		return rcptTo;
	}

	/**
	 * @param rcptTo the rcptTo to set
	 */
	public void setRcptTo(String rcptTo) {
		this.rcptTo = rcptTo;
	}

}
