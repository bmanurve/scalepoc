package com.ss8.experiments.indexer;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.concurrent.BlockingQueue;

import org.apache.solr.client.solrj.impl.CloudSolrClient;
import org.apache.solr.common.SolrInputDocument;

public class SolrCassandraIndexer implements Runnable {
	
	private BlockingQueue<Document> indexerQueue;
	
	public SolrCassandraIndexer(final BlockingQueue<Document> indexerQueue) {
		this.indexerQueue = indexerQueue;
	}

	@Override
	public void run() {
		// long startTime = System.currentTimeMillis();
		try {
			Collection<Document> documents = new LinkedList<Document>();
			indexerQueue.drainTo(documents);
			//System.out.println(Thread.currentThread().getName() + " Start - " + documents.size());
			CloudSolrClient server = SolrUtils.getSolrServer();
			ArrayList<SolrInputDocument> docs = new ArrayList<SolrInputDocument>();
			int batchSize = 1000;
			for (Document doc : documents) {
				SolrInputDocument solrInputDocument = new SolrInputDocument();
				// Common fields.
				solrInputDocument.addField(Document.FIELD_ID, doc.getId());
				solrInputDocument.addField(Document.FIELD_TYPE, doc.getType());
				solrInputDocument.addField(Document.FIELD_PROVIDER, doc.getProvider());
				solrInputDocument.addField(Document.FIELD_TITLE, doc.getTitle());
				solrInputDocument.addField(Document.FIELD_DATE, doc.getDate());
				solrInputDocument.addField(Document.FIELD_CONTENT_URL, doc.getContentUrl());
				solrInputDocument.addField(Document.FIELD_CONTENT_TYPE, doc.getContentType());
				solrInputDocument.addField(Document.FIELD_SERVER_IP, doc.getServerIP());
				solrInputDocument.addField(Document.FIELD_CLIENT_IP, doc.getClientIP());
				solrInputDocument.addField(Document.FIELD_RELEVANCE, doc.getRelevance());
				solrInputDocument.addField(Document.FIELD_CORRELATION, doc.getCorrelation());
				solrInputDocument.addField(Document.FIELD_CLIENT_PORT, doc.getClientPort());
				solrInputDocument.addField(Document.FIELD_SERVER_PORT, doc.getServerPort());
				solrInputDocument.addField(Document.FIELD_THREATS, doc.getThreats());
				solrInputDocument.addField(Document.FIELD_TRASHED_TIME, doc.getTrashedTime());
				solrInputDocument.addField(Document.FIELD_USER_AGENT, doc.getUserAgent());
				solrInputDocument.addField(Document.FIELD_SIZE, doc.getContentSize());
				solrInputDocument.addField(Document.FIELD_DIRECTION, doc.getDirection());
				solrInputDocument.addField(Document.FIELD_SUB_TYPE, doc.getSubType());
				solrInputDocument.addField(Document.FIELD_INGEST_DATE, doc.getIngestDate());
				solrInputDocument.addField(Document.FIELD_GLOBAL_IDENTIFICATION_NUMBER, doc.getGlobalIdentificationNumber());
				solrInputDocument.addField(Document.FIELD_VERSION_NUMBER, doc.getVersionNumber());
				solrInputDocument.addField(Document.FIELD_GEOFENCE, doc.getGeofence());
				solrInputDocument.addField(Document.FIELD_APPSESSIONID, doc.getAppSessionID());
				solrInputDocument.addField(Document.FIELD_INTERCEPT, doc.getIntercept());
				solrInputDocument.addField(Document.FIELD_INTERCEPT_NAME, doc.getInterceptName());
				solrInputDocument.addField(Document.FIELD_INTERCEPT_DESCRIPTION, doc.getInterceptDesc());
				solrInputDocument.addField(Document.FIELD_SOI, doc.getSoi());
				solrInputDocument.addField(Document.FIELD_SOI_NAME, doc.getSoiName());
				solrInputDocument.addField(Document.FIELD_SUBJECT, doc.getSubject());
				solrInputDocument.addField(Document.FIELD_LOCATION, doc.getLocations());
				solrInputDocument.addField(Document.FIELD_TAP, doc.getTap());
				solrInputDocument.addField(Document.FIELD_GENRE, doc.getGenre());
				solrInputDocument.addField(Document.FIELD_LANG, doc.getLang());
				solrInputDocument.addField(Document.FIELD_CONTENT, doc.getContent());
				solrInputDocument.addField(Document.FIELD_SENDER, doc.getSender());
				solrInputDocument.addField(Document.FIELD_RECIPIENT, doc.getRecipient());
				solrInputDocument.addField(Document.FIELD_DOCUMENT_STATUS, 0);
				solrInputDocument.addField("idSort", doc.getId());
				solrInputDocument.addField("dateSort", doc.getDate());
				solrInputDocument.addField("ingestDateSort", doc.getIngestDate());
			
				if (Document.TYPE_WEB == doc.getType()) {
					WebPage webPage = (WebPage) doc;
					solrInputDocument.addField("responseCode", webPage.getResponseCode());
					solrInputDocument.addField("httpReferer", webPage.getHttpReferer());
					solrInputDocument.addField("webSearchQueryText", webPage.getWebSearchQueryText());
					solrInputDocument.addField("webSearchQueryType", webPage.getWebSearchQueryType());
					solrInputDocument.addField("webMapAccountLogin", webPage.getWebMapAccountLogin());
					solrInputDocument.addField("webMapAccountName", webPage.getWebMapAccountName());
					solrInputDocument.addField("webMapAccountLocation", webPage.getWebMapAccountLocation());
					solrInputDocument.addField("webMapAccountCity", webPage.getWebMapAccountCity());
					solrInputDocument.addField("webMapAccountState", webPage.getWebMapAccountState());
					solrInputDocument.addField("webMapAccountPostal", webPage.getWebMapAccountPostal());
					solrInputDocument.addField("webMapAccountCountry", webPage.getWebMapAccountCountry());
					solrInputDocument.addField("webMapAccountAddress", webPage.getWebMapAccountAddress());
					solrInputDocument.addField("webMapLocationQueryText", webPage.getWebMapLocationQueryText());
					solrInputDocument.addField("webMapLocationWaypointType", webPage.getWebMapLocationWaypointType());
					solrInputDocument.addField("webMapLocationStreet", webPage.getWebMapLocationStreet());
					solrInputDocument.addField("webMapLocationCity", webPage.getWebMapLocationCity());
					solrInputDocument.addField("webMapLocationState", webPage.getWebMapLocationState());
					solrInputDocument.addField("webMapLocationCountry", webPage.getWebMapLocationCountry());
					solrInputDocument.addField("webMapLocationPostal", webPage.getWebMapLocationPostal());
					solrInputDocument.addField("webMapLocationAddress", webPage.getWebMapLocationAddress());
					solrInputDocument.addField("cookie", webPage.getCookie());
					solrInputDocument.addField("reqCookie", webPage.getReqCookie());
					solrInputDocument.addField("respCookie", webPage.getRespCookie());
					solrInputDocument.addField("server", webPage.getServer());
				} else if (Document.TYPE_IRI == doc.getType()) {
					Iri iri = (Iri) doc;
					solrInputDocument.addField("iRIMsisdn", iri.getiRIMsisdn());
					solrInputDocument.addField("iRIImsi", iri.getiRIImsi());
					solrInputDocument.addField("iRIImei", iri.getiRIImei());
				} else if (Document.TYPE_CALL == doc.getType()) {
					Call call = (Call) doc;
					solrInputDocument.addField("imei", call.getImei());
					solrInputDocument.addField("imsi", call.getImsi());
					solrInputDocument.addField("ringDuration", call.getRingDuration());
					solrInputDocument.addField("teluriCallee", call.getTeluriCallee());
					solrInputDocument.addField("sipurlCallee", call.getSipurlCallee());
					solrInputDocument.addField("teluriForwarded", call.getTeluriForwarded());
					solrInputDocument.addField("sipurlForwarded", call.getSipurlForwarded());
					solrInputDocument.addField("msisdnCallee", call.getMsisdnCallee());
					solrInputDocument.addField("imsiCallee", call.getImsiCallee());
					solrInputDocument.addField("imeiCallee", call.getImeiCallee());
					solrInputDocument.addField("teluriForwarded", call.getTeluriForwarded());
					solrInputDocument.addField("msisdnForwarded", call.getMsisdnForwarded());
					solrInputDocument.addField("imsiForwarded", call.getImsiForwarded());
					solrInputDocument.addField("imeiForwarded", call.getImeiForwarded());
					solrInputDocument.addField("callStatus", call.getCallStatus());
					solrInputDocument.addField("dialed", call.getDialed());
					solrInputDocument.addField("msisdn", call.getMsisdn());
					solrInputDocument.addField("callType", call.getCallType());
					solrInputDocument.addField("starttime", call.getStartTime());
					solrInputDocument.addField("endtime", call.getEndTime());
				} else if (Document.TYPE_UNKNOWN == doc.getType()) {
					UnknownProtocol unknownProtocol = (UnknownProtocol) doc;
					solrInputDocument.addField("protocolStack", unknownProtocol.getProtocolStack());
				}
				
				docs.add(solrInputDocument);
				if (docs.size() == batchSize) {
					server.add(docs);
					docs.clear();
				}
			}
			if (docs.size() > 0) {
				server.add(docs);
				docs.clear();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		//System.out.println(Thread.currentThread().getName() + " End - " + (System.currentTimeMillis() - startTime));
	}
}
