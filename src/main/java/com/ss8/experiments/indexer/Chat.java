package com.ss8.experiments.indexer;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonCreator;

public class Chat extends Document {
	
	private String loginAlias;
	
	private String loginNickname;
	
	private String loginProfile;
	
	private String fileName;
	
	private String chatProto;
	
	private int sessionDuration;
	
	private String msgText;
	
	private int sessionStartTime;
	
	private int sessionEndTime;
	
	private String senderNickname;
	
	private String receiverNickname;
	
	private String msgEncoding;
	
	private String senderUserAgent;
	
	private String receiverUserAgent;
	
	private String fileSender;
	
	private int fileSize;
	
	private Date voiceStartTime;
	
	private Date voiceEndTime;
	
	private String voiceSender;
	
	private String voiceReceiver;
	
	private String voiceSenderUserAgent;
	
	private String voiceReceiverUserAgent;
	
	private int voiceDuration;
	
	private Date videoStartTime;
	
	private Date videoEndTime;
	
	private String videoSender;
	
	private String videoReceiver;
	
	private String videoSenderUserAgent;
	
	private String videoReceiverUserAgent;
	
	private int videoDuration;

	@JsonCreator
	public Chat(long id, String name, int type, String provider, String title,
			Date date, String contentUrl, String contentEncoding,
			String contentType, String serverIP, String clientIP,
			int relevance, String correlation, int capture, int clientPort,
			int serverPort, int timezoneOffset, String threats,
			String enrichedData, Date trashedTime, String userAgent,
			int contentSize, String transport, int direction, String subType,
			Date ingestDate, String globalIdentificationNumber,
			String versionNumber, String geofence, String appSessionID,
			long intercept, String interceptName, String interceptDesc,
			long soi, String soiName, String subject, String locations,
			String tap, String genre, String lang, String loginAlias,
			String loginNickname, String loginProfile, String fileName,
			String chatProto, int sessionDuration, String msgText,
			int sessionStartTime, int sessionEndTime, String senderNickname,
			String receiverNickname, String msgEncoding,
			String senderUserAgent, String receiverUserAgent,
			String fileSender, int fileSize, Date voiceStartTime,
			Date voiceEndTime, String voiceSender, String voiceReceiver,
			String voiceSenderUserAgent, String voiceReceiverUserAgent,
			int voiceDuration, Date videoStartTime, Date videoEndTime,
			String videoSender, String videoReceiver,
			String videoSenderUserAgent, String videoReceiverUserAgent,
			int videoDuration) {
		super(id, name, type, provider, title, date, contentUrl,
				contentEncoding, contentType, serverIP, clientIP, relevance,
				correlation, capture, clientPort, serverPort, timezoneOffset,
				threats, enrichedData, trashedTime, userAgent, contentSize,
				transport, direction, subType, ingestDate,
				globalIdentificationNumber, versionNumber, geofence,
				appSessionID, intercept, interceptName, interceptDesc, soi,
				soiName, subject, locations, tap, genre, lang);
		this.loginAlias = loginAlias;
		this.loginNickname = loginNickname;
		this.loginProfile = loginProfile;
		this.fileName = fileName;
		this.chatProto = chatProto;
		this.sessionDuration = sessionDuration;
		this.msgText = msgText;
		this.sessionStartTime = sessionStartTime;
		this.sessionEndTime = sessionEndTime;
		this.senderNickname = senderNickname;
		this.receiverNickname = receiverNickname;
		this.msgEncoding = msgEncoding;
		this.senderUserAgent = senderUserAgent;
		this.receiverUserAgent = receiverUserAgent;
		this.fileSender = fileSender;
		this.fileSize = fileSize;
		this.voiceStartTime = voiceStartTime;
		this.voiceEndTime = voiceEndTime;
		this.voiceSender = voiceSender;
		this.voiceReceiver = voiceReceiver;
		this.voiceSenderUserAgent = voiceSenderUserAgent;
		this.voiceReceiverUserAgent = voiceReceiverUserAgent;
		this.voiceDuration = voiceDuration;
		this.videoStartTime = videoStartTime;
		this.videoEndTime = videoEndTime;
		this.videoSender = videoSender;
		this.videoReceiver = videoReceiver;
		this.videoSenderUserAgent = videoSenderUserAgent;
		this.videoReceiverUserAgent = videoReceiverUserAgent;
		this.videoDuration = videoDuration;
	}



	public Chat(long id) {
		super(id);
		// TODO Auto-generated constructor stub
	}



	/**
	 * @return the loginAlias
	 */
	public String getLoginAlias() {
		return loginAlias;
	}



	/**
	 * @param loginAlias the loginAlias to set
	 */
	public void setLoginAlias(String loginAlias) {
		this.loginAlias = loginAlias;
	}



	/**
	 * @return the loginNickname
	 */
	public String getLoginNickname() {
		return loginNickname;
	}



	/**
	 * @param loginNickname the loginNickname to set
	 */
	public void setLoginNickname(String loginNickname) {
		this.loginNickname = loginNickname;
	}



	/**
	 * @return the loginProfile
	 */
	public String getLoginProfile() {
		return loginProfile;
	}



	/**
	 * @param loginProfile the loginProfile to set
	 */
	public void setLoginProfile(String loginProfile) {
		this.loginProfile = loginProfile;
	}



	/**
	 * @return the fileName
	 */
	public String getFileName() {
		return fileName;
	}



	/**
	 * @param fileName the fileName to set
	 */
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}



	/**
	 * @return the chatProto
	 */
	public String getChatProto() {
		return chatProto;
	}



	/**
	 * @param chatProto the chatProto to set
	 */
	public void setChatProto(String chatProto) {
		this.chatProto = chatProto;
	}



	/**
	 * @return the sessionDuration
	 */
	public int getSessionDuration() {
		return sessionDuration;
	}



	/**
	 * @param sessionDuration the sessionDuration to set
	 */
	public void setSessionDuration(int sessionDuration) {
		this.sessionDuration = sessionDuration;
	}



	/**
	 * @return the msgText
	 */
	public String getMsgText() {
		return msgText;
	}



	/**
	 * @param msgText the msgText to set
	 */
	public void setMsgText(String msgText) {
		this.msgText = msgText;
	}



	/**
	 * @return the sessionStartTime
	 */
	public int getSessionStartTime() {
		return sessionStartTime;
	}



	/**
	 * @param sessionStartTime the sessionStartTime to set
	 */
	public void setSessionStartTime(int sessionStartTime) {
		this.sessionStartTime = sessionStartTime;
	}



	/**
	 * @return the sessionEndTime
	 */
	public int getSessionEndTime() {
		return sessionEndTime;
	}



	/**
	 * @param sessionEndTime the sessionEndTime to set
	 */
	public void setSessionEndTime(int sessionEndTime) {
		this.sessionEndTime = sessionEndTime;
	}



	/**
	 * @return the senderNickname
	 */
	public String getSenderNickname() {
		return senderNickname;
	}



	/**
	 * @param senderNickname the senderNickname to set
	 */
	public void setSenderNickname(String senderNickname) {
		this.senderNickname = senderNickname;
	}



	/**
	 * @return the receiverNickname
	 */
	public String getReceiverNickname() {
		return receiverNickname;
	}



	/**
	 * @param receiverNickname the receiverNickname to set
	 */
	public void setReceiverNickname(String receiverNickname) {
		this.receiverNickname = receiverNickname;
	}



	/**
	 * @return the msgEncoding
	 */
	public String getMsgEncoding() {
		return msgEncoding;
	}



	/**
	 * @param msgEncoding the msgEncoding to set
	 */
	public void setMsgEncoding(String msgEncoding) {
		this.msgEncoding = msgEncoding;
	}



	/**
	 * @return the senderUserAgent
	 */
	public String getSenderUserAgent() {
		return senderUserAgent;
	}



	/**
	 * @param senderUserAgent the senderUserAgent to set
	 */
	public void setSenderUserAgent(String senderUserAgent) {
		this.senderUserAgent = senderUserAgent;
	}



	/**
	 * @return the receiverUserAgent
	 */
	public String getReceiverUserAgent() {
		return receiverUserAgent;
	}



	/**
	 * @param receiverUserAgent the receiverUserAgent to set
	 */
	public void setReceiverUserAgent(String receiverUserAgent) {
		this.receiverUserAgent = receiverUserAgent;
	}



	/**
	 * @return the fileSender
	 */
	public String getFileSender() {
		return fileSender;
	}



	/**
	 * @param fileSender the fileSender to set
	 */
	public void setFileSender(String fileSender) {
		this.fileSender = fileSender;
	}



	/**
	 * @return the fileSize
	 */
	public int getFileSize() {
		return fileSize;
	}



	/**
	 * @param fileSize the fileSize to set
	 */
	public void setFileSize(int fileSize) {
		this.fileSize = fileSize;
	}



	/**
	 * @return the voiceStartTime
	 */
	public Date getVoiceStartTime() {
		return voiceStartTime;
	}



	/**
	 * @param voiceStartTime the voiceStartTime to set
	 */
	public void setVoiceStartTime(Date voiceStartTime) {
		this.voiceStartTime = voiceStartTime;
	}



	/**
	 * @return the voiceEndTime
	 */
	public Date getVoiceEndTime() {
		return voiceEndTime;
	}



	/**
	 * @param voiceEndTime the voiceEndTime to set
	 */
	public void setVoiceEndTime(Date voiceEndTime) {
		this.voiceEndTime = voiceEndTime;
	}



	/**
	 * @return the voiceSender
	 */
	public String getVoiceSender() {
		return voiceSender;
	}



	/**
	 * @param voiceSender the voiceSender to set
	 */
	public void setVoiceSender(String voiceSender) {
		this.voiceSender = voiceSender;
	}



	/**
	 * @return the voiceReceiver
	 */
	public String getVoiceReceiver() {
		return voiceReceiver;
	}



	/**
	 * @param voiceReceiver the voiceReceiver to set
	 */
	public void setVoiceReceiver(String voiceReceiver) {
		this.voiceReceiver = voiceReceiver;
	}



	/**
	 * @return the voiceSenderUserAgent
	 */
	public String getVoiceSenderUserAgent() {
		return voiceSenderUserAgent;
	}



	/**
	 * @param voiceSenderUserAgent the voiceSenderUserAgent to set
	 */
	public void setVoiceSenderUserAgent(String voiceSenderUserAgent) {
		this.voiceSenderUserAgent = voiceSenderUserAgent;
	}



	/**
	 * @return the voiceReceiverUserAgent
	 */
	public String getVoiceReceiverUserAgent() {
		return voiceReceiverUserAgent;
	}



	/**
	 * @param voiceReceiverUserAgent the voiceReceiverUserAgent to set
	 */
	public void setVoiceReceiverUserAgent(String voiceReceiverUserAgent) {
		this.voiceReceiverUserAgent = voiceReceiverUserAgent;
	}



	/**
	 * @return the voiceDuration
	 */
	public int getVoiceDuration() {
		return voiceDuration;
	}



	/**
	 * @param voiceDuration the voiceDuration to set
	 */
	public void setVoiceDuration(int voiceDuration) {
		this.voiceDuration = voiceDuration;
	}



	/**
	 * @return the videoStartTime
	 */
	public Date getVideoStartTime() {
		return videoStartTime;
	}



	/**
	 * @param videoStartTime the videoStartTime to set
	 */
	public void setVideoStartTime(Date videoStartTime) {
		this.videoStartTime = videoStartTime;
	}



	/**
	 * @return the videoEndTime
	 */
	public Date getVideoEndTime() {
		return videoEndTime;
	}



	/**
	 * @param videoEndTime the videoEndTime to set
	 */
	public void setVideoEndTime(Date videoEndTime) {
		this.videoEndTime = videoEndTime;
	}



	/**
	 * @return the videoReceiver
	 */
	public String getVideoReceiver() {
		return videoReceiver;
	}



	/**
	 * @param videoReceiver the videoReceiver to set
	 */
	public void setVideoReceiver(String videoReceiver) {
		this.videoReceiver = videoReceiver;
	}



	/**
	 * @return the videoSender
	 */
	public String getVideoSender() {
		return videoSender;
	}



	/**
	 * @param videoSender the videoSender to set
	 */
	public void setVideoSender(String videoSender) {
		this.videoSender = videoSender;
	}



	/**
	 * @return the videoReceiverUserAgent
	 */
	public String getVideoReceiverUserAgent() {
		return videoReceiverUserAgent;
	}



	/**
	 * @param videoReceiverUserAgent the videoReceiverUserAgent to set
	 */
	public void setVideoReceiverUserAgent(String videoReceiverUserAgent) {
		this.videoReceiverUserAgent = videoReceiverUserAgent;
	}



	/**
	 * @return the videoSenderUserAgent
	 */
	public String getVideoSenderUserAgent() {
		return videoSenderUserAgent;
	}



	/**
	 * @param videoSenderUserAgent the videoSenderUserAgent to set
	 */
	public void setVideoSenderUserAgent(String videoSenderUserAgent) {
		this.videoSenderUserAgent = videoSenderUserAgent;
	}



	/**
	 * @return the videoDuration
	 */
	public int getVideoDuration() {
		return videoDuration;
	}



	/**
	 * @param videoDuration the videoDuration to set
	 */
	public void setVideoDuration(int videoDuration) {
		this.videoDuration = videoDuration;
	}

}
