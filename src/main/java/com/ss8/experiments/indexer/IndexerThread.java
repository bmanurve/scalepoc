package com.ss8.experiments.indexer;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedList;
import java.util.concurrent.BlockingQueue;

import org.apache.solr.client.solrj.impl.CloudSolrClient;
import org.apache.solr.common.SolrInputDocument;

public class IndexerThread implements Runnable {
	
	private BlockingQueue<Document> ingestQueue;
	
	public IndexerThread(final BlockingQueue<Document> ingestQueue) {
		this.ingestQueue = ingestQueue;
	}

	@Override
	public void run() {
		long startTime = System.currentTimeMillis();
		try {
			Collection<Document> documents = new LinkedList<Document>();
			ingestQueue.drainTo(documents);
			//System.out.println(Thread.currentThread().getName() + " Start - " + documents.size());
			CloudSolrClient server = SolrUtils.getSolrServer();
			ArrayList<SolrInputDocument> docs = new ArrayList<SolrInputDocument>();
			int batchSize = 500;
			for (Document doc : documents) {
				SolrInputDocument solrInputDocument = new SolrInputDocument();
				// Common fields.
				solrInputDocument.addField(Document.FIELD_ID, doc.getId());
				solrInputDocument.addField(Document.FIELD_NAME, doc.getName());
				solrInputDocument.addField(Document.FIELD_TYPE, doc.getType());
				solrInputDocument.addField(Document.FIELD_PROVIDER, doc.getProvider());
				solrInputDocument.addField(Document.FIELD_TITLE, doc.getTitle());
				solrInputDocument.addField(Document.FIELD_DATE, doc.getDate());
				solrInputDocument.addField(Document.FIELD_CONTENT_URL, doc.getContentUrl());
				solrInputDocument.addField(Document.FIELD_CONTENT_ENCODING, doc.getContentEncoding());
				solrInputDocument.addField(Document.FIELD_CONTENT_TYPE, doc.getContentType());
				solrInputDocument.addField(Document.FIELD_SERVER_IP, doc.getServerIP());
				solrInputDocument.addField(Document.FIELD_CLIENT_IP, doc.getClientIP());
				solrInputDocument.addField(Document.FIELD_RELEVANCE, doc.getRelevance());
				solrInputDocument.addField(Document.FIELD_CORRELATION, doc.getCorrelation());
				solrInputDocument.addField(Document.FIELD_CAPTURE, doc.getCapture());
				solrInputDocument.addField(Document.FIELD_CLIENT_PORT, doc.getClientPort());
				solrInputDocument.addField(Document.FIELD_SERVER_PORT, doc.getServerPort());
				solrInputDocument.addField(Document.FIELD_TIMEZONE_OFFSET, doc.getTimezoneOffset());
				solrInputDocument.addField(Document.FIELD_THREATS, doc.getThreats());
				solrInputDocument.addField(Document.FIELD_ENRICHED_DATA, doc.getEnrichedData());
				solrInputDocument.addField(Document.FIELD_TRASHED_TIME, doc.getTrashedTime());
				solrInputDocument.addField(Document.FIELD_USER_AGENT, doc.getUserAgent());
				solrInputDocument.addField(Document.FIELD_SIZE, doc.getContentSize());
				solrInputDocument.addField(Document.FIELD_TRANSPORT, doc.getTransport());
				solrInputDocument.addField(Document.FIELD_DIRECTION, doc.getDirection());
				solrInputDocument.addField(Document.FIELD_SUB_TYPE, doc.getSubType());
				solrInputDocument.addField(Document.FIELD_INGEST_DATE, new Date());
				solrInputDocument.addField(Document.FIELD_GLOBAL_IDENTIFICATION_NUMBER, doc.getGlobalIdentificationNumber());
				solrInputDocument.addField(Document.FIELD_VERSION_NUMBER, doc.getVersionNumber());
				solrInputDocument.addField(Document.FIELD_GEOFENCE, doc.getGeofence());
				solrInputDocument.addField(Document.FIELD_APPSESSIONID, doc.getAppSessionID());
				solrInputDocument.addField(Document.FIELD_INTERCEPT, doc.getIntercept());
				solrInputDocument.addField(Document.FIELD_INTERCEPT_NAME, doc.getInterceptName());
				solrInputDocument.addField(Document.FIELD_INTERCEPT_DESCRIPTION, doc.getInterceptDesc());
				solrInputDocument.addField(Document.FIELD_SOI, doc.getSoi());
				solrInputDocument.addField(Document.FIELD_SOI_NAME, doc.getSoiName());
				solrInputDocument.addField(Document.FIELD_SUBJECT, doc.getSubject());
				solrInputDocument.addField(Document.FIELD_LOCATION, doc.getLocations());
				solrInputDocument.addField(Document.FIELD_TAP, doc.getTap());
				solrInputDocument.addField(Document.FIELD_GENRE, doc.getGenre());
				solrInputDocument.addField(Document.FIELD_LANG, doc.getLang());
				solrInputDocument.addField(Document.FIELD_CONTENT, doc.getContent());
				solrInputDocument.addField(Document.FIELD_SENDER, doc.getSender());
				solrInputDocument.addField(Document.FIELD_RECIPIENT, doc.getRecipient());
				solrInputDocument.addField(Document.FIELD_DOCUMENT_STATUS, 0);
			
				if (Document.TYPE_WEB == doc.getType()) {
					WebPage webPage = (WebPage) doc;
					solrInputDocument.addField("httpUrl", webPage.getHttpUrl());
					solrInputDocument.addField("link", webPage.getLink());
					solrInputDocument.addField("webMethod", webPage.getWebMethod());
					solrInputDocument.addField("responseCode", webPage.getResponseCode());
					solrInputDocument.addField("httpReferer", webPage.getHttpReferer());
					solrInputDocument.addField("range", webPage.getRange());
					solrInputDocument.addField("videoID", webPage.getVideoID());
					solrInputDocument.addField("videoDuration", webPage.getVideoDuration());
					solrInputDocument.addField("videoAuthor", webPage.getVideoAuthor());
					solrInputDocument.addField("videoData", webPage.getVideoData());
					solrInputDocument.addField("videoFormat", webPage.getVideoFormat());
					solrInputDocument.addField("videoFile", webPage.getVideoFile());
					solrInputDocument.addField("headers", webPage.getHeaders());
					solrInputDocument.addField("webSearchQueryText", webPage.getWebSearchQueryText());
					solrInputDocument.addField("webSearchQueryType", webPage.getWebSearchQueryType());
					solrInputDocument.addField("webMapAccountLogin", webPage.getWebMapAccountLogin());
					solrInputDocument.addField("webMapAccountName", webPage.getWebMapAccountName());
					solrInputDocument.addField("webMapAccountLocation", webPage.getWebMapAccountLocation());
					solrInputDocument.addField("webMapAccountCity", webPage.getWebMapAccountCity());
					solrInputDocument.addField("webMapAccountState", webPage.getWebMapAccountState());
					solrInputDocument.addField("webMapAccountPostal", webPage.getWebMapAccountPostal());
					solrInputDocument.addField("webMapAccountCountry", webPage.getWebMapAccountCountry());
					solrInputDocument.addField("webMapAccountAddress", webPage.getWebMapAccountAddress());
					solrInputDocument.addField("webMapLocationQueryText", webPage.getWebMapLocationQueryText());
					solrInputDocument.addField("webMapLocationWaypointType", webPage.getWebMapLocationWaypointType());
					solrInputDocument.addField("webMapLocationStreet", webPage.getWebMapLocationStreet());
					solrInputDocument.addField("webMapLocationCity", webPage.getWebMapLocationCity());
					solrInputDocument.addField("webMapLocationState", webPage.getWebMapLocationState());
					solrInputDocument.addField("webMapLocationCountry", webPage.getWebMapLocationCountry());
					solrInputDocument.addField("webMapLocationPostal", webPage.getWebMapLocationPostal());
					solrInputDocument.addField("webMapLocationAddress", webPage.getWebMapLocationAddress());
					solrInputDocument.addField("cookie", webPage.getCookie());
					solrInputDocument.addField("reqCookie", webPage.getReqCookie());
					solrInputDocument.addField("respCookie", webPage.getRespCookie());
					solrInputDocument.addField("server", webPage.getServer());
				} else if (Document.TYPE_EMAIL == doc.getType()) {
					Email email = (Email) doc;
					solrInputDocument.addField("messageId", email.getMessageId());
					solrInputDocument.addField("action", email.getAction());
					solrInputDocument.addField("to", email.getTo());
					solrInputDocument.addField("cc", email.getCc());
					solrInputDocument.addField("bcc",email.getBcc());
					solrInputDocument.addField("replyto", email.getReplyto());
					solrInputDocument.addField("draft", email.getDraft());
					solrInputDocument.addField("sent", email.getSent());
					solrInputDocument.addField("duplicateId", email.getDuplicateId());
					solrInputDocument.addField("emailHash", email.getEmailHash());
					solrInputDocument.addField("duplicateCount", email.getDuplicateCount());
					solrInputDocument.addField("attachmentCount", email.getAttachmentCount());
					solrInputDocument.addField("subjectEncoding", email.getSubjectEncoding());
					solrInputDocument.addField("rcptTo", email.getRcptTo());
				} else if (Document.TYPE_ATTACHMENT == doc.getType()) {
					Attachment attachment = (Attachment) doc;
					solrInputDocument.addField("attachmentId", attachment.getAttachmentId());
					solrInputDocument.addField("attachmentNameEncoding", attachment.getAttachmentNameEncoding());
					solrInputDocument.addField("attachmentType", attachment.getAttachmentType());
					solrInputDocument.addField("attachmentSize", attachment.getAttachmentSize());
					solrInputDocument.addField("parent", attachment.getParent());
				} else if (Document.TYPE_CHAT == doc.getType()) {
					Chat chat = (Chat) doc;
					solrInputDocument.addField("loginAlias", chat.getLoginAlias());
					solrInputDocument.addField("loginNickname", chat.getLoginNickname());
					solrInputDocument.addField("loginProfile", chat.getLoginProfile());
					solrInputDocument.addField("fileName", chat.getFileName());
					solrInputDocument.addField("chatProto", chat.getChatProto());
					solrInputDocument.addField("sessionDuration", chat.getSessionDuration());
					solrInputDocument.addField("msgText", chat.getMsgText());
					solrInputDocument.addField("sessionStartTime", chat.getSessionStartTime());
					solrInputDocument.addField("sessionEndTime", chat.getSessionEndTime());
					solrInputDocument.addField("senderNickname", chat.getSenderNickname());
					solrInputDocument.addField("receiverNickname", chat.getReceiverNickname());
					solrInputDocument.addField("msgEncoding", chat.getMsgEncoding());
					solrInputDocument.addField("senderUserAgent", chat.getSenderUserAgent());
					solrInputDocument.addField("receiverUserAgent", chat.getReceiverUserAgent());
					solrInputDocument.addField("fileSender", chat.getFileSender());
					solrInputDocument.addField("fileSize", chat.getFileSize());
					solrInputDocument.addField("voiceStartTime", chat.getVoiceStartTime());
					solrInputDocument.addField("voiceEndTime", chat.getVoiceEndTime());
					solrInputDocument.addField("voiceSender", chat.getVoiceSender());
					solrInputDocument.addField("voiceReceiver", chat.getVoiceReceiver());
					solrInputDocument.addField("voiceSenderUserAgent", chat.getVoiceSenderUserAgent());
					solrInputDocument.addField("voiceReceiverUserAgent", chat.getVoiceReceiverUserAgent());
					solrInputDocument.addField("voiceDuration", chat.getVoiceDuration());
					solrInputDocument.addField("videoStartTime", chat.getVideoStartTime());
					solrInputDocument.addField("videoEndTime", chat.getVoiceEndTime());
					solrInputDocument.addField("videoSender", chat.getVoiceSender());
					solrInputDocument.addField("videoReceiver", chat.getVideoReceiver());
					solrInputDocument.addField("videoSenderUserAgent", chat.getVideoSenderUserAgent());
					solrInputDocument.addField("videoReceiverUserAgent", chat.getVideoReceiverUserAgent());
					solrInputDocument.addField("videoDuration", chat.getVideoDuration());
				} else if (Document.TYPE_IRI == doc.getType()) {
					Iri iri = (Iri) doc;
					solrInputDocument.addField("liid", iri.getLiid());
					solrInputDocument.addField("pdu", iri.getPdu());
					solrInputDocument.addField("ipAccessType", iri.getIpAccessType());
					solrInputDocument.addField("ipAddress", iri.getIpAddress());
					solrInputDocument.addField("e164Address", iri.getE164Address());
					solrInputDocument.addField("messageType", iri.getMessageType());
					solrInputDocument.addField("iriLocations", iri.getIri_locations());
					solrInputDocument.addField("iRIMsisdn", iri.getiRIMsisdn());
					solrInputDocument.addField("iRIImsi", iri.getiRIImsi());
					solrInputDocument.addField("iRIImei", iri.getiRIImei());
				} else if (Document.TYPE_CALL == doc.getType()) {
					Call call = (Call) doc;
					solrInputDocument.addField(Call.FIELD_CALLEE, call.getCallee());
					solrInputDocument.addField(Call.FIELD_CALLER, call.getCaller());
					solrInputDocument.addField(Call.FIELD_CALLEE_NICK_NAME, call.getCalleeNickname());
					solrInputDocument.addField(Call.FIELD_CALLER_NICK_NAME, call.getCallerNickname());
					solrInputDocument.addField(Call.FIELD_CALLEE_USER_AGENT, call.getCalleeUserAgent());
					solrInputDocument.addField(Call.FIELD_CALLER_USER_AGENT, call.getCallerUserAgent());
					solrInputDocument.addField("callerIp", call.getCallerIp());
					solrInputDocument.addField("calleeIp", call.getCalleeIp());
					solrInputDocument.addField("callerPort", call.getCallerPort());
					solrInputDocument.addField("calleePort", call.getCalleePort());
					solrInputDocument.addField("starttime", call.getStartTime());
					solrInputDocument.addField("endtime", call.getEndTime());
					solrInputDocument.addField("audioEncoding", call.getAudioEncoding());
					solrInputDocument.addField("videoEncoding", call.getVideoEncoding());
					solrInputDocument.addField("callerE164", call.getCallerE164());
					solrInputDocument.addField("calleeE164", call.getCalleeE164());
					solrInputDocument.addField("codec", call.getCodec());
					solrInputDocument.addField("ringDuration", call.getRingDuration());
					solrInputDocument.addField("conversationDuration", call.getConversationDuration());
					solrInputDocument.addField("liveListenPipe", call.getLiveListenPipe());
					solrInputDocument.addField("callInfo", call.getCallInfo());
					solrInputDocument.addField("callLocations", call.getCallLocations());
					solrInputDocument.addField("callParticipants", call.getCallParticipants());
					solrInputDocument.addField("callProducts", call.getCallProducts());
					solrInputDocument.addField("callState", call.getCallState());
					solrInputDocument.addField("mediaType", call.getMediaType());
					solrInputDocument.addField("mediaChunkID", call.getMediaChunkID());
					solrInputDocument.addField("mediaChunkStreamID", call.getMediaChunkStreamID());
					solrInputDocument.addField("cseq", call.getCseq());
					solrInputDocument.addField("msisdn", call.getMsisdn());
					solrInputDocument.addField("dialed", call.getDialed());
					solrInputDocument.addField("other", call.getOther());
					solrInputDocument.addField("imei", call.getImei());
					solrInputDocument.addField("imsi", call.getImsi());
					solrInputDocument.addField("connectTime", call.getConnectTime());
					solrInputDocument.addField("disconnectTime", call.getDisconnectTime());
					solrInputDocument.addField("callType", call.getCallType());
					solrInputDocument.addField("serviceType", call.getServiceType());
					solrInputDocument.addField("conditionCode", call.getConditionCode());
					solrInputDocument.addField("disconnectCode", call.getDisconnectCode());
					solrInputDocument.addField("pin", call.getPin());
					solrInputDocument.addField("chargingID", call.getChargingID());
					solrInputDocument.addField("sgsnAddress", call.getSgsnAddress());
					solrInputDocument.addField("ggsnAddress", call.getGgsnAddress());
					solrInputDocument.addField("operatorID", call.getOperatorID());
					solrInputDocument.addField("countryCallingCode", call.getCountryCallingCode());
					solrInputDocument.addField("timezone", call.getTimezone());
					solrInputDocument.addField("accessCode", call.getAccessCode());
					solrInputDocument.addField("trunkID", call.getTrunkID());
					solrInputDocument.addField("incomingCktID", call.getIncomingCktID());
					solrInputDocument.addField("outgoingCktID", call.getOutgoingCktID());
					solrInputDocument.addField("routingArea", call.getRoutingArea());
					solrInputDocument.addField("locationArea", call.getLocationArea());
					solrInputDocument.addField("cellIdentifier", call.getCellIdentifier());
					solrInputDocument.addField("finalPartyNumber", call.getFinalPartyNumber());
					solrInputDocument.addField("accountCode", call.getAccountCode());
					solrInputDocument.addField("teluri", call.getTeluri());
					solrInputDocument.addField("sipurl", call.getSipurl());
					solrInputDocument.addField("teluriCallee", call.getTeluriCallee());
					solrInputDocument.addField("sipurlCallee", call.getSipurlCallee());
					solrInputDocument.addField("teluriForwarded", call.getTeluriForwarded());
					solrInputDocument.addField("sipurlForwarded", call.getSipurlForwarded());
					solrInputDocument.addField("msisdnCallee", call.getMsisdnCallee());
					solrInputDocument.addField("imsiCallee", call.getImsiCallee());
					solrInputDocument.addField("imeiCallee", call.getImeiCallee());
					solrInputDocument.addField("teluriForwarded", call.getTeluriForwarded());
					solrInputDocument.addField("msisdnForwarded", call.getMsisdnForwarded());
					solrInputDocument.addField("imsiForwarded", call.getImsiForwarded());
					solrInputDocument.addField("imeiForwarded", call.getImeiForwarded());
					solrInputDocument.addField("forwarded", call.getForwarded());
					solrInputDocument.addField("callStatus", call.getCallStatus());
				} else if (Document.TYPE_FILEXFER == doc.getType()) {
					FileXfer fileXfer = (FileXfer) doc;
					solrInputDocument.addField("password", fileXfer.getPassword());
					solrInputDocument.addField("mode", fileXfer.getMode());
					solrInputDocument.addField("method", fileXfer.getMethod());
					solrInputDocument.addField("code", fileXfer.getCode());
				} else if (Document.TYPE_INBOX == doc.getType()) {
					Inbox inbox = (Inbox) doc;
					solrInputDocument.addField("msgSenderLogin", inbox.getMsgSenderLogin());
					solrInputDocument.addField("msgSenderAlias", inbox.getMsgSenderAlias());
					solrInputDocument.addField("msgSubject", inbox.getMsgSubject());
					solrInputDocument.addField("msgDate", inbox.getMsgDate());
					solrInputDocument.addField("msgReceiverLogin", inbox.getMsgReceiverLogin());
					solrInputDocument.addField("msgReceiverAlias", inbox.getMsgReceiverAlias());
					solrInputDocument.addField("folderName", inbox.getFolderName());
				} else if (Document.TYPE_CERTIFICATE == doc.getType()) {
					Certificate certificate = (Certificate) doc;
					solrInputDocument.addField("sessionId", certificate.getSessionId());
					solrInputDocument.addField("serverName", certificate.getServerName());
					solrInputDocument.addField("version", certificate.getVersion());
					solrInputDocument.addField("serialNumber", certificate.getSerialNumber());
					solrInputDocument.addField("validityStart", certificate.getValidityStart());
					solrInputDocument.addField("validityEnd", certificate.getValidityEnd());
					solrInputDocument.addField("commonName", certificate.getCommonName());
					solrInputDocument.addField("alternateNames", certificate.getAlternateNames());
					solrInputDocument.addField("issuer", certificate.getIssuer());
				} else if (Document.TYPE_UNKNOWN == doc.getType()) {
					UnknownProtocol unknownProtocol = (UnknownProtocol) doc;
					solrInputDocument.addField("protocolStack", unknownProtocol.getProtocolStack());
					solrInputDocument.addField("pps", unknownProtocol.getPps());
					solrInputDocument.addField("avgPktSizeForSec", unknownProtocol.getAvgPktSizeForSec());
					solrInputDocument.addField("totalChunks", unknownProtocol.getTotalChunks());
				} else if (Document.TYPE_CONTACT == doc.getType()) {
					Contact contact = (Contact) doc;
					solrInputDocument.addField("accountId", contact.getAccountId());
					solrInputDocument.addField("accountName", contact.getAccountName());
					solrInputDocument.addField("onlineStatus", contact.getOnlineStatus());
					solrInputDocument.addField("firstname", contact.getFirstname());
					solrInputDocument.addField("middlename", contact.getMiddlename());
					solrInputDocument.addField("lastname", contact.getLastname());
				}
				
				docs.add(solrInputDocument);
				if (docs.size() == batchSize) {
					server.add(docs);
					docs.clear();
				}
			}
			if (docs.size() > 0) {
				server.add(docs);
				docs.clear();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		//System.out.println(Thread.currentThread().getName() + " End - " + (System.currentTimeMillis() - startTime));
	}
}
