package com.ss8.experiments.indexer;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonCreator;

public class WebPage extends Document {
	
	private String httpUrl;
	
	private long link;
	
	private String webMethod;
	
	private String responseCode;
	
	private String httpReferer;
	
	private String range;
	
	private String videoID;
	
	private int videoDuration;
	
	private String videoAuthor;
	
	private String videoData;
	
	private String videoFormat;
	
	private String videoFile;
	
	private String headers;
	
	private String webSearchQueryText;
	
	private String webSearchQueryType;
	
	private String webMapAccountLogin;
	
	private String webMapAccountName;
	
	private String webMapAccountLocation;
	
	private String webMapAccountCity;
	
	private String webMapAccountState;
	
	private String webMapAccountPostal;
	
	private String webMapAccountCountry;
	
	private String webMapAccountAddress;
	
	private String webMapLocationQueryText;
	
	private String webMapLocationQueryCategory;
	
	private String webMapLocationWaypointType;
	
	private String webMapLocation;
	
	private String webMapLocationStreet;
	
	private String webMapLocationCity;
	
	private String webMapLocationState;
	
	private String webMapLocationCountry;
	
	private String webMapLocationPostal;
	
	private String webMapLocationAddress;
	
	private String cookie;
	
	private String reqCookie;
	
	private String respCookie;
	
	private String server;

	@JsonCreator
	public WebPage(long id, String name, int type, String provider,
			String title, Date date, String contentUrl, String contentEncoding,
			String contentType, String serverIP, String clientIP,
			int relevance, String correlation, int capture, int clientPort,
			int serverPort, int timezoneOffset, String threats,
			String enrichedData, Date trashedTime, String userAgent,
			int contentSize, String transport, int direction, String subType,
			Date ingestDate, String globalIdentificationNumber,
			String versionNumber, String geofence, String appSessionID,
			long intercept, String interceptName, String interceptDesc,
			long soi, String soiName, String subject, String locations,
			String tap, String genre, String lang, String httpUrl, long link,
			String webMethod, String responseCode, String httpReferer,
			String range, String videoID, int videoDuration,
			String videoAuthor, String videoData, String videoFormat,
			String videoFile, String headers, String webSearchQueryText,
			String webSearchQueryType, String webMapAccountLogin,
			String webMapAccountName, String webMapAccountLocation,
			String webMapAccountCity, String webMapAccountState,
			String webMapAccountPostal, String webMapAccountCountry,
			String webMapAccountAddress, String webMapLocationQueryText,
			String webMapLocationQueryCategory,
			String webMapLocationWaypointType, String webMapLocation,
			String webMapLocationStreet, String webMapLocationCity,
			String webMapLocationState, String webMapLocationCountry,
			String webMapLocationPostal, String webMapLocationAddress,
			String cookie, String reqCookie, String respCookie, String server) {
		super(id, name, type, provider, title, date, contentUrl,
				contentEncoding, contentType, serverIP, clientIP, relevance,
				correlation, capture, clientPort, serverPort, timezoneOffset,
				threats, enrichedData, trashedTime, userAgent, contentSize,
				transport, direction, subType, ingestDate,
				globalIdentificationNumber, versionNumber, geofence,
				appSessionID, intercept, interceptName, interceptDesc, soi,
				soiName, subject, locations, tap, genre, lang);
		this.httpUrl = httpUrl;
		this.link = link;
		this.webMethod = webMethod;
		this.responseCode = responseCode;
		this.httpReferer = httpReferer;
		this.range = range;
		this.videoID = videoID;
		this.videoDuration = videoDuration;
		this.videoAuthor = videoAuthor;
		this.videoData = videoData;
		this.videoFormat = videoFormat;
		this.videoFile = videoFile;
		this.headers = headers;
		this.webSearchQueryText = webSearchQueryText;
		this.webSearchQueryType = webSearchQueryType;
		this.webMapAccountLogin = webMapAccountLogin;
		this.webMapAccountName = webMapAccountName;
		this.webMapAccountLocation = webMapAccountLocation;
		this.webMapAccountCity = webMapAccountCity;
		this.webMapAccountState = webMapAccountState;
		this.webMapAccountPostal = webMapAccountPostal;
		this.webMapAccountCountry = webMapAccountCountry;
		this.webMapAccountAddress = webMapAccountAddress;
		this.webMapLocationQueryText = webMapLocationQueryText;
		this.webMapLocationQueryCategory = webMapLocationQueryCategory;
		this.webMapLocationWaypointType = webMapLocationWaypointType;
		this.webMapLocation = webMapLocation;
		this.webMapLocationStreet = webMapLocationStreet;
		this.webMapLocationCity = webMapLocationCity;
		this.webMapLocationState = webMapLocationState;
		this.webMapLocationCountry = webMapLocationCountry;
		this.webMapLocationPostal = webMapLocationPostal;
		this.webMapLocationAddress = webMapLocationAddress;
		this.cookie = cookie;
		this.reqCookie = reqCookie;
		this.respCookie = respCookie;
		this.server = server;
	}

	public WebPage(long id) {
		super(id);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the httpUrl
	 */
	public String getHttpUrl() {
		return httpUrl;
	}

	/**
	 * @param httpUrl the httpUrl to set
	 */
	public void setHttpUrl(String httpUrl) {
		this.httpUrl = httpUrl;
	}

	/**
	 * @return the link
	 */
	public long getLink() {
		return link;
	}

	/**
	 * @param link the link to set
	 */
	public void setLink(long link) {
		this.link = link;
	}

	/**
	 * @return the webMethod
	 */
	public String getWebMethod() {
		return webMethod;
	}

	/**
	 * @param webMethod the webMethod to set
	 */
	public void setWebMethod(String webMethod) {
		this.webMethod = webMethod;
	}

	/**
	 * @return the responseCode
	 */
	public String getResponseCode() {
		return responseCode;
	}

	/**
	 * @param responseCode the responseCode to set
	 */
	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	/**
	 * @return the httpReferer
	 */
	public String getHttpReferer() {
		return httpReferer;
	}

	/**
	 * @param httpReferer the httpReferer to set
	 */
	public void setHttpReferer(String httpReferer) {
		this.httpReferer = httpReferer;
	}

	/**
	 * @return the range
	 */
	public String getRange() {
		return range;
	}

	/**
	 * @param range the range to set
	 */
	public void setRange(String range) {
		this.range = range;
	}

	/**
	 * @return the videoID
	 */
	public String getVideoID() {
		return videoID;
	}

	/**
	 * @param videoID the videoID to set
	 */
	public void setVideoID(String videoID) {
		this.videoID = videoID;
	}

	/**
	 * @return the videoDuration
	 */
	public int getVideoDuration() {
		return videoDuration;
	}

	/**
	 * @param videoDuration the videoDuration to set
	 */
	public void setVideoDuration(int videoDuration) {
		this.videoDuration = videoDuration;
	}

	/**
	 * @return the videoAuthor
	 */
	public String getVideoAuthor() {
		return videoAuthor;
	}

	/**
	 * @param videoAuthor the videoAuthor to set
	 */
	public void setVideoAuthor(String videoAuthor) {
		this.videoAuthor = videoAuthor;
	}

	/**
	 * @return the videoData
	 */
	public String getVideoData() {
		return videoData;
	}

	/**
	 * @param videoData the videoData to set
	 */
	public void setVideoData(String videoData) {
		this.videoData = videoData;
	}

	/**
	 * @return the videoFormat
	 */
	public String getVideoFormat() {
		return videoFormat;
	}

	/**
	 * @param videoFormat the videoFormat to set
	 */
	public void setVideoFormat(String videoFormat) {
		this.videoFormat = videoFormat;
	}

	/**
	 * @return the videoFile
	 */
	public String getVideoFile() {
		return videoFile;
	}

	/**
	 * @param videoFile the videoFile to set
	 */
	public void setVideoFile(String videoFile) {
		this.videoFile = videoFile;
	}

	/**
	 * @return the headers
	 */
	public String getHeaders() {
		return headers;
	}

	/**
	 * @param headers the headers to set
	 */
	public void setHeaders(String headers) {
		this.headers = headers;
	}

	/**
	 * @return the webSearchQueryText
	 */
	public String getWebSearchQueryText() {
		return webSearchQueryText;
	}

	/**
	 * @param webSearchQueryText the webSearchQueryText to set
	 */
	public void setWebSearchQueryText(String webSearchQueryText) {
		this.webSearchQueryText = webSearchQueryText;
	}

	/**
	 * @return the webSearchQueryType
	 */
	public String getWebSearchQueryType() {
		return webSearchQueryType;
	}

	/**
	 * @param webSearchQueryType the webSearchQueryType to set
	 */
	public void setWebSearchQueryType(String webSearchQueryType) {
		this.webSearchQueryType = webSearchQueryType;
	}

	/**
	 * @return the webMapAccountLogin
	 */
	public String getWebMapAccountLogin() {
		return webMapAccountLogin;
	}

	/**
	 * @param webMapAccountLogin the webMapAccountLogin to set
	 */
	public void setWebMapAccountLogin(String webMapAccountLogin) {
		this.webMapAccountLogin = webMapAccountLogin;
	}

	/**
	 * @return the webMapAccountName
	 */
	public String getWebMapAccountName() {
		return webMapAccountName;
	}

	/**
	 * @param webMapAccountName the webMapAccountName to set
	 */
	public void setWebMapAccountName(String webMapAccountName) {
		this.webMapAccountName = webMapAccountName;
	}

	/**
	 * @return the webMapAccountLocation
	 */
	public String getWebMapAccountLocation() {
		return webMapAccountLocation;
	}

	/**
	 * @param webMapAccountLocation the webMapAccountLocation to set
	 */
	public void setWebMapAccountLocation(String webMapAccountLocation) {
		this.webMapAccountLocation = webMapAccountLocation;
	}

	/**
	 * @return the webMapAccountCity
	 */
	public String getWebMapAccountCity() {
		return webMapAccountCity;
	}

	/**
	 * @param webMapAccountCity the webMapAccountCity to set
	 */
	public void setWebMapAccountCity(String webMapAccountCity) {
		this.webMapAccountCity = webMapAccountCity;
	}

	/**
	 * @return the webMapAccountState
	 */
	public String getWebMapAccountState() {
		return webMapAccountState;
	}

	/**
	 * @param webMapAccountState the webMapAccountState to set
	 */
	public void setWebMapAccountState(String webMapAccountState) {
		this.webMapAccountState = webMapAccountState;
	}

	/**
	 * @return the webMapAccountPostal
	 */
	public String getWebMapAccountPostal() {
		return webMapAccountPostal;
	}

	/**
	 * @param webMapAccountPostal the webMapAccountPostal to set
	 */
	public void setWebMapAccountPostal(String webMapAccountPostal) {
		this.webMapAccountPostal = webMapAccountPostal;
	}

	/**
	 * @return the webMapAccountCountry
	 */
	public String getWebMapAccountCountry() {
		return webMapAccountCountry;
	}

	/**
	 * @param webMapAccountCountry the webMapAccountCountry to set
	 */
	public void setWebMapAccountCountry(String webMapAccountCountry) {
		this.webMapAccountCountry = webMapAccountCountry;
	}

	/**
	 * @return the webMapAccountAddress
	 */
	public String getWebMapAccountAddress() {
		return webMapAccountAddress;
	}

	/**
	 * @param webMapAccountAddress the webMapAccountAddress to set
	 */
	public void setWebMapAccountAddress(String webMapAccountAddress) {
		this.webMapAccountAddress = webMapAccountAddress;
	}

	/**
	 * @return the webMapLocationQueryText
	 */
	public String getWebMapLocationQueryText() {
		return webMapLocationQueryText;
	}

	/**
	 * @param webMapLocationQueryText the webMapLocationQueryText to set
	 */
	public void setWebMapLocationQueryText(String webMapLocationQueryText) {
		this.webMapLocationQueryText = webMapLocationQueryText;
	}

	/**
	 * @return the webMapLocationQueryCategory
	 */
	public String getWebMapLocationQueryCategory() {
		return webMapLocationQueryCategory;
	}

	/**
	 * @param webMapLocationQueryCategory the webMapLocationQueryCategory to set
	 */
	public void setWebMapLocationQueryCategory(
			String webMapLocationQueryCategory) {
		this.webMapLocationQueryCategory = webMapLocationQueryCategory;
	}

	/**
	 * @return the webMapLocationWaypointType
	 */
	public String getWebMapLocationWaypointType() {
		return webMapLocationWaypointType;
	}

	/**
	 * @param webMapLocationWaypointType the webMapLocationWaypointType to set
	 */
	public void setWebMapLocationWaypointType(String webMapLocationWaypointType) {
		this.webMapLocationWaypointType = webMapLocationWaypointType;
	}

	/**
	 * @return the webMapLocation
	 */
	public String getWebMapLocation() {
		return webMapLocation;
	}

	/**
	 * @param webMapLocation the webMapLocation to set
	 */
	public void setWebMapLocation(String webMapLocation) {
		this.webMapLocation = webMapLocation;
	}

	/**
	 * @return the webMapLocationStreet
	 */
	public String getWebMapLocationStreet() {
		return webMapLocationStreet;
	}

	/**
	 * @param webMapLocationStreet the webMapLocationStreet to set
	 */
	public void setWebMapLocationStreet(String webMapLocationStreet) {
		this.webMapLocationStreet = webMapLocationStreet;
	}

	/**
	 * @return the webMapLocationCity
	 */
	public String getWebMapLocationCity() {
		return webMapLocationCity;
	}

	/**
	 * @param webMapLocationCity the webMapLocationCity to set
	 */
	public void setWebMapLocationCity(String webMapLocationCity) {
		this.webMapLocationCity = webMapLocationCity;
	}

	/**
	 * @return the webMapLocationState
	 */
	public String getWebMapLocationState() {
		return webMapLocationState;
	}

	/**
	 * @param webMapLocationState the webMapLocationState to set
	 */
	public void setWebMapLocationState(String webMapLocationState) {
		this.webMapLocationState = webMapLocationState;
	}

	/**
	 * @return the webMapLocationCountry
	 */
	public String getWebMapLocationCountry() {
		return webMapLocationCountry;
	}

	/**
	 * @param webMapLocationCountry the webMapLocationCountry to set
	 */
	public void setWebMapLocationCountry(String webMapLocationCountry) {
		this.webMapLocationCountry = webMapLocationCountry;
	}

	/**
	 * @return the webMapLocationPostal
	 */
	public String getWebMapLocationPostal() {
		return webMapLocationPostal;
	}

	/**
	 * @param webMapLocationPostal the webMapLocationPostal to set
	 */
	public void setWebMapLocationPostal(String webMapLocationPostal) {
		this.webMapLocationPostal = webMapLocationPostal;
	}

	/**
	 * @return the webMapLocationAddress
	 */
	public String getWebMapLocationAddress() {
		return webMapLocationAddress;
	}

	/**
	 * @param webMapLocationAddress the webMapLocationAddress to set
	 */
	public void setWebMapLocationAddress(String webMapLocationAddress) {
		this.webMapLocationAddress = webMapLocationAddress;
	}

	/**
	 * @return the cookie
	 */
	public String getCookie() {
		return cookie;
	}

	/**
	 * @param cookie the cookie to set
	 */
	public void setCookie(String cookie) {
		this.cookie = cookie;
	}

	/**
	 * @return the reqCookie
	 */
	public String getReqCookie() {
		return reqCookie;
	}

	/**
	 * @param reqCookie the reqCookie to set
	 */
	public void setReqCookie(String reqCookie) {
		this.reqCookie = reqCookie;
	}

	/**
	 * @return the respCookie
	 */
	public String getRespCookie() {
		return respCookie;
	}

	/**
	 * @param respCookie the respCookie to set
	 */
	public void setRespCookie(String respCookie) {
		this.respCookie = respCookie;
	}

	/**
	 * @return the server
	 */
	public String getServer() {
		return server;
	}

	/**
	 * @param server the server to set
	 */
	public void setServer(String server) {
		this.server = server;
	}
}
