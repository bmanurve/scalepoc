package com.ss8.experiments.indexer;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonCreator;


public class Iri extends Document {
	
	private String liid;
	
	private String pdu;
	
	private String ipAccessType;
	
	private String ipAddress;
	
	private String e164Address;
	
	private int messageType;
	
	private String iri_locations;
	
	private String iRIMsisdn;
	
	private String iRIImsi;
	
	private String iRIImei;
	
	@JsonCreator
	public Iri(long id, String name, int type, String provider, String title,
			Date date, String contentUrl, String contentEncoding,
			String contentType, String serverIP, String clientIP,
			int relevance, String correlation, int capture, int clientPort,
			int serverPort, int timezoneOffset, String threats,
			String enrichedData, Date trashedTime, String userAgent,
			int contentSize, String transport, int direction, String subType,
			Date ingestDate, String globalIdentificationNumber,
			String versionNumber, String geofence, String appSessionID,
			long intercept, String interceptName, String interceptDesc,
			long soi, String soiName, String subject, String locations,
			String tap, String genre, String lang, String liid, String pdu,
			String ipAccessType, String ipAddress, String e164Address,
			int messageType, String iri_locations, String iRIMsisdn,
			String iRIImsi, String iRIImei) {
		super(id, name, type, provider, title, date, contentUrl,
				contentEncoding, contentType, serverIP, clientIP, relevance,
				correlation, capture, clientPort, serverPort, timezoneOffset,
				threats, enrichedData, trashedTime, userAgent, contentSize,
				transport, direction, subType, ingestDate,
				globalIdentificationNumber, versionNumber, geofence,
				appSessionID, intercept, interceptName, interceptDesc, soi,
				soiName, subject, locations, tap, genre, lang);
		this.liid = liid;
		this.pdu = pdu;
		this.ipAccessType = ipAccessType;
		this.ipAddress = ipAddress;
		this.e164Address = e164Address;
		this.messageType = messageType;
		this.iri_locations = iri_locations;
		this.iRIMsisdn = iRIMsisdn;
		this.iRIImsi = iRIImsi;
		this.iRIImei = iRIImei;
	}

	public Iri(long id) {
		super(id);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the liid
	 */
	public String getLiid() {
		return liid;
	}

	/**
	 * @param liid the liid to set
	 */
	public void setLiid(String liid) {
		this.liid = liid;
	}

	/**
	 * @return the pdu
	 */
	public String getPdu() {
		return pdu;
	}

	/**
	 * @param pdu the pdu to set
	 */
	public void setPdu(String pdu) {
		this.pdu = pdu;
	}

	/**
	 * @return the ipAccessType
	 */
	public String getIpAccessType() {
		return ipAccessType;
	}

	/**
	 * @param ipAccessType the ipAccessType to set
	 */
	public void setIpAccessType(String ipAccessType) {
		this.ipAccessType = ipAccessType;
	}

	/**
	 * @return the ipAddress
	 */
	public String getIpAddress() {
		return ipAddress;
	}

	/**
	 * @param ipAddress the ipAddress to set
	 */
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	/**
	 * @return the e164Address
	 */
	public String getE164Address() {
		return e164Address;
	}

	/**
	 * @param e164Address the e164Address to set
	 */
	public void setE164Address(String e164Address) {
		this.e164Address = e164Address;
	}

	/**
	 * @return the messageType
	 */
	public int getMessageType() {
		return messageType;
	}

	/**
	 * @param messageType the messageType to set
	 */
	public void setMessageType(int messageType) {
		this.messageType = messageType;
	}

	/**
	 * @return the iri_locations
	 */
	public String getIri_locations() {
		return iri_locations;
	}

	/**
	 * @param iri_locations the iri_locations to set
	 */
	public void setIri_locations(String iri_locations) {
		this.iri_locations = iri_locations;
	}

	/**
	 * @return the iRIMsisdn
	 */
	public String getiRIMsisdn() {
		return iRIMsisdn;
	}

	/**
	 * @param iRIMsisdn the iRIMsisdn to set
	 */
	public void setiRIMsisdn(String iRIMsisdn) {
		this.iRIMsisdn = iRIMsisdn;
	}

	/**
	 * @return the iRIImsi
	 */
	public String getiRIImsi() {
		return iRIImsi;
	}

	/**
	 * @param iRIImsi the iRIImsi to set
	 */
	public void setiRIImsi(String iRIImsi) {
		this.iRIImsi = iRIImsi;
	}

	/**
	 * @return the iRIImei
	 */
	public String getiRIImei() {
		return iRIImei;
	}

	/**
	 * @param iRIImei the iRIImei to set
	 */
	public void setiRIImei(String iRIImei) {
		this.iRIImei = iRIImei;
	}
}
