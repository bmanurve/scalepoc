package com.ss8.experiments.indexer;

import java.util.Date;
import java.util.HashMap;

import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrQuery.ORDER;
import org.apache.solr.client.solrj.impl.CloudSolrClient;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.SolrInputDocument;

public class UpdateQueriesExecutor implements Runnable {
	
	private int queryIndex = 0;
	
	public UpdateQueriesExecutor(final int queryIndex) {
		this.queryIndex = queryIndex;
	}

	@Override
	public void run() {
		// Execute multiple queries like sort by id, sort by creation date, time range queries and facet queries.
		try {
			CloudSolrClient server = SolrUtils.getSolrServer();
			SolrQuery query = new SolrQuery();
			query.setQuery("(intercept : " + this.queryIndex + ") AND (date:[NOW-2HOURS TO NOW])");
			query.addSort("id", ORDER.desc);
			query.setRows(10);
			QueryResponse queryResponse = server.query(query);
			// Get the top 10 rows and then change from new to visited and also trash and tag few events.
			SolrDocumentList docList = queryResponse.getResults();
			//System.out.println(Thread.currentThread().getName() + " Start - " + this.queryIndex);
			if (queryResponse.getQTime() > 1000) {
				System.out.println(new Date() + " - Query time for getting 10 ids - " + queryResponse.getQTime());
			}
			if (docList != null && docList.size() > 0) {
				int index = 0;
				for (SolrDocument solrDocument : docList) {
					query = new SolrQuery();
					query.setQuery("(id : " + (long) solrDocument.getFieldValue(Document.FIELD_ID) + " )");
					queryResponse = server.query(query);
					if (queryResponse.getQTime() > 1000) {
						System.out.println(new Date() + " - Query time for getting data for 1 id - " + queryResponse.getQTime());
					}
					Thread.sleep(5000);
					SolrInputDocument solrInputDocument = new SolrInputDocument();
					solrInputDocument.addField(Document.FIELD_ID, (long) solrDocument.getFieldValue(Document.FIELD_ID));
					HashMap<String, Object> oper = new HashMap<String, Object>();
					if (index < 5) {
						oper.put("set", 1);
					} else if (index >= 5 && index <= 8) {
						oper.put("set", 3);
						HashMap<String, Object> trashOperation = new HashMap<String, Object>();
						trashOperation.put("set", new Date());
						solrInputDocument.removeField(Document.FIELD_TRASHED_TIME);
						solrInputDocument.addField(Document.FIELD_TRASHED_TIME, trashOperation);
					} else {
						oper.put("set", 2);
						HashMap<String, Object> tagOperation = new HashMap<String, Object>();
						tagOperation.put("set", "highmark");
						solrInputDocument.removeField(Document.FIELD_TAGNAMES);
						solrInputDocument.addField(Document.FIELD_TAGNAMES, tagOperation);
					}
					solrInputDocument.removeField(Document.FIELD_DOCUMENT_STATUS);
					solrInputDocument.addField(Document.FIELD_DOCUMENT_STATUS, oper);
					// update time
					HashMap<String, Object> updateTimeOperation = new HashMap<String, Object>();
					updateTimeOperation.put("set", new Date());
					solrInputDocument.removeField("updateDate");
					solrInputDocument.addField("updateDate", updateTimeOperation);
					server.add(solrInputDocument);
					index++;
				}
			}
			// System.out.println(" - Query end - " + this.queryIndex);
			//System.out.println(Thread.currentThread().getName() + " Stop - " + this.queryIndex);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
