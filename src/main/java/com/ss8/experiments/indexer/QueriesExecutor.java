package com.ss8.experiments.indexer;

import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrQuery.ORDER;
import org.apache.solr.client.solrj.impl.CloudSolrClient;
import org.apache.solr.client.solrj.response.QueryResponse;

public class QueriesExecutor implements Runnable {
	
	private int queryIndex = 0;
	
	public QueriesExecutor(final int queryIndex) {
		this.queryIndex = queryIndex;
	}

	@Override
	public void run() {
		// Execute multiple queries like sort by id, sort by creation date, time range queries and facet queries.
		try {
			if (this.queryIndex % 5 == 0) {
				CloudSolrClient server = SolrUtils.getSolrServer();
				SolrQuery query = new SolrQuery();
				query.setQuery("((intercept : 1) OR (intercept : 2) OR (intercept : 3) OR (intercept : 4) OR (intercept : 5))");
				query.addSort("id", ORDER.desc);
				query.setRows(10);
				QueryResponse response = server.query(query);
				// Get the top 10 rows and 
				System.out.println("Query - " + query.getQuery() + " Response time - " + response.getQTime() + " Query index - " + this.queryIndex);
				//System.out.println("Query time for id sort by desc :- " + response.getQTime() + " Query index - " + this.queryIndex);
				
				query = new SolrQuery();
				query.setQuery("((intercept : 1) OR (intercept : 2) OR (intercept : 3) OR (intercept : 4) OR (intercept : 5))");
				query.addSort("date", ORDER.desc);
				response = server.query(query);
				System.out.println("Query time for id sort by creation date :- " + response.getQTime() + " Query index - " + this.queryIndex);
				
				query = new SolrQuery();
				query.setQuery("((intercept : 1) OR (intercept : 2) OR (intercept : 3) OR (intercept : 4) OR (intercept : 5)) AND (date:[NOW-1HOUR TO NOW])");
				query.addSort("date", ORDER.desc);
				response = server.query(query);
				System.out.println("Query - " + query.getQuery() + " Response time - " + response.getQTime() + " Query index - " + this.queryIndex);
				//System.out.println("Query time for documents created from now to 1 hour ago :- " + response.getQTime() + " Query index - " + this.queryIndex);
				
				query = new SolrQuery();
				query.setQuery("((intercept : 1) OR (intercept : 2) OR (intercept : 3) OR (intercept : 4) OR (intercept : 5)) AND (date:[NOW-10HOURS TO NOW])");
				query.addFacetField("id");
				response = server.query(query);
				System.out.println("Query time for facet query to get all documents created from now to 10 hours ago and facet by id field :- " + response.getQTime() + " Query index - " + this.queryIndex);
			} else if (this.queryIndex % 5 == 1) {
				CloudSolrClient server = SolrUtils.getSolrServer();
				SolrQuery query = new SolrQuery();
				query.setQuery("((intercept : 6) OR (intercept : 7) OR (intercept : 8) OR (intercept : 9) OR (intercept : 10))");
				query.addSort("id", ORDER.desc);
				QueryResponse response = server.query(query);
				System.out.println("Query time for id sort by asc :- " + response.getQTime() + " Query index - " + this.queryIndex);
				
				query = new SolrQuery();
				query.setQuery("((intercept : 6) OR (intercept : 7) OR (intercept : 8) OR (intercept : 9) OR (intercept : 10))");
				query.addSort("ingestDate", ORDER.desc);
				response = server.query(query);
				System.out.println("Query time for id sort by ingestion date :- " + response.getQTime() + " Query index - " + this.queryIndex);
				
				query = new SolrQuery();
				query.setQuery("((intercept : 6) OR (intercept : 7) OR (intercept : 8) OR (intercept : 9) OR (intercept : 10)) AND (date:[NOW-2HOURS TO NOW])");
				query.addSort("date", ORDER.desc);
				response = server.query(query);
				System.out.println("Query time for documents created from now to 2 hours ago :- " + response.getQTime() + " Query index - " + this.queryIndex);
				
				query = new SolrQuery();
				query.setQuery("((intercept : 6) OR (intercept : 7) OR (intercept : 8) OR (intercept : 9) OR (intercept : 10)) AND (date:[NOW-5HOURS TO NOW])");
				query.addFacetField("id");
				response = server.query(query);
				System.out.println("Query time for facet query to get all documents created from now to 5 hours ago and facet by id field :- " + response.getQTime() + " Query index - " + this.queryIndex);
			} else if (this.queryIndex % 5 == 2) {
				CloudSolrClient server = SolrUtils.getSolrServer();
				SolrQuery query = new SolrQuery();
				query.setQuery("((intercept : 11) OR (intercept : 12) OR (intercept : 13) OR (intercept : 14) OR (intercept : 15))");
				query.addSort("id", ORDER.desc);
				QueryResponse response = server.query(query);
				System.out.println("Query time for id sort by asc :- " + response.getQTime() + " Query index - " + this.queryIndex);
				
				query = new SolrQuery();
				query.setQuery("((intercept : 11) OR (intercept : 12) OR (intercept : 13) OR (intercept : 14) OR (intercept : 15))");
				query.addSort("ingestDate", ORDER.desc);
				response = server.query(query);
				System.out.println("Query time for id sort by ingestion date :- " + response.getQTime() + " Query index - " + this.queryIndex);
				
				query = new SolrQuery();
				query.setQuery("((intercept : 11) OR (intercept : 12) OR (intercept : 13) OR (intercept : 14) OR (intercept : 15)) AND (date:[NOW-2HOURS TO NOW])");
				query.addSort("date", ORDER.desc);
				response = server.query(query);
				System.out.println("Query time for documents created from now to 2 hours ago :- " + response.getQTime() + " Query index - " + this.queryIndex);
				
				query = new SolrQuery();
				query.setQuery("((intercept : 11) OR (intercept : 12) OR (intercept : 13) OR (intercept : 14) OR (intercept : 15)) AND (date:[NOW-5HOURS TO NOW])");
				query.addFacetField("id");
				response = server.query(query);
				System.out.println("Query time for facet query to get all documents created from now to 5 hours ago and facet by id field :- " + response.getQTime() + " Query index - " + this.queryIndex);
			} else if (this.queryIndex % 5 == 3) {
				CloudSolrClient server = SolrUtils.getSolrServer();
				SolrQuery query = new SolrQuery();
				query.setQuery("((intercept : 16) OR (intercept : 17) OR (intercept : 18) OR (intercept : 19) OR (intercept : 20))");
				query.addSort("id", ORDER.desc);
				QueryResponse response = server.query(query);
				System.out.println("Query time for id sort by asc :- " + response.getQTime() + " Query index - " + this.queryIndex);
				
				query = new SolrQuery();
				query.setQuery("((intercept : 16) OR (intercept : 17) OR (intercept : 18) OR (intercept : 19) OR (intercept : 20))");
				query.addSort("ingestDate", ORDER.desc);
				response = server.query(query);
				System.out.println("Query time for id sort by ingestion date :- " + response.getQTime() + " Query index - " + this.queryIndex);
				
				query = new SolrQuery();
				query.setQuery("((intercept : 16) OR (intercept : 17) OR (intercept : 18) OR (intercept : 19) OR (intercept : 20)) AND (date:[NOW-2HOURS TO NOW])");
				query.addSort("date", ORDER.desc);
				response = server.query(query);
				System.out.println("Query time for documents created from now to 2 hours ago :- " + response.getQTime() + " Query index - " + this.queryIndex);
				
				query = new SolrQuery();
				query.setQuery("((intercept : 16) OR (intercept : 17) OR (intercept : 18) OR (intercept : 19) OR (intercept : 20)) AND (date:[NOW-5HOURS TO NOW])");
				query.addFacetField("id");
				response = server.query(query);
				System.out.println("Query time for facet query to get all documents created from now to 5 hours ago and facet by id field :- " + response.getQTime() + " Query index - " + this.queryIndex);
			} else if (this.queryIndex % 5 == 4) {
				CloudSolrClient server = SolrUtils.getSolrServer();
				SolrQuery query = new SolrQuery();
				query.setQuery("((intercept : 21) OR (intercept : 22) OR (intercept : 23) OR (intercept : 24) OR (intercept : 25))");
				query.addSort("id", ORDER.desc);
				QueryResponse response = server.query(query);
				System.out.println("Query time for id sort by asc :- " + response.getQTime() + " Query index - " + this.queryIndex);
				
				query = new SolrQuery();
				query.setQuery("((intercept : 21) OR (intercept : 22) OR (intercept : 23) OR (intercept : 24) OR (intercept : 25))");
				query.addSort("ingestDate", ORDER.desc);
				response = server.query(query);
				System.out.println("Query time for id sort by ingestion date :- " + response.getQTime() + " Query index - " + this.queryIndex);
				
				query = new SolrQuery();
				query.setQuery("((intercept : 21) OR (intercept : 22) OR (intercept : 23) OR (intercept : 24) OR (intercept : 25)) AND (date:[NOW-2HOURS TO NOW])");
				query.addSort("date", ORDER.desc);
				response = server.query(query);
				System.out.println("Query time for documents created from now to 2 hours ago :- " + response.getQTime() + " Query index - " + this.queryIndex);
				
				query = new SolrQuery();
				query.setQuery("((intercept : 21) OR (intercept : 22) OR (intercept : 23) OR (intercept : 24) OR (intercept : 25)) AND (date:[NOW-5HOURS TO NOW])");
				query.addFacetField("id");
				response = server.query(query);
				System.out.println("Query time for facet query to get all documents created from now to 5 hours ago and facet by id field :- " + response.getQTime() + " Query index - " + this.queryIndex);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
