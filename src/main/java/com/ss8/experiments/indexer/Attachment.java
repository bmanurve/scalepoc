package com.ss8.experiments.indexer;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonCreator;

public class Attachment extends Document {
	
	private String attachmentId;
	
	private String attachmentNameEncoding;
	
	private String attachmentType;
	
	private int attachmentSize;
	
	private long parent;

	@JsonCreator
	public Attachment(long id, String name, int type, String provider,
			String title, Date date, String contentUrl, String contentEncoding,
			String contentType, String serverIP, String clientIP,
			int relevance, String correlation, int capture, int clientPort,
			int serverPort, int timezoneOffset, String threats,
			String enrichedData, Date trashedTime, String userAgent,
			int contentSize, String transport, int direction, String subType,
			Date ingestDate, String globalIdentificationNumber,
			String versionNumber, String geofence, String appSessionID,
			long intercept, String interceptName, String interceptDesc,
			long soi, String soiName, String subject, String locations,
			String tap, String genre, String lang, String attachmentId,
			String attachmentNameEncoding, String attachmentType,
			int attachmentSize, long parent) {
		super(id, name, type, provider, title, date, contentUrl,
				contentEncoding, contentType, serverIP, clientIP, relevance,
				correlation, capture, clientPort, serverPort, timezoneOffset,
				threats, enrichedData, trashedTime, userAgent, contentSize,
				transport, direction, subType, ingestDate,
				globalIdentificationNumber, versionNumber, geofence,
				appSessionID, intercept, interceptName, interceptDesc, soi,
				soiName, subject, locations, tap, genre, lang);
		this.attachmentId = attachmentId;
		this.attachmentNameEncoding = attachmentNameEncoding;
		this.attachmentType = attachmentType;
		this.attachmentSize = attachmentSize;
		this.parent = parent;
	}

	public Attachment(long id) {
		super(id);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the attachmentId
	 */
	public String getAttachmentId() {
		return attachmentId;
	}

	/**
	 * @param attachmentId the attachmentId to set
	 */
	public void setAttachmentId(String attachmentId) {
		this.attachmentId = attachmentId;
	}

	/**
	 * @return the attachmentNameEncoding
	 */
	public String getAttachmentNameEncoding() {
		return attachmentNameEncoding;
	}

	/**
	 * @param attachmentNameEncoding the attachmentNameEncoding to set
	 */
	public void setAttachmentNameEncoding(String attachmentNameEncoding) {
		this.attachmentNameEncoding = attachmentNameEncoding;
	}

	/**
	 * @return the attachmentType
	 */
	public String getAttachmentType() {
		return attachmentType;
	}

	/**
	 * @param attachmentType the attachmentType to set
	 */
	public void setAttachmentType(String attachmentType) {
		this.attachmentType = attachmentType;
	}

	/**
	 * @return the attachmentSize
	 */
	public int getAttachmentSize() {
		return attachmentSize;
	}

	/**
	 * @param attachmentSize the attachmentSize to set
	 */
	public void setAttachmentSize(int attachmentSize) {
		this.attachmentSize = attachmentSize;
	}

	/**
	 * @return the parent
	 */
	public long getParent() {
		return parent;
	}

	/**
	 * @param parent the parent to set
	 */
	public void setParent(long parent) {
		this.parent = parent;
	}

}
