package com.ss8.experiments.indexer;

import java.util.Date;
import java.util.concurrent.BlockingQueue;

public class IngesterThread implements Runnable {
	
	private BlockingQueue<Document> ingesterQueue;
	
	private int threadId;
	
	private boolean isDrap = false;
	
	public IngesterThread(int threadId, final BlockingQueue<Document> ingesterQueue, final boolean isDrap) {
		this.threadId = threadId;
		this.ingesterQueue = ingesterQueue;
		this.isDrap = isDrap;
	}

	@Override
	public void run() {
		//System.out.println(Thread.currentThread().getName() + " Start - " + this.threadName);
		if (threadId % 5 == 0) {
			// Create calls, iri, unknown and chat.
			// Create call
			Call call = createCall(threadId);
			try {
				if (!this.isDrap) {
					this.ingesterQueue.put(call);
				}
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			// Create iris
			for (int index = 0; index < 6; index++) {
				Iri iri = createIri(threadId);
				try {
					if (!this.isDrap) {
						this.ingesterQueue.put(iri);
					}
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			
			// Create unknown protocol
			UnknownProtocol unknownProtocol = createUnknownProtocol(threadId);
			try {
				this.ingesterQueue.put(unknownProtocol);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			// Create chats
			for (int index = 0; index < 2; index++) {
				Chat chat = createChat(threadId);
				try {
					if (!this.isDrap) {
						this.ingesterQueue.put(chat);
					}
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		} else if (threadId % 5 == 1) {
			// Create web, email, ftp and attachments.
			// Create web.
			for (int index = 0; index < 2; index++) {
				WebPage webPage = createWebPage(threadId);
				try {
					this.ingesterQueue.put(webPage);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			
			// Create email
			for (int index = 0; index < 2; index++) {
				Email email = createEmail(threadId);
				try {
					if (!this.isDrap) {
						this.ingesterQueue.put(email);
					}
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			
			// Create fileXfer
			for (int index = 0; index < 2; index++) {
				FileXfer fileXfer = createFileXfer(this.threadId);
				try {
					this.ingesterQueue.put(fileXfer);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			
			// Create attachments
			for (int index = 0; index < 4; index++) {
				Attachment attachment = createAttachment(threadId);
				try {
					if (!this.isDrap) {
						this.ingesterQueue.put(attachment);
					}
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		} else if (threadId % 5 == 2) {
			// Create web.
			for (int index = 0; index < 30; index++) {
				WebPage webPage = createWebPage(threadId);
				try {
					this.ingesterQueue.put(webPage);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		} else if (threadId % 5 == 3) {
			// Create web, certificates, inbox and contact.
			// Create web.
			for (int index = 0; index < 8; index++) {
				WebPage webPage = createWebPage(threadId);
				try {
					this.ingesterQueue.put(webPage);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			
			// Create certificates.
			for (int index = 0; index < 6; index++) {
				Certificate certificate = createCertificate(threadId);
				try {
					this.ingesterQueue.put(certificate);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			
			// Create inbox.
			for (int index = 0; index < 6; index++) {
				Inbox inbox = createInbox(threadId);
				try {
					this.ingesterQueue.put(inbox);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			
			// Create contact.
			for (int index = 0; index < 6; index++) {
				Contact contact = createContact(threadId);
				try {
					this.ingesterQueue.put(contact);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		} else if (threadId % 5 == 4) {
			// Create web, email, chat, certificates and attachments.
			// Create web
			for (int index = 0; index < 3; index++) {
				WebPage webPage = createWebPage(threadId);
				try {
					this.ingesterQueue.put(webPage);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			
			// Create email
			for (int index = 0; index < 2; index++) {
				Email email = createEmail(threadId);
				try {
					if (!this.isDrap) {
						this.ingesterQueue.put(email);
					}
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			
			// Create chat
			for (int index = 0; index < 1; index++) {
				Chat chat = createChat(threadId);
				try {
					if (!this.isDrap) {
						this.ingesterQueue.put(chat);
					}
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			
			// Create certificates
			for (int index = 0; index < 2; index++) {
				Certificate certificate = createCertificate(threadId);
				try {
					this.ingesterQueue.put(certificate);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			
			// Create attachments
			for (int index = 0; index < 2; index++) {
				Attachment attachment = createAttachment(threadId);
				try {
					if (!this.isDrap) {
						this.ingesterQueue.put(attachment);
					}
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		}
		
		//System.out.println(Thread.currentThread().getName() + " End - " + this.threadName + " Size :- " + this.ingesterQueue.size());
	}
	
	private synchronized Call createCall(long threadId) {
		Call call = new Call(QueueLoader.getNextId(), "call", 6, "call", "CallTitle",
				new Date(), "test.wav", "encoding",
				"call", "0.0.0.0", "0.0.0.0",
				1, "IRI-CORRELATION", 3, 0,
				0, 480, "",
				"test1234", null, null,
				100, null, 3, null,
				null, "12345678910",
				"test1234567893", "geofence", "sessionid_1234567893",
				threadId, "Intercept - " + threadId, "Intercept_Description_oRktY",
				300, "soiName", "Subject blah blah how are you?", "LOC1-1234-56-789-3,LOC2-1234-56-789-3,LOC3-1234-56-789-3,LOC4-1234-56-789-3,LOC5-1234-56-789-3",
				"3", "genre", "en_us", "1234567893",
				"1234567895", "CallerNickName", "CalleeNickName",
				"CalleeUserAgent", "CallerUserAgent", "10.0.156.192",
				"10.0.156.191", 50, 300, new Date(),
				new Date(), "audioEncoding", "videoEncoding",
				"callerE164", "calleeE164", "codec",
				300, 50000, "/var/intellego/contentvmc/" + threadId + "/test.wav",
				"[{\"id\":1, \"timestamp\":\"2017-07-10T03:00:00\", \"type\":3, \"partyDesignator\":10, \"event\":\"callEvent\", \"value\":\"test1234567893test1234567893test1234567893test1234567893test1234567893test1234567893\", \"call\":3}, {\"id\":2, \"timestamp\":\"2017-07-10T03:00:00\", \"type\":3, \"partyDesignator\":10, \"event\":\"callEvent\", \"value\":\"test1234567893test1234567893test1234567893test1234567893test1234567893test1234567893\", \"call\":3}]", 
				"[{\"id\":1, \"timestamp\":\"2017-07-10T03:00:00\", \"type\":3, \"value\":\"234-10-32276-13099\", \"call\":3}, {\"id\":2, \"timestamp\":\"2017-07-10T03:00:00\", \"type\":3, \"value\":\"234-10-32276-13099\", \"call\":3}, {\"id\":3, \"timestamp\":\"2017-07-10T03:00:00\", \"type\":3, \"value\":\"234-10-32276-13099\", \"call\":3}, {\"id\":4, \"timestamp\":\"2017-07-10T03:00:00\", \"type\":3, \"value\":\"234-10-32276-13099\", \"call\":3}, {\"id\":5, \"timestamp\":\"2017-07-10T03:00:00\", \"type\":3, \"value\":\"234-10-32276-13099\", \"call\":3}]",  
				"[{\"id\":1, \"timestamp\":\"2017-07-10T03:00:00\", \"type\":3, \"phoneNo\":01632960010, \"msisdn\":447700900000, \"imei\":510123234763453, \"e164Format\":510123234763453, \"tei\":510123234763453, \"sipURI\":\"sipUrl\", \"telURL\":\"telURL\", \"userAgent\":\"userAgent\", \"call\":3}, {\"id\":2, \"timestamp\":\"2017-07-10T03:00:00\", \"type\":3, \"phoneNo\":07700900001, \"msisdn\":447700900000, \"imei\":510123234763453, \"e164Format\":510123234763453, \"tei\":510123234763453, \"sipURI\":\"sipUrl\", \"telURL\":\"telURL\", \"userAgent\":\"userAgent\", \"call\":3}]",
				"[{\"id\":1, \"clip_time_origin\":\"2017-07-10T03:00:00\", \"type\":3, \"url\":\"file://var/intellego/contentvmc/EAST-1/2016-10-26/201610262247/1/target/wav/66eada72-2d9e-4023-b005-b8951fcd0bc9.wav\", \"size\":300, \"order\":300, \"clip_duration\":300, \"name\":\"call_product\", \"description\":\"description\", \"exemplar\":300, \"call\":3}]", 
				30, 30,
				30, 800, 800, "msisdn",
				"dialed", "other", "510123234763453", "310350987656436",
				new Date(), new Date(), 3,
				"serviceType", 800, 800,
				"310350987656436", "chargingID", "510123234763453",
				"510123234763453", "510123234763453", 800,
				"timezone", "accessCode", "trunkID",
				"incomingCktID", "outgoingCktID", "routingArea",
				"locationArea", "510123234763453",
				"510123234763453", "accountCode", "teluri",
				"sipurl", "teluriCallee", "sipurlCallee",
				"teluriForwarded", "sipurlForwarded",
				"msisdnCallee", "imsiCallee", "imeiCallee",
				"msisdnForwarded", "imsiForwarded", "imeiForwarded",
				"forwarded", 300);
		call.setContent("Hi, I am nearly there. Are you there yet?  Hi, I am nearly there. Are you there yet?  Hi, I am nearly there. Are you there yet?  Hi, I am nearly there. Are you there yet?  Hi, I am nearly there. Are you there yet?  ");
		return call;
	}
	
	private synchronized Iri createIri(long threadId) {
		Iri iri = new Iri(QueueLoader.getNextId(), "iri", 5, "iri", "IriTitle",
				new Date(), "test.wav", "encoding",
				"call", "0.0.0.0", "0.0.0.0",
				2, "IRI-CORRELATION", 3, 0,
				0, 480, "",
				"test1234", null, null,
				100, null, 3, null,
				null, "12345678910",
				"test1234567893", "geofence", "sessionid_1234567893",
				threadId, "Intercept - " + threadId, "Intercept_Description_oRktY",
				300, "soiName", "Subject blah blah how are you?", "LOC1-1234-56-789-3,LOC2-1234-56-789-3,LOC3-1234-56-789-3,LOC4-1234-56-789-3,LOC5-1234-56-789-3",
				"3", "genre", "en_us", "Intercept", 
				"pSHeader{li-psDomainId { 0 4 0 2 2 5 1 13 }, lawfulInterceptionIdentifier 454153542D31H,authorizationCountryCode \"GB\",communicationIdentifier{networkIdentifier{operatorIdentifier 6E61H,networkElementIdentifier 31302E332E35302E323336H},communicationIdentityNumber 111},sequenceNumber 0,timeStamp \"20161019150931Z\"},payload iRIPayloadSequence :{{iRIType iRI-Report,iRIContents eTSI671IRI : iRI-Parameters :{domainID { 0 4 0 2 2 1 16 },iRIversion lastVersion,lawfulInterceptionIdentifier 454153542D31H,communicationIdentifier{communication-Identity-Number 313131H,network-Identifier{operator-Identifier 6E61H}},timeStamp localTime :{generalizedTime \"20161019150931Z\",winterSummerIndication notProvided},intercepted-Call-Direct terminating-Target,intercepted-Call-State idle,locationOfTheTarget{globalCellID 32F4017E14332BH},partyInformation{{party-Qualifier originating-Party,partyIdentity{imei 15103232743654F3H,imsi 13300589676534F6H,callingPartyNumber iSUP-Format : 8410700790000000H,msISDN 51447700090000H}},{party-Qualifier terminating-Party,partyIdentity{imei 15304247957832F7H,imsi 13300575437856F4H,calledPartyNumber iSUP-Format : 8410700790000001H,msISDN 51447700090010H}}},nature-Of-The-intercepted-call gSM-SMS-Message,sMS{communicationIdentifier{network-Identifier{operator-Identifier 6E61H}},timeStamp utcTime : \"0001010000Z\",sMS-Contents{initiator server,content 0405A82143F500006101116121850031B2 ...H}}}}}} ",
				"ipAccessType", "10.0.156.192", "e164Address",
				300, 
				"[{\"id\":1, \"timestamp\":\"2017-07-10T03:00:00\", \"type\":3, \"partyDesignator\":10, \"event\":\"callEvent\", \"value\":\"test1234567893test1234567893test1234567893test1234567893test1234567893test1234567893\", \"call\":3}, {\"id\":2, \"timestamp\":\"2017-07-10T03:00:00\", \"type\":3, \"partyDesignator\":10, \"event\":\"callEvent\", \"value\":\"test1234567893test1234567893test1234567893test1234567893test1234567893test1234567893\", \"call\":3}]", 
				"510123234763453",
				"510123234763453", "510123234763453");
		return iri;
	}
	
	private synchronized Chat createChat(long threadId) {
		Chat chat = new Chat(QueueLoader.getNextId(), "chat", 4, "chat", "ChatTitle",
				new Date(), "test.wav", "encoding",
				"call", "0.0.0.0", "0.0.0.0",
				1, "IRI-CORRELATION", 3, 0,
				0, 480, "",
				"test1234", null, null,
				100, null, 3, null,
				null, "12345678910",
				"test1234567893", "geofence", "sessionid_1234567893",
				threadId, "Intercept - " + threadId, "Intercept_Description_oRktY",
				300, "soiName", "Subject blah blah how are you?", "LOC1-1234-56-789-3,LOC2-1234-56-789-3,LOC3-1234-56-789-3,LOC4-1234-56-789-3,LOC5-1234-56-789-3",
				"3", "genre", "en_us", "loginAlias",
				"loginNickname", "loginProfile", "fileName",
				"chatProto", 3000, "Subject blah blah how are you?",
				3000, 8000, "senderNickname",
				"receiverNickname", "msgEncoding",
				"senderUserAgent", "receiverUserAgent",
				"fileSender", 3000, new Date(),
				new Date(), "voiceSender", "voiceReceiver",
				"voiceSenderUserAgent", "voiceReceiverUserAgent",
				3000, new Date(), new Date(),
				"videoSender", "videoReceiver",
				"videoSenderUserAgent", "videoReceiverUserAgent",
				3000);
		chat.setContent("Hi, I am nearly there. Are you there yet?  Hi, I am nearly there. Are you there yet?  Hi, I am nearly there. Are you there yet?  Hi, I am nearly there. Are you there yet?  Hi, I am nearly there. Are you there yet?  ");
		return chat;
	}
	
	private synchronized UnknownProtocol createUnknownProtocol(long threadId) {
		UnknownProtocol unknownProtocol = new UnknownProtocol(QueueLoader.getNextId(), "unknown", 10, "unknown", "UnknownTitle",
				new Date(), "test.wav", "encoding",
				"call", "0.0.0.0", "0.0.0.0",
				1, "IRI-CORRELATION", 3, 0,
				0, 480, "",
				"test1234", null, null,
				100, null, 3, null,
				null, "12345678910",
				"test1234567893", "geofence", "sessionid_1234567893",
				threadId, "Intercept - " + threadId, "Intercept_Description_oRktY",
				300, "soiName", "Subject blah blah how are you?", "LOC1-1234-56-789-3,LOC2-1234-56-789-3,LOC3-1234-56-789-3,LOC4-1234-56-789-3,LOC5-1234-56-789-3",
				"3", "genre", "en_us", 
				"protocolStack",
				300, 300, 300);
		return unknownProtocol;
	}
	
	private synchronized WebPage createWebPage(long threadId) {
		WebPage webPage = new WebPage(QueueLoader.getNextId(), "webPage", 1, "webPage", "WebPageTitle",
				new Date(), "test.wav", "encoding",
				"application/octet-stream", "0.0.0.0", "0.0.0.0",
				1, "IRI-CORRELATION", 3, 0,
				0, 480, "",
				"test1234", null, "Mozilla/5.0 (Windows NT 6.1; rv:23.0) Gecko/20100101 Firefox/23.0",
				100, null, 3, null,
				null, "12345678910",
				"test1234567893", "geofence", "sessionid_1234567893",
				threadId, "Intercept - " + threadId, "Intercept_Description_oRktY",
				300, "soiName", "Subject blah blah how are you?", "LOC1-1234-56-789-3,LOC2-1234-56-789-3,LOC3-1234-56-789-3,LOC4-1234-56-789-3,LOC5-1234-56-789-3",
				"3", "genre", "en_us", 
				"httpUrl", 300,
				"POST", "500", "httpReferer",
				"range_1234567893", "videoID", 300,
				"videoAuthor", "videoData", "videoFormat",
				"videoFile", 
				"| X-Nokia-MSISDN==19099521783  MSISDN==9099521783  X-Nokia-IMSI==310260481271106  X-Nokia-sgsnipaddress==216.155.168.238  Proxy-Connection==Keep-Alive  HOST==10.184.33.187  Accept==application/vnd.wap.mms-message, */*  Accept-Charset==utf-8  Accept-Language==en  User-Agent==SAMSUNG-SGH-T819/T819UVHB2 SHP/VPP/R5 NetFront/3.4 SMM-MMS/1.2.0 profile/MIDP-2.0 configuration/CLDC-1.1  x-wap-profile==\"http://wap.samsungmobile.com/uaprof/SGH-T819.xml\" |", 
				"webSearchQueryText AND webSearchQueryText AND webSearchQueryText AND webSearchQueryText OR webSearchQueryText",
				"webSearchQueryType", "webMapAccountLogin",
				"webMapAccountName", "webMapAccountLocation",
				"webMapAccountCity", "webMapAccountState",
				"webMapAccountPostal", "webMapAccountCountry",
				"webMapAccountAddress_webMapAccountAddress_webMapAccountAddress_webMapAccountAddress_webMapAccountAddress_", 
				"webMapLocationQueryText AND webMapLocationQueryText OR webMapLocationQueryText AND webMapLocationQueryText",
				"webMapLocationQueryCategory",
				"webMapLocationWaypointType", "webMapLocation",
				"webMapLocationStreet_webMapLocationStreet_webMapLocationStreet_webMapLocationStreet_", "webMapLocationCity",
				"webMapLocationState", "webMapLocationCountry",
				"webMapLocationPostal", "webMapLocationAddress",
				"bh=test12345567893333333333333333333!#vex!!!!#>4]Rd!%Clh!!!!#>4]R:!%ODP!!!!#>4]LN!%QY0!!!!#>4]P$!%^v; path=/; expires=Sat, 09-May-2015 02:02:28 GMT", 
				"bh=test12345567893333333333333333333!#vex!!!!#>4]Rd!%Clh!!!!#>4]R:!%ODP!!!!#>4]LN!%QY0!!!!#>4]P$!%^v; path=/; expires=Sat, 09-May-2015 02:02:28 GMTbh=test12345567893333333333333333333!#vex!!!!#>4]Rd!%Clh!!!!#>4]R:!%ODP!!!!#>4]LN!%QY0!!!!#>4]P$!%^v; path=/; expires=Sat, 09-May-2015 02:02:28 GMTbh=test12345567893333333333333333333!#vex!!!!#>4]Rd!%Clh!!!!#>4]R:!%ODP!!!!#>4]LN!%QY0!!!!#>4]P$!%^v; path=/; expires=Sat, 09-May-2015 02:02:28 GMT", 
				"bh=test12345567893333333333333333333!#vex!!!!#>4]Rd!%Clh!!!!#>4]R:!%ODP!!!!#>4]LN!%QY0!!!!#>4]P$!%^v; path=/; expires=Sat, 09-May-2015 02:02:28 GMTbh=test12345567893333333333333333333!#vex!!!!#>4]Rd!%Clh!!!!#>4]R:!%ODP!!!!#>4]LN!%QY0!!!!#>4]P$!%^v; path=/; expires=Sat, 09-May-2015 02:02:28 GMTbh=test12345567893333333333333333333!#vex!!!!#>4]Rd!%Clh!!!!#>4]R:!%ODP!!!!#>4]LN!%QY0!!!!#>4]P$!%^v; path=/; expires=Sat, 09-May-2015 02:02:28 GMT", 
				"googleads.g.doubleclick.net");
		/*webPage.setSender("www.google-analytics.com/__utm.gif?utmwv=5.4.5&utms=5&utmn=1295126235&utmhn=www.metacafe.com&utmt=event&utme=5(SearchPageActions*Video%20Feed%20Click*11001279%20-%20Brad%20Pitt%20in%20Talks%20for%20World%20War%20Z%20Sequel)8(Landing*FamilyFilter*4!TopicDetails*Branding)9(84*Anonymous%2Fon*4!Brad_Pitt-main*Topic%2Ftopics)11(1*2)&utmcs=UTF-8&utmsr=1366x768&utmvp=1003x576&utmsc=24-bit&utmul=en-us&utmje=1&utmfl=11.8%20r800&utmdt=Watch%20Brad%20Pitt%20Videos%2C%20Interviews%2C%20Quotes%20And%20Guest%20Star%20Appearances%20Online&utmhid=1331674618&utmr=0&utmp=%2Ftopics%2Fbrad_pitt%2F&utmht=1380015906846&utmac=UA-6119600-12&utmcc=__utma%3D255612564.1475690143.1380015770.1380015770.1380015770.1%3B%2B__utmz%3D255612564.1380015770.1.1.utmcsr%3D(direct)%7Cutmccn%3D(direct)%7Cutmcmd%3D(none)%3B&utmu=yQCgAAAAIC~");
		webPage.setRecipient("www.google-analytics.com/__utm.gif?utmwv=5.4.5&utms=5&utmn=1295126235&utmhn=www.metacafe.com&utmt=event&utme=5(SearchPageActions*Video%20Feed%20Click*11001279%20-%20Brad%20Pitt%20in%20Talks%20for%20World%20War%20Z%20Sequel)8(Landing*FamilyFilter*4!TopicDetails*Branding)9(84*Anonymous%2Fon*4!Brad_Pitt-main*Topic%2Ftopics)11(1*2)&utmcs=UTF-8&utmsr=1366x768&utmvp=1003x576&utmsc=24-bit&utmul=en-us&utmje=1&utmfl=11.8%20r800&utmdt=Watch%20Brad%20Pitt%20Videos%2C%20Interviews%2C%20Quotes%20And%20Guest%20Star%20Appearances%20Online&utmhid=1331674618&utmr=0&utmp=%2Ftopics%2Fbrad_pitt%2F&utmht=1380015906846&utmac=UA-6119600-12&utmcc=__utma%3D255612564.1475690143.1380015770.1380015770.1380015770.1%3B%2B__utmz%3D255612564.1380015770.1.1.utmcsr%3D(direct)%7Cutmccn%3D(direct)%7Cutmcmd%3D(none)%3B&utmu=yQCgAAAAIC~");*/
		//webPage.setContent("SS8 Inc Safeguarding Societies Always Skip to main content Home page SS8 Inc logo Home page SS8 Inc Search form Search Enter the terms you wish to search for Products Overview Xcipio Mediation Intellego Xcipio Probe Industries Overview Law Enforcement Telecom Providers National Governments Solutions Overview Gray VoIP High Bandwidth Smartphone Advanced Monitoring Center Data discovery Encryption Ipv6 Threat Discovery Company Overview Management Investors Careers Partners Overview Interoperability Partners Activities Events Pages Overview Trademarks Copyright Privacy Policy Contact Locations Inquiries http www.ss8.com solutions solutions telecommunications providers http www.ss8.com solutions solutions law enforcement agencies http www.ss8.com solutionsoverview solutions customer-3 Previous Pause Next Law Enforcement Agencies No packet unturned That's what law enforcement gets when they need access to any communications their suspects use from webmail web search and social media use to call detail records about Learn More Telecommunications Providers Return on Intelligence Telecommunications providers facing lawful intercept legislative and regulatory requirements need sophisticated scalable standards compliant voice and ip data access and Learn More National Governments Connecting the digital dots Having access to the most comprehensive set of intelligence data on your suspects improves your knowledge of your target their associates and their actions You need a Learn More Social Media Updates Find SS8 Inc on Twitter Find SS8 Inc on Twitter Find SS8 Inc on LinkedIn Find SS8 Inc on LinkedIn Find SS8 Inc on Twitter Find SS8 Inc on Twitter Find SS8 Inc on LinkedIn Find SS8 Inc on LinkedIn Login Lost Password Manage Your Account Products Overview Xcipio Mediation Intellego Xcipio Probe Industries Overview Law Enforcement Telecom Providers National Governments Solutions Overview Gray VoIP High Bandwidth Smartphone Advanced Monitoring Center Data discovery Encryption Ipv6 Threat Discovery Company Overview Management Investors Careers Partners Interoperability Partners Activities Events Contact Locations Inquiries Terms Conditions Privacy Policy Privacy Policy Trademarks of SS8 1990-2012 SS8 Inc All Rights Reserved ");
		webPage.setSender("www.google-analytics.com/");
		webPage.setRecipient("www.google-analytics.com/__utm.gif?utmwv=5.4");
		webPage.setContent("test");
		return webPage;
	}
	
	private synchronized FileXfer createFileXfer(long threadId) {
		FileXfer fileXfer = new FileXfer(QueueLoader.getNextId(), "ftp", 7, "ftp", "FtpTitle",
				new Date(), "test.wav", "encoding",
				"call", "0.0.0.0", "0.0.0.0",
				1, "IRI-CORRELATION", 3, 0,
				0, 480, "",
				"test1234", null, null,
				100, null, 3, null,
				null, "12345678910",
				"test1234567893", "geofence", "sessionid_1234567893",
				threadId, "Intercept - " + threadId, "Intercept_Description_oRktY",
				300, "soiName", "Subject blah blah how are you?", "LOC1-1234-56-789-3,LOC2-1234-56-789-3,LOC3-1234-56-789-3,LOC4-1234-56-789-3,LOC5-1234-56-789-3",
				"3", "genre", "en_us", 
				"password",
				"mode", 800, 1800);
		return fileXfer;
	}
	
	private synchronized Inbox createInbox(long threadId) {
		Inbox inbox = new Inbox(QueueLoader.getNextId(), "inbox", 8, "inbox", "InboxTitle",
				new Date(), "test.wav", "encoding",
				"call", "0.0.0.0", "0.0.0.0",
				1, "IRI-CORRELATION", 3, 0,
				0, 480, "",
				"test1234", null, null,
				100, null, 3, null,
				null, "12345678910",
				"test1234567893", "geofence", "sessionid_1234567893",
				threadId, "Intercept - " + threadId, "Intercept_Description_oRktY",
				300, "soiName", "Subject blah blah how are you?", "LOC1-1234-56-789-3,LOC2-1234-56-789-3,LOC3-1234-56-789-3,LOC4-1234-56-789-3,LOC5-1234-56-789-3",
				"3", "genre", "en_us", 
				"msgSenderLogin",
				"msgSenderAlias", "Subject blah blah how are you?", new Date(),
				"msgReceiverLogin", "msgReceiverAlias", "folderName");
		return inbox;
	}
	
	private synchronized Email createEmail(long threadId) {
		Email email = new Email(QueueLoader.getNextId(), "email", 2, "email", "EmailTitle",
				new Date(), "test.wav", "encoding",
				"call", "0.0.0.0", "0.0.0.0",
				1, "IRI-CORRELATION", 3, 0,
				0, 480, "",
				"test1234", null, null,
				100, null, 3, null,
				null, "12345678910",
				"test1234567893", "geofence", "sessionid_1234567893",
				threadId, "Intercept - " + threadId, "Intercept_Description_oRktY",
				300, "soiName", "Subject blah blah how are you?", "LOC1-1234-56-789-3,LOC2-1234-56-789-3,LOC3-1234-56-789-3,LOC4-1234-56-789-3,LOC5-1234-56-789-3",
				"3", "genre", "en_us", 
				"0AA5A47056970000BA400001~sun5x1035003",
				"action", "test800@gmail.com", "test3@gmail.com", "test5@gmail.com", "test8@gmail.com",
				3, new Date(), 800, "0AA5A47056970000BA400001~sun5x1035003",
				0, 3, "0AA5A47056970000BA400001~sun5x1035003",
				"test300@gmail.com");
		email.setSender("test300@gmail.com");
		email.setRecipient("test800@gmail.com");
		/*email.setContent("SS8 Inc Safeguarding Societies Always Skip to main content Home page SS8 Inc logo Home page SS8 Inc Search form Search Enter the terms you wish to search for Products Overview Xcipio Mediation Intellego Xcipio Probe Industries Overview Law Enforcement Telecom Providers National Governments Solutions Overview Gray VoIP High Bandwidth Smartphone Advanced Monitoring Center Data discovery Encryption Ipv6 Threat Discovery Company Overview Management Investors Careers Partners Overview Interoperability Partners Activities Events Pages Overview Trademarks Copyright Privacy Policy Contact Locations Inquiries http www.ss8.com solutions solutions telecommunications providers http www.ss8.com solutions solutions law enforcement agencies http www.ss8.com solutionsoverview solutions customer-3 Previous Pause Next Law Enforcement Agencies No packet unturned That's what law enforcement gets when they need access to any communications their suspects use from webmail web search and social media use to call detail records about Learn More Telecommunications Providers Return on Intelligence Telecommunications providers facing lawful intercept legislative and regulatory requirements need sophisticated scalable standards compliant voice and ip data access and Learn More National Governments Connecting the digital dots Having access to the most comprehensive set of intelligence data on your suspects improves your knowledge of your target their associates and their actions You need a Learn More Social Media Updates Find SS8 Inc on Twitter Find SS8 Inc on Twitter Find SS8 Inc on LinkedIn Find SS8 Inc on LinkedIn Find SS8 Inc on Twitter Find SS8 Inc on Twitter Find SS8 Inc on LinkedIn Find SS8 Inc on LinkedIn Login Lost Password Manage Your Account Products Overview Xcipio Mediation Intellego Xcipio Probe Industries Overview Law Enforcement Telecom Providers National Governments Solutions Overview Gray VoIP High Bandwidth Smartphone Advanced Monitoring Center Data discovery Encryption Ipv6 Threat Discovery Company Overview Management Investors Careers Partners Interoperability Partners Activities Events Contact Locations Inquiries Terms Conditions Privacy Policy Privacy Policy Trademarks of SS8 1990-2012 SS8 Inc All Rights Reserved ");*/
		email.setContent("email content for testing purpose");
		return email;
	}
	
	private synchronized Certificate createCertificate(long threadId) {
		Certificate certificate = new Certificate(QueueLoader.getNextId(), "certificate", 9, "certificate", "CertificateTitle",
				new Date(), "test.wav", "encoding",
				"call", "0.0.0.0", "0.0.0.0",
				1, "IRI-CORRELATION", 3, 0,
				0, 480, "",
				"test1234", null, null,
				100, null, 3, null,
				null, "12345678910",
				"test1234567893", "geofence", "sessionid_1234567893",
				threadId, "Intercept - " + threadId, "Intercept_Description_oRktY",
				300, "soiName", "Subject blah blah how are you?", "LOC1-1234-56-789-3,LOC2-1234-56-789-3,LOC3-1234-56-789-3,LOC4-1234-56-789-3,LOC5-1234-56-789-3",
				"3", "genre", "en_us", 
				"0AA5A47056970000BA400001~sun5x1035003",
				"login.yahoo.com", "0851f959814145cabde024e212c9c20e", "b64ccc7c8cd35551b9653de3202c51c8",
				"0851f959814145cabde024e212c9c20e", new Date(), new Date(),
				"DigiCert High Assurance CA-3", "DigiCert High Assurance CA-3 - Alternate", "C=US,O=DigiCert Inc,OU=www.digicert.com,CN=DigiCert High Assurance EV Root CA");
		return certificate;
	}
	
	private synchronized Attachment createAttachment(long threadId) {
		Attachment attachment = new Attachment(QueueLoader.getNextId(), "attachment", 3, "attachment", "AttachmentTitle",
				new Date(), "test.wav", "encoding",
				"application/pdf", "0.0.0.0", "0.0.0.0",
				2, "IRI-CORRELATION", 3, 0,
				0, 480, "",
				"test1234", null, null,
				100, null, 3, null,
				null, "12345678910",
				"test1234567893", "geofence", "sessionid_1234567893",
				threadId, "Intercept - " + threadId, "Intercept_Description_oRktY",
				300, "soiName", "Subject blah blah how are you?", "LOC1-1234-56-789-3,LOC2-1234-56-789-3,LOC3-1234-56-789-3,LOC4-1234-56-789-3,LOC5-1234-56-789-3",
				"3", "genre", "en_us", 
				"0851f959814145cabde024e212c9c20e",
				"0851f959814145cabde024e212c9c20e", "attachmentType",
				300, 300);
		/*attachment.setContent("SS8 Inc Safeguarding Societies Always Skip to main content Home page SS8 Inc logo Home page SS8 Inc Search form Search Enter the terms you wish to search for Products Overview Xcipio Mediation Intellego Xcipio Probe Industries Overview Law Enforcement Telecom Providers National Governments Solutions Overview Gray VoIP High Bandwidth Smartphone Advanced Monitoring Center Data discovery Encryption Ipv6 Threat Discovery Company Overview Management Investors Careers Partners Overview Interoperability Partners Activities Events Pages Overview Trademarks Copyright Privacy Policy Contact Locations Inquiries http www.ss8.com solutions solutions telecommunications providers http www.ss8.com solutions solutions law enforcement agencies http www.ss8.com solutionsoverview solutions customer-3 Previous Pause Next Law Enforcement Agencies No packet unturned That's what law enforcement gets when they need access to any communications their suspects use from webmail web search and social media use to call detail records about Learn More Telecommunications Providers Return on Intelligence Telecommunications providers facing lawful intercept legislative and regulatory requirements need sophisticated scalable standards compliant voice and ip data access and Learn More National Governments Connecting the digital dots Having access to the most comprehensive set of intelligence data on your suspects improves your knowledge of your target their associates and their actions You need a Learn More Social Media Updates Find SS8 Inc on Twitter Find SS8 Inc on Twitter Find SS8 Inc on LinkedIn Find SS8 Inc on LinkedIn Find SS8 Inc on Twitter Find SS8 Inc on Twitter Find SS8 Inc on LinkedIn Find SS8 Inc on LinkedIn Login Lost Password Manage Your Account Products Overview Xcipio Mediation Intellego Xcipio Probe Industries Overview Law Enforcement Telecom Providers National Governments Solutions Overview Gray VoIP High Bandwidth Smartphone Advanced Monitoring Center Data discovery Encryption Ipv6 Threat Discovery Company Overview Management Investors Careers Partners Interoperability Partners Activities Events Contact Locations Inquiries Terms Conditions Privacy Policy Privacy Policy Trademarks of SS8 1990-2012 SS8 Inc All Rights Reserved SS8 Inc Safeguarding Societies Always Skip to main content Home page SS8 Inc logo Home page SS8 Inc Search form Search Enter the terms you wish to search for Products Overview Xcipio Mediation Intellego Xcipio Probe Industries Overview Law Enforcement Telecom Providers National Governments Solutions Overview Gray VoIP High Bandwidth Smartphone Advanced Monitoring Center Data discovery Encryption Ipv6 Threat Discovery Company Overview Management Investors Careers Partners Overview Interoperability Partners Activities Events Pages Overview Trademarks Copyright Privacy Policy Contact Locations Inquiries http www.ss8.com solutions solutions telecommunications providers http www.ss8.com solutions solutions law enforcement agencies http www.ss8.com solutionsoverview solutions customer-3 Previous Pause Next Law Enforcement Agencies No packet unturned That's what law enforcement gets when they need access to any communications their suspects use from webmail web search and social media use to call detail records about Learn More Telecommunications Providers Return on Intelligence Telecommunications providers facing lawful intercept legislative and regulatory requirements need sophisticated scalable standards compliant voice and ip data access and Learn More National Governments Connecting the digital dots Having access to the most comprehensive set of intelligence data on your suspects improves your knowledge of your target their associates and their actions You need a Learn More Social Media Updates Find SS8 Inc on Twitter Find SS8 Inc on Twitter Find SS8 Inc on LinkedIn Find SS8 Inc on LinkedIn Find SS8 Inc on Twitter Find SS8 Inc on Twitter Find SS8 Inc on LinkedIn Find SS8 Inc on LinkedIn Login Lost Password Manage Your Account Products Overview Xcipio Mediation Intellego Xcipio Probe Industries Overview Law Enforcement Telecom Providers National Governments Solutions Overview Gray VoIP High Bandwidth Smartphone Advanced Monitoring Center Data discovery Encryption Ipv6 Threat Discovery Company Overview Management Investors Careers Partners Interoperability Partners Activities Events Contact Locations Inquiries Terms Conditions Privacy Policy Privacy Policy Trademarks of SS8 1990-2012 SS8 Inc All Rights Reserved SS8 Inc Safeguarding Societies Always Skip to main content Home page SS8 Inc logo Home page SS8 Inc Search form Search Enter the terms you wish to search for Products Overview Xcipio Mediation Intellego Xcipio Probe Industries Overview Law Enforcement Telecom Providers National Governments Solutions Overview Gray VoIP High Bandwidth Smartphone Advanced Monitoring Center Data discovery Encryption Ipv6 Threat Discovery Company Overview Management Investors Careers Partners Overview Interoperability Partners Activities Events Pages Overview Trademarks Copyright Privacy Policy Contact Locations Inquiries http www.ss8.com solutions solutions telecommunications providers http www.ss8.com solutions solutions law enforcement agencies http www.ss8.com solutionsoverview solutions customer-3 Previous Pause Next Law Enforcement Agencies No packet unturned That's what law enforcement gets when they need access to any communications their suspects use from webmail web search and social media use to call detail records about Learn More Telecommunications Providers Return on Intelligence Telecommunications providers facing lawful intercept legislative and regulatory requirements need sophisticated scalable standards compliant voice and ip data access and Learn More National Governments Connecting the digital dots Having access to the most comprehensive set of intelligence data on your suspects improves your knowledge of your target their associates and their actions You need a Learn More Social Media Updates Find SS8 Inc on Twitter Find SS8 Inc on Twitter Find SS8 Inc on LinkedIn Find SS8 Inc on LinkedIn Find SS8 Inc on Twitter Find SS8 Inc on Twitter Find SS8 Inc on LinkedIn Find SS8 Inc on LinkedIn Login Lost Password Manage Your Account Products Overview Xcipio Mediation Intellego Xcipio Probe Industries Overview Law Enforcement Telecom Providers National Governments Solutions Overview Gray VoIP High Bandwidth Smartphone Advanced Monitoring Center Data discovery Encryption Ipv6 Threat Discovery Company Overview Management Investors Careers Partners Interoperability Partners Activities Events Contact Locations Inquiries Terms Conditions Privacy Policy Privacy Policy Trademarks of SS8 1990-2012 SS8 Inc All Rights Reserved SS8 Inc Safeguarding Societies Always Skip to main content Home page SS8 Inc logo Home page SS8 Inc Search form Search Enter the terms you wish to search for Products Overview Xcipio Mediation Intellego Xcipio Probe Industries Overview Law Enforcement Telecom Providers National Governments Solutions Overview Gray VoIP High Bandwidth Smartphone Advanced Monitoring Center Data discovery Encryption Ipv6 Threat Discovery Company Overview Management Investors Careers Partners Overview Interoperability Partners Activities Events Pages Overview Trademarks Copyright Privacy Policy Contact Locations Inquiries http www.ss8.com solutions solutions telecommunications providers http www.ss8.com solutions solutions law enforcement agencies http www.ss8.com solutionsoverview solutions customer-3 Previous Pause Next Law Enforcement Agencies No packet unturned That's what law enforcement gets when they need access to any communications their suspects use from webmail web search and social media use to call detail records about Learn More Telecommunications Providers Return on Intelligence Telecommunications providers facing lawful intercept legislative and regulatory requirements need sophisticated scalable standards compliant voice and ip data access and Learn More National Governments Connecting the digital dots Having access to the most comprehensive set of intelligence data on your suspects improves your knowledge of your target their associates and their actions You need a Learn More Social Media Updates Find SS8 Inc on Twitter Find SS8 Inc on Twitter Find SS8 Inc on LinkedIn Find SS8 Inc on LinkedIn Find SS8 Inc on Twitter Find SS8 Inc on Twitter Find SS8 Inc on LinkedIn Find SS8 Inc on LinkedIn Login Lost Password Manage Your Account Products Overview Xcipio Mediation Intellego Xcipio Probe Industries Overview Law Enforcement Telecom Providers National Governments Solutions Overview Gray VoIP High Bandwidth Smartphone Advanced Monitoring Center Data discovery Encryption Ipv6 Threat Discovery Company Overview Management Investors Careers Partners Interoperability Partners Activities Events Contact Locations Inquiries Terms Conditions Privacy Policy Privacy Policy Trademarks of SS8 1990-2012 SS8 Inc All Rights Reserved SS8 Inc Safeguarding Societies Always Skip to main content Home page SS8 Inc logo Home page SS8 Inc Search form Search Enter the terms you wish to search for Products Overview Xcipio Mediation Intellego Xcipio Probe Industries Overview Law Enforcement Telecom Providers National Governments Solutions Overview Gray VoIP High Bandwidth Smartphone Advanced Monitoring Center Data discovery Encryption Ipv6 Threat Discovery Company Overview Management Investors Careers Partners Overview Interoperability Partners Activities Events Pages Overview Trademarks Copyright Privacy Policy Contact Locations Inquiries http www.ss8.com solutions solutions telecommunications providers http www.ss8.com solutions solutions law enforcement agencies http www.ss8.com solutionsoverview solutions customer-3 Previous Pause Next Law Enforcement Agencies No packet unturned That's what law enforcement gets when they need access to any communications their suspects use from webmail web search and social media use to call detail records about Learn More Telecommunications Providers Return on Intelligence Telecommunications providers facing lawful intercept legislative and regulatory requirements need sophisticated scalable standards compliant voice and ip data access and Learn More National Governments Connecting the digital dots Having access to the most comprehensive set of intelligence data on your suspects improves your knowledge of your target their associates and their actions You need a Learn More Social Media Updates Find SS8 Inc on Twitter Find SS8 Inc on Twitter Find SS8 Inc on LinkedIn Find SS8 Inc on LinkedIn Find SS8 Inc on Twitter Find SS8 Inc on Twitter Find SS8 Inc on LinkedIn Find SS8 Inc on LinkedIn Login Lost Password Manage Your Account Products Overview Xcipio Mediation Intellego Xcipio Probe Industries Overview Law Enforcement Telecom Providers National Governments Solutions Overview Gray VoIP High Bandwidth Smartphone Advanced Monitoring Center Data discovery Encryption Ipv6 Threat Discovery Company Overview Management Investors Careers Partners Interoperability Partners Activities Events Contact Locations Inquiries Terms Conditions Privacy Policy Privacy Policy Trademarks of SS8 1990-2012 SS8 Inc All Rights Reserved SS8 Inc Safeguarding Societies Always Skip to main content Home page SS8 Inc logo Home page SS8 Inc Search form Search Enter the terms you wish to search for Products Overview Xcipio Mediation Intellego Xcipio Probe Industries Overview Law Enforcement Telecom Providers National Governments Solutions Overview Gray VoIP High Bandwidth Smartphone Advanced Monitoring Center Data discovery Encryption Ipv6 Threat Discovery Company Overview Management Investors Careers Partners Overview Interoperability Partners Activities Events Pages Overview Trademarks Copyright Privacy Policy Contact Locations Inquiries http www.ss8.com solutions solutions telecommunications providers http www.ss8.com solutions solutions law enforcement agencies http www.ss8.com solutionsoverview solutions customer-3 Previous Pause Next Law Enforcement Agencies No packet unturned That's what law enforcement gets when they need access to any communications their suspects use from webmail web search and social media use to call detail records about Learn More Telecommunications Providers Return on Intelligence Telecommunications providers facing lawful intercept legislative and regulatory requirements need sophisticated scalable standards compliant voice and ip data access and Learn More National Governments Connecting the digital dots Having access to the most comprehensive set of intelligence data on your suspects improves your knowledge of your target their associates and their actions You need a Learn More Social Media Updates Find SS8 Inc on Twitter Find SS8 Inc on Twitter Find SS8 Inc on LinkedIn Find SS8 Inc on LinkedIn Find SS8 Inc on Twitter Find SS8 Inc on Twitter Find SS8 Inc on LinkedIn Find SS8 Inc on LinkedIn Login Lost Password Manage Your Account Products Overview Xcipio Mediation Intellego Xcipio Probe Industries Overview Law Enforcement Telecom Providers National Governments Solutions Overview Gray VoIP High Bandwidth Smartphone Advanced Monitoring Center Data discovery Encryption Ipv6 Threat Discovery Company Overview Management Investors Careers Partners Interoperability Partners Activities Events Contact Locations Inquiries Terms Conditions Privacy Policy Privacy Policy Trademarks of SS8 1990-2012 SS8 Inc All Rights Reserved SS8 Inc Safeguarding Societies Always Skip to main content Home page SS8 Inc logo Home page SS8 Inc Search form Search Enter the terms you wish to search for Products Overview Xcipio Mediation Intellego Xcipio Probe Industries Overview Law Enforcement Telecom Providers National Governments Solutions Overview Gray VoIP High Bandwidth Smartphone Advanced Monitoring Center Data discovery Encryption Ipv6 Threat Discovery Company Overview Management Investors Careers Partners Overview Interoperability Partners Activities Events Pages Overview Trademarks Copyright Privacy Policy Contact Locations Inquiries http www.ss8.com solutions solutions telecommunications providers http www.ss8.com solutions solutions law enforcement agencies http www.ss8.com solutionsoverview solutions customer-3 Previous Pause Next Law Enforcement Agencies No packet unturned That's what law enforcement gets when they need access to any communications their suspects use from webmail web search and social media use to call detail records about Learn More Telecommunications Providers Return on Intelligence Telecommunications providers facing lawful intercept legislative and regulatory requirements need sophisticated scalable standards compliant voice and ip data access and Learn More National Governments Connecting the digital dots Having access to the most comprehensive set of intelligence data on your suspects improves your knowledge of your target their associates and their actions You need a Learn More Social Media Updates Find SS8 Inc on Twitter Find SS8 Inc on Twitter Find SS8 Inc on LinkedIn Find SS8 Inc on LinkedIn Find SS8 Inc on Twitter Find SS8 Inc on Twitter Find SS8 Inc on LinkedIn Find SS8 Inc on LinkedIn Login Lost Password Manage Your Account Products Overview Xcipio Mediation Intellego Xcipio Probe Industries Overview Law Enforcement Telecom Providers National Governments Solutions Overview Gray VoIP High Bandwidth Smartphone Advanced Monitoring Center Data discovery Encryption Ipv6 Threat Discovery Company Overview Management Investors Careers Partners Interoperability Partners Activities Events Contact Locations Inquiries Terms Conditions Privacy Policy Privacy Policy Trademarks of SS8 1990-2012 SS8 Inc All Rights Reserved SS8 Inc Safeguarding Societies Always Skip to main content Home page SS8 Inc logo Home page SS8 Inc Search form Search Enter the terms you wish to search for Products Overview Xcipio Mediation Intellego Xcipio Probe Industries Overview Law Enforcement Telecom Providers National Governments Solutions Overview Gray VoIP High Bandwidth Smartphone Advanced Monitoring Center Data discovery Encryption Ipv6 Threat Discovery Company Overview Management Investors Careers Partners Overview Interoperability Partners Activities Events Pages Overview Trademarks Copyright Privacy Policy Contact Locations Inquiries http www.ss8.com solutions solutions telecommunications providers http www.ss8.com solutions solutions law enforcement agencies http www.ss8.com solutionsoverview solutions customer-3 Previous Pause Next Law Enforcement Agencies No packet unturned That's what law enforcement gets when they need access to any communications their suspects use from webmail web search and social media use to call detail records about Learn More Telecommunications Providers Return on Intelligence Telecommunications providers facing lawful intercept legislative and regulatory requirements need sophisticated scalable standards compliant voice and ip data access and Learn More National Governments Connecting the digital dots Having access to the most comprehensive set of intelligence data on your suspects improves your knowledge of your target their associates and their actions You need a Learn More Social Media Updates Find SS8 Inc on Twitter Find SS8 Inc on Twitter Find SS8 Inc on LinkedIn Find SS8 Inc on LinkedIn Find SS8 Inc on Twitter Find SS8 Inc on Twitter Find SS8 Inc on LinkedIn Find SS8 Inc on LinkedIn Login Lost Password Manage Your Account Products Overview Xcipio Mediation Intellego Xcipio Probe Industries Overview Law Enforcement Telecom Providers National Governments Solutions Overview Gray VoIP High Bandwidth Smartphone Advanced Monitoring Center Data discovery Encryption Ipv6 Threat Discovery Company Overview Management Investors Careers Partners Interoperability Partners Activities Events Contact Locations Inquiries Terms Conditions Privacy Policy Privacy Policy Trademarks of SS8 1990-2012 SS8 Inc All Rights Reserved SS8 Inc Safeguarding Societies Always Skip to main content Home page SS8 Inc logo Home page SS8 Inc Search form Search Enter the terms you wish to search for Products Overview Xcipio Mediation Intellego Xcipio Probe Industries Overview Law Enforcement Telecom Providers National Governments Solutions Overview Gray VoIP High Bandwidth Smartphone Advanced Monitoring Center Data discovery Encryption Ipv6 Threat Discovery Company Overview Management Investors Careers Partners Overview Interoperability Partners Activities Events Pages Overview Trademarks Copyright Privacy Policy Contact Locations Inquiries http www.ss8.com solutions solutions telecommunications providers http www.ss8.com solutions solutions law enforcement agencies http www.ss8.com solutionsoverview solutions customer-3 Previous Pause Next Law Enforcement Agencies No packet unturned That's what law enforcement gets when they need access to any communications their suspects use from webmail web search and social media use to call detail records about Learn More Telecommunications Providers Return on Intelligence Telecommunications providers facing lawful intercept legislative and regulatory requirements need sophisticated scalable standards compliant voice and ip data access and Learn More National Governments Connecting the digital dots Having access to the most comprehensive set of intelligence data on your suspects improves your knowledge of your target their associates and their actions You need a Learn More Social Media Updates Find SS8 Inc on Twitter Find SS8 Inc on Twitter Find SS8 Inc on LinkedIn Find SS8 Inc on LinkedIn Find SS8 Inc on Twitter Find SS8 Inc on Twitter Find SS8 Inc on LinkedIn Find SS8 Inc on LinkedIn Login Lost Password Manage Your Account Products Overview Xcipio Mediation Intellego Xcipio Probe Industries Overview Law Enforcement Telecom Providers National Governments Solutions Overview Gray VoIP High Bandwidth Smartphone Advanced Monitoring Center Data discovery Encryption Ipv6 Threat Discovery Company Overview Management Investors Careers Partners Interoperability Partners Activities Events Contact Locations Inquiries Terms Conditions Privacy Policy Privacy Policy Trademarks of SS8 1990-2012 SS8 Inc All Rights Reserved SS8 Inc Safeguarding Societies Always Skip to main content Home page SS8 Inc logo Home page SS8 Inc Search form Search Enter the terms you wish to search for Products Overview Xcipio Mediation Intellego Xcipio Probe Industries Overview Law Enforcement Telecom Providers National Governments Solutions Overview Gray VoIP High Bandwidth Smartphone Advanced Monitoring Center Data discovery Encryption Ipv6 Threat Discovery Company Overview Management Investors Careers Partners Overview Interoperability Partners Activities Events Pages Overview Trademarks Copyright Privacy Policy Contact Locations Inquiries http www.ss8.com solutions solutions telecommunications providers http www.ss8.com solutions solutions law enforcement agencies http www.ss8.com solutionsoverview solutions customer-3 Previous Pause Next Law Enforcement Agencies No packet unturned That's what law enforcement gets when they need access to any communications their suspects use from webmail web search and social media use to call detail records about Learn More Telecommunications Providers Return on Intelligence Telecommunications providers facing lawful intercept legislative and regulatory requirements need sophisticated scalable standards compliant voice and ip data access and Learn More National Governments Connecting the digital dots Having access to the most comprehensive set of intelligence data on your suspects improves your knowledge of your target their associates and their actions You need a Learn More Social Media Updates Find SS8 Inc on Twitter Find SS8 Inc on Twitter Find SS8 Inc on LinkedIn Find SS8 Inc on LinkedIn Find SS8 Inc on Twitter Find SS8 Inc on Twitter Find SS8 Inc on LinkedIn Find SS8 Inc on LinkedIn Login Lost Password Manage Your Account Products Overview Xcipio Mediation Intellego Xcipio Probe Industries Overview Law Enforcement Telecom Providers National Governments Solutions Overview Gray VoIP High Bandwidth Smartphone Advanced Monitoring Center Data discovery Encryption Ipv6 Threat Discovery Company Overview Management Investors Careers Partners Interoperability Partners Activities Events Contact Locations Inquiries Terms Conditions Privacy Policy Privacy Policy Trademarks of SS8 1990-2012 SS8 Inc All Rights Reserved ");*/
		attachment.setContent("SS8 Inc Safeguarding Societies Always Skip to main content Home page");
		return attachment;
	}
	
	private synchronized Contact createContact(long threadId) {
		Contact contact = new Contact(QueueLoader.getNextId(), "contact", 11, "contact", "ContactTitle",
				new Date(), "test.wav", "encoding",
				"call", "0.0.0.0", "0.0.0.0",
				1, "IRI-CORRELATION", 3, 0,
				0, 480, "",
				"test1234", null, null,
				100, null, 3, null,
				null, "12345678910",
				"test1234567893", "geofence", "sessionid_1234567893",
				threadId, "Intercept - " + threadId, "Intercept_Description_oRktY",
				300, "soiName", "Subject blah blah how are you?", "LOC1-1234-56-789-3,LOC2-1234-56-789-3,LOC3-1234-56-789-3,LOC4-1234-56-789-3,LOC5-1234-56-789-3",
				"3", "genre", "en_us", 
				"accountId",
				"accountName", "onlineStatus", "firstname",
				"middlename", "lastname");
		return contact;
	}
}
