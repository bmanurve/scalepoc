package com.ss8.experiments.indexer;

import java.util.Collection;
import java.util.LinkedList;
import java.util.concurrent.BlockingQueue;

public class ElasticSearchIndexer implements Runnable {
private BlockingQueue<Document> indexerQueue;
	
	public ElasticSearchIndexer(final BlockingQueue<Document> indexerQueue) {
		this.indexerQueue = indexerQueue;
	}

	@Override
	public void run() {
		// long startTime = System.currentTimeMillis();
		try {
			Collection<Document> documents = new LinkedList<Document>();
			indexerQueue.drainTo(documents);
			// TODO : Add Elastic Search bulk indexing logic here.
		} catch (Exception e) {
			e.printStackTrace();
		}
		//System.out.println(Thread.currentThread().getName() + " End - " + (System.currentTimeMillis() - startTime));
	}
}
