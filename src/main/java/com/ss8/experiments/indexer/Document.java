package com.ss8.experiments.indexer;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties({"content", "date", "id", "ingestDate", "trashedTime", "name", "status", "type", "tagNames"})
public class Document {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7131025634705144075L;
	public static final byte TYPE_WEB			= 1;
	public static final byte TYPE_EMAIL			= 2;
	public static final byte TYPE_ATTACHMENT	= 3;
	public static final byte TYPE_CHAT			= 4;
	public static final byte TYPE_IRI			= 5;
	public static final byte TYPE_CALL			= 6;
	public static final byte TYPE_CONTACT		= 11;
	public static final byte TYPE_INBOX			= 8;
	public static final byte TYPE_FILEXFER		= 7;
	public static final byte TYPE_CERTIFICATE	= 9;
	public static final byte TYPE_UNKNOWN		= 10;
	
	// All Solr index fields
		public static final String FIELD_ID = "id";
		public static final String FIELD_NAME = "name";
		public static final String FIELD_INTERCEPT = "intercept";
		public static final String FIELD_INTERCEPT_NAME	= "interceptName";
		public static final String FIELD_INTERCEPT_DESCRIPTION = "interceptDesc";
		public static final String FIELD_SOI = "soi";
		public static final String FIELD_CONTENT_URL = "contentUrl";
		public static final String FIELD_CONTENT_ENCODING = "contentEncoding";
		public static final String FIELD_SOI_NAME = "soiName";
		public static final String FIELD_CLIENT_PORT = "clientPort";
		public static final String FIELD_SERVER_PORT = "serverPort";
		public static final String FIELD_TIMEZONE_OFFSET = "timezoneOffset";
		public static final String FIELD_TYPE =	"type";
		public static final String FIELD_SUBTYPE = "subType";
		public static final String FIELD_PROVIDER = "provider";
		public static final String FIELD_MEDIA = "media";
		public static final String FIELD_DATE =	"date";
		public static final String FIELD_CONTENT = "content";
		public static final String FIELD_TITLE = "title";
		public static final String FIELD_CLIENT_IP = "clientIP";
		public static final String FIELD_SERVER_IP = "serverIP";
		public static final String FIELD_SUBJECT = "subject";
		public static final String FIELD_SERVER	= "server";
		public static final String FIELD_LOGIN = "login";
		public static final String FIELD_ALIAS = "alias";
		public static final String FIELD_CORRELATION = "correlation";
		public static final String FIELD_CAPTURE = "capture";
		public static final String FIELD_MSISDN	= "msisdn";
		public static final String FIELD_MSISDN_CALLEE	= "msisdnCallee";
		public static final String FIELD_MSISDN_FORWARDED  = "msisdnForwarded";
		public static final String FIELD_DIALED	= "dialed";
		public static final String FIELD_IMSI =	"imsi";
		public static final String FIELD_IMSI_CALLEE = "imsiCallee";
		public static final String FIELD_IMSI_FORWARDED	= "imsiForwarded";
		public static final String FIELD_IMEI =	"imei";
		public static final String FIELD_IMEI_CALLEE	= "imeiCallee";
		public static final	String FIELD_IMEI_FORWARDED  = "imeiForwarded";
		public static final String FIELD_CELL =	"cell";
		public static final String FIELD_DURATION = "duration";
		public static final String FIELD_SENDER	= "sender";
		public static final String FIELD_RECIPIENT = "recipient";
		public static final String FIELD_LOCATION = "location";
		public static final String FIELD_THREATS = "threats";
		public static final String FIELD_THREATS_S = "threats_s";
		public static final String FIELD_TAGS =	"tags";
		public static final String FIELD_TAGS_S	= "tags_s";
		public static final String FIELD_TAGNAMES = "tagNames";
		public static final String FIELD_SERVERPORT = "serverPort";
		public static final String FIELD_CLIENTPORT = "clientPort";
		public static final String FIELD_USER_AGENT = "userAgent";
		public static final String FIELD_CALL_DIRECTION	= "callDirection";
		public static final String FIELD_DIRECTION	= "direction";
		public static final String FIELD_CALL_STARTTIME	= "starttime";
		public static final String FIELD_CALL_ENDTIME =	"endtime";
		public static final String FIELD_CALL_TERMINATIONREASON	= "callTerminationReason";
		public static final String FIELD_CALL_TELURI = "teluri";
		public static final String FIELD_TEL_URI_CALLEE	  = "teluriCallee";
		public static final	String FIELD_TEL_URI_FORWARDED	 = "teluriForwarded";
		public static final String FIELD_CALL_SIPURI = "sipurl";
		public static final String FIELD_SIP_URI_CALLEE	    = "sipurlCallee";
		public static final	String FIELD_SIP_URI_FORWARDED	 = "sipurlForwarded";
		public static final String FIELD_CALL_STATUS = "callStatus";
		public static final String FIELD_FORWARDED = "forwarded";
		public static final String FIELD_DOCUMENT_STATUS = "status";
		public static final String FIELD_RING_DURATION = "ringDuration";
		public static final String FIELD_CONVERSATION_DURATION = "conversationDuration";
		public static final String FIELD_SUMMARY_NOTE	 = "summary";
		public static final String FIELD_TRASHED_TIME	 = "trashedTime";
		public static final String FIELD_GEOFENCE	 = "geofence";
		public static final String FIELD_APPSESSIONID= "appSessionID";
		public static final String FIELD_ENRICHED_DATA = "enrichedData";
		public static final String FIELD_TRANSPORT = "transport";
		public static final String FIELD_SUB_TYPE = "subType";
		
		//in-678
		public static final String FIELD_REQUEST_COOKIE		 = "reqCookie";
		public static final String FIELD_RESPONSE_COOKIE		 = "cookie";
		//end
		public static final String FIELD_TAP		 = "tap";
		public static final String FIELD_HTTP_METHOD	 = "method";
		public static final String FIELD_HTTP_REFERER	 = "httpReferer";
		public static final String FIELD_HTTP_RESPONSE_CODE	 = "responseCode";
		public static final String FIELD_CONTENT_TYPE	 = "contentType";
		public static final String FIELD_LANG	 = "lang";
		public static final String FIELD_SIZE	 = "size";
		public static final String FIELD_CALL_TYPE = "callType";
		public static final String FIELD_CALL_ID = "callID";
		public static final String FIELD_RELEVANCE = "relevance";
		public static final String FIELD_IRI_MSISDN     = "iRIMsisdn";
		public static final String FIELD_IRI_IMSI     	= "iRIImsi";
		public static final String FIELD_IRI_IMEI     	= "iRIImei";
		public static final String FIELD_IRI_IP_ACCESS 	= "iRIIpAccessType";
		public static final String FIELD_IRI_IP_ADDRESS = "iRIIpAddress";

		//dns solrfields
		public static final String FIELD_DNS_QUERY     = "dnsQuery";
		public static final String FIELD_DNS_QTYPE     	= "dnsQType";
		public static final String FIELD_DNS_QCLASS     	= "dnsQclass";
		public static final String FIELD_DNS_NAME 	= "dnsName";
		public static final String FIELD_DNS_TYPE = "dnsType";
		public static final String FIELD_DNS_CLASS     	= "dnsClass";
		public static final String FIELD_DNS_DATA 	= "dnsData";
		public static final String FIELD_DNS_TTL = "dnsTTL";
		public static final String FIELD_DNS_QREPLY_CODE = "dnsQReplyCode";
		public static final String FIELD_DNS_QRESPONSE_TIME = "dnsQResponseTime";
		//end
		
		//solr fields IN-561 IN-69 
		public static final String FIELD_GENRE     		= "genre";
		public static final String FIELD_INGEST_DATE     		= "ingestDate";
		public static final String FIELD_GLOBAL_IDENTIFICATION_NUMBER 	= "globalIdentificationNumber";
		public static final String FIELD_VERSION_NUMBER 		= "versionNumber";
		//end
		
		//solr fields for unknown protocol
		public static final String FIELD_UNKNOWN_PRTOSTACK     = "protocolStack";
		
		//solr fields for websearch
		public static final String FIELD_WEB_SEARCH_QUERY_TEXT = "webSearchQueryText";
		public static final String FIELD_WEB_SEARCH_QUERY_TYPE = "webSearchQueryType";
		
		// solr fields for webmap
		public static final String FIELD_WEB_MAP_ACCOUNT_LOGIN = "webMapAccountLogin";
		public static final String FIELD_WEB_MAP_ACCOUNT_NAME = "webMapAccountName";
		public static final String FIELD_WEB_MAP_ACCOUNT_LOCATION = "webMapAccountLocation";
		public static final String FIELD_WEB_MAP_ACCOUNT_CITY = "webMapAccountCity";
		public static final String FIELD_WEB_MAP_ACCOUNT_STATE = "webMapAccountState";
		public static final String FIELD_WEB_MAP_ACCOUNT_POSTAL = "webMapAccountPostal";
		public static final String FIELD_WEB_MAP_ACCOUNT_COUNTRY = "webMapAccountCountry";
		public static final String FIELD_WEB_MAP_ACCOUNT_ADDRESS = "webMapAccountAddress";
		public static final String FIELD_WEB_MAP_LOCATION_QUERY_TEXT = "webMapLocationQueryText";
		public static final String FIELD_WEB_MAP_LOCATION_QUERY_CATEGORY = "webMapLocationQueryCategory";
		public static final String FIELD_WEB_MAP_LOCATION_WAYPOINT_TYPE = "webMapLocationWaypointType";
		public static final String FIELD_WEB_MAP_LOCATION = "webMapLocation";
		public static final String FIELD_WEB_MAP_LOCATION_STREET = "webMapLocationStreet";
		public static final String FIELD_WEB_MAP_LOCATION_CITY = "webMapLocationCity";
		public static final String FIELD_WEB_MAP_LOCATION_STATE = "webMapLocationState";
		public static final String FIELD_WEB_MAP_LOCATION_COUNTRY = "webMapLocationCountry";
		public static final String FIELD_WEB_MAP_LOCATION_POSTAL = "webMapLocationPostal";
		public static final String FIELD_WEB_MAP_LOCATION_ADDRESS = "webMapLocationAddress";
	
	private long id;
	
	private String name;
	
	private int type;
	
	private String provider;
	
	private String title;
	
	private Date date;
	
	private String contentUrl;
	
	private String contentEncoding;
	
	private String contentType;
	
	private String serverIP;
	
	private String clientIP;
	
	private int relevance;
	
	private String correlation;
	
	private int capture;
	
	private int clientPort;
	
	private int serverPort;
	
	private int timezoneOffset;
	
	private String threats;
	
	private String enrichedData;
	
	private Date trashedTime;
	
	private String userAgent;
	
	private int contentSize;
	
	private String transport;
	
	private int direction;
	
	private String subType;
	
	private Date ingestDate;
	
	private String globalIdentificationNumber;
	
	private String versionNumber;
	
	private String geofence;
	
	private String appSessionID;
	
	private long intercept;
	
	private String interceptName;
	
	private String interceptDesc;
	
	private long soi;
	
	private String soiName;
	
	private String subject;
	
	private String locations;
	
	private String tap;
	
	private String genre;
	
	private String lang;
	
	private String content;
	
	private String sender;
	
	private String recipient;
	
	private int status;
	
	private String tagNames;
	
	@JsonCreator
	public Document(long id, String name, int type, String provider,
			String title, Date date, String contentUrl, String contentEncoding,
			String contentType, String serverIP, String clientIP,
			int relevance, String correlation, int capture, int clientPort,
			int serverPort, int timezoneOffset, String threats,
			String enrichedData, Date trashedTime, String userAgent,
			int contentSize, String transport, int direction, String subType,
			Date ingestDate, String globalIdentificationNumber,
			String versionNumber, String geofence, String appSessionID,
			long intercept, String interceptName, String interceptDesc,
			long soi, String soiName, String subject, String locations,
			String tap, String genre, String lang) {
		super();
		this.id = id;
		this.name = name;
		this.type = type;
		this.provider = provider;
		this.title = title;
		this.date = date;
		this.contentUrl = contentUrl;
		this.contentEncoding = contentEncoding;
		this.contentType = contentType;
		this.serverIP = serverIP;
		this.clientIP = clientIP;
		this.relevance = relevance;
		this.correlation = correlation;
		this.capture = capture;
		this.clientPort = clientPort;
		this.serverPort = serverPort;
		this.timezoneOffset = timezoneOffset;
		this.threats = threats;
		this.enrichedData = enrichedData;
		this.trashedTime = trashedTime;
		this.userAgent = userAgent;
		this.contentSize = contentSize;
		this.transport = transport;
		this.direction = direction;
		this.subType = subType;
		this.ingestDate = ingestDate;
		this.globalIdentificationNumber = globalIdentificationNumber;
		this.versionNumber = versionNumber;
		this.geofence = geofence;
		this.appSessionID = appSessionID;
		this.intercept = intercept;
		this.interceptName = interceptName;
		this.interceptDesc = interceptDesc;
		this.soi = soi;
		this.soiName = soiName;
		this.subject = subject;
		this.locations = locations;
		this.tap = tap;
		this.genre = genre;
		this.lang = lang;
	}

	/*public Document(long id, String name, int type, String provider, String title, Date date, String contentUrl, String contentEncoding,
			String contentType, String serverIP, String clientIP, int relevance, String correlation, int capture, int clientPort, int serverPort,
			int timezoneOffset, String threats, String enrichedData, Date trashedTime, String userAgent, int contentSize, String transport,
			int direction, String subType, Date ingestDate, String globalIdentificationNumber, String versionNumber, String geofence, String appSessionID) {
		this.setId(id);
	}*/
	
	@JsonCreator
	public Document(long id) {
		this.setId(id);
	}

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the type
	 */
	public int getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(int type) {
		this.type = type;
	}

	/**
	 * @return the provider
	 */
	public String getProvider() {
		return provider;
	}

	/**
	 * @param provider the provider to set
	 */
	public void setProvider(String provider) {
		this.provider = provider;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the date
	 */
	public Date getDate() {
		return date;
	}

	/**
	 * @param date the date to set
	 */
	public void setDate(Date date) {
		this.date = date;
	}

	/**
	 * @return the contentUrl
	 */
	public String getContentUrl() {
		return contentUrl;
	}

	/**
	 * @param contentUrl the contentUrl to set
	 */
	public void setContentUrl(String contentUrl) {
		this.contentUrl = contentUrl;
	}

	/**
	 * @return the contentEncoding
	 */
	public String getContentEncoding() {
		return contentEncoding;
	}

	/**
	 * @param contentEncoding the contentEncoding to set
	 */
	public void setContentEncoding(String contentEncoding) {
		this.contentEncoding = contentEncoding;
	}

	/**
	 * @return the contentType
	 */
	public String getContentType() {
		return contentType;
	}

	/**
	 * @param contentType the contentType to set
	 */
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	/**
	 * @return the serverIP
	 */
	public String getServerIP() {
		return serverIP;
	}

	/**
	 * @param serverIP the serverIP to set
	 */
	public void setServerIP(String serverIP) {
		this.serverIP = serverIP;
	}

	/**
	 * @return the clientIP
	 */
	public String getClientIP() {
		return clientIP;
	}

	/**
	 * @param clientIP the clientIP to set
	 */
	public void setClientIP(String clientIP) {
		this.clientIP = clientIP;
	}

	/**
	 * @return the relevance
	 */
	public int getRelevance() {
		return relevance;
	}

	/**
	 * @param relevance the relevance to set
	 */
	public void setRelevance(int relevance) {
		this.relevance = relevance;
	}

	/**
	 * @return the correlation
	 */
	public String getCorrelation() {
		return correlation;
	}

	/**
	 * @param correlation the correlation to set
	 */
	public void setCorrelation(String correlation) {
		this.correlation = correlation;
	}

	/**
	 * @return the capture
	 */
	public int getCapture() {
		return capture;
	}

	/**
	 * @param capture the capture to set
	 */
	public void setCapture(int capture) {
		this.capture = capture;
	}

	/**
	 * @return the clientPort
	 */
	public int getClientPort() {
		return clientPort;
	}

	/**
	 * @param clientPort the clientPort to set
	 */
	public void setClientPort(int clientPort) {
		this.clientPort = clientPort;
	}

	/**
	 * @return the serverPort
	 */
	public int getServerPort() {
		return serverPort;
	}

	/**
	 * @param serverPort the serverPort to set
	 */
	public void setServerPort(int serverPort) {
		this.serverPort = serverPort;
	}

	/**
	 * @return the timezoneOffset
	 */
	public int getTimezoneOffset() {
		return timezoneOffset;
	}

	/**
	 * @param timezoneOffset the timezoneOffset to set
	 */
	public void setTimezoneOffset(int timezoneOffset) {
		this.timezoneOffset = timezoneOffset;
	}

	/**
	 * @return the threats
	 */
	public String getThreats() {
		return threats;
	}

	/**
	 * @param threats the threats to set
	 */
	public void setThreats(String threats) {
		this.threats = threats;
	}

	/**
	 * @return the enrichedData
	 */
	public String getEnrichedData() {
		return enrichedData;
	}

	/**
	 * @param enrichedData the enrichedData to set
	 */
	public void setEnrichedData(String enrichedData) {
		this.enrichedData = enrichedData;
	}

	/**
	 * @return the trashedTime
	 */
	public Date getTrashedTime() {
		return trashedTime;
	}

	/**
	 * @param trashedTime the trashedTime to set
	 */
	public void setTrashedTime(Date trashedTime) {
		this.trashedTime = trashedTime;
	}

	/**
	 * @return the userAgent
	 */
	public String getUserAgent() {
		return userAgent;
	}

	/**
	 * @param userAgent the userAgent to set
	 */
	public void setUserAgent(String userAgent) {
		this.userAgent = userAgent;
	}

	/**
	 * @return the contentSize
	 */
	public int getContentSize() {
		return contentSize;
	}

	/**
	 * @param contentSize the contentSize to set
	 */
	public void setContentSize(int contentSize) {
		this.contentSize = contentSize;
	}

	/**
	 * @return the transport
	 */
	public String getTransport() {
		return transport;
	}

	/**
	 * @param transport the transport to set
	 */
	public void setTransport(String transport) {
		this.transport = transport;
	}

	/**
	 * @return the direction
	 */
	public int getDirection() {
		return direction;
	}

	/**
	 * @param direction the direction to set
	 */
	public void setDirection(int direction) {
		this.direction = direction;
	}

	/**
	 * @return the subType
	 */
	public String getSubType() {
		return subType;
	}

	/**
	 * @param subType the subType to set
	 */
	public void setSubType(String subType) {
		this.subType = subType;
	}

	/**
	 * @return the globalIdentificationNumber
	 */
	public String getGlobalIdentificationNumber() {
		return globalIdentificationNumber;
	}

	/**
	 * @param globalIdentificationNumber the globalIdentificationNumber to set
	 */
	public void setGlobalIdentificationNumber(String globalIdentificationNumber) {
		this.globalIdentificationNumber = globalIdentificationNumber;
	}

	/**
	 * @return the ingestDate
	 */
	public Date getIngestDate() {
		return ingestDate;
	}

	/**
	 * @param ingestDate the ingestDate to set
	 */
	public void setIngestDate(Date ingestDate) {
		this.ingestDate = ingestDate;
	}

	/**
	 * @return the versionNumber
	 */
	public String getVersionNumber() {
		return versionNumber;
	}

	/**
	 * @param versionNumber the versionNumber to set
	 */
	public void setVersionNumber(String versionNumber) {
		this.versionNumber = versionNumber;
	}

	/**
	 * @return the geofence
	 */
	public String getGeofence() {
		return geofence;
	}

	/**
	 * @param geofence the geofence to set
	 */
	public void setGeofence(String geofence) {
		this.geofence = geofence;
	}

	/**
	 * @return the appSessionID
	 */
	public String getAppSessionID() {
		return appSessionID;
	}

	/**
	 * @param appSessionID the appSessionID to set
	 */
	public void setAppSessionID(String appSessionID) {
		this.appSessionID = appSessionID;
	}

	/**
	 * @return the intercept
	 */
	public long getIntercept() {
		return intercept;
	}

	/**
	 * @param intercept the intercept to set
	 */
	public void setIntercept(long intercept) {
		this.intercept = intercept;
	}

	/**
	 * @return the interceptName
	 */
	public String getInterceptName() {
		return interceptName;
	}

	/**
	 * @param interceptName the interceptName to set
	 */
	public void setInterceptName(String interceptName) {
		this.interceptName = interceptName;
	}

	/**
	 * @return the interceptDesc
	 */
	public String getInterceptDesc() {
		return interceptDesc;
	}

	/**
	 * @param interceptDesc the interceptDesc to set
	 */
	public void setInterceptDesc(String interceptDesc) {
		this.interceptDesc = interceptDesc;
	}

	/**
	 * @return the soi
	 */
	public long getSoi() {
		return soi;
	}

	/**
	 * @param soi the soi to set
	 */
	public void setSoi(long soi) {
		this.soi = soi;
	}

	/**
	 * @return the soiName
	 */
	public String getSoiName() {
		return soiName;
	}

	/**
	 * @param soiName the soiName to set
	 */
	public void setSoiName(String soiName) {
		this.soiName = soiName;
	}

	/**
	 * @return the subject
	 */
	public String getSubject() {
		return subject;
	}

	/**
	 * @param subject the subject to set
	 */
	public void setSubject(String subject) {
		this.subject = subject;
	}

	/**
	 * @return the locations
	 */
	public String getLocations() {
		return locations;
	}

	/**
	 * @param locations the locations to set
	 */
	public void setLocations(String locations) {
		this.locations = locations;
	}

	/**
	 * @return the tap
	 */
	public String getTap() {
		return tap;
	}

	/**
	 * @param tap the tap to set
	 */
	public void setTap(String tap) {
		this.tap = tap;
	}

	/**
	 * @return the genre
	 */
	public String getGenre() {
		return genre;
	}

	/**
	 * @param genre the genre to set
	 */
	public void setGenre(String genre) {
		this.genre = genre;
	}

	/**
	 * @return the lang
	 */
	public String getLang() {
		return lang;
	}

	/**
	 * @param lang the lang to set
	 */
	public void setLang(String lang) {
		this.lang = lang;
	}

	/**
	 * @return the content
	 */
	public String getContent() {
		return content;
	}

	/**
	 * @param content the content to set
	 */
	public void setContent(String content) {
		this.content = content;
	}

	/**
	 * @return the sender
	 */
	public String getSender() {
		return sender;
	}

	/**
	 * @param sender the sender to set
	 */
	public void setSender(String sender) {
		this.sender = sender;
	}

	/**
	 * @return the recipient
	 */
	public String getRecipient() {
		return recipient;
	}

	/**
	 * @param recipient the recipient to set
	 */
	public void setRecipient(String recipient) {
		this.recipient = recipient;
	}

	/**
	 * @return the status
	 */
	public int getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(int status) {
		this.status = status;
	}

	/**
	 * @return the tagNames
	 */
	public String getTagNames() {
		return tagNames;
	}

	/**
	 * @param tagNames the tagNames to set
	 */
	public void setTagNames(String tagNames) {
		this.tagNames = tagNames;
	}
	
	
}
