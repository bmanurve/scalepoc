package com.ss8.experiments.indexer;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrRequest;
import org.apache.solr.client.solrj.SolrQuery.ORDER;
import org.apache.solr.client.solrj.impl.CloudSolrClient;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.SolrInputDocument;

public class SolrQueriesServicer implements Runnable {

	private int userId = 0;
	
	private int totalIdsSolrQueries = 0;
	
	private int totalBatchQueries = 0;
	
	private int totalIdQueries = 0;
	
	private int totalUpdateQueries = 0;
	
	private int totalFacetQueries = 0;
	
	private long queryLogStartTime = new Date().getTime();
	
	private long totalFacetQueriesTime = 0;
	
	private long maxFacetQueryTime = 0;
	
	private long totalIdsSolrQueriesTime = 0;
	
	private long maxIdsSolrQueryTime = 0;
	
	private long totalBatchQueriesTime = 0;
	
	private long maxBatchQueryTime = 0;
	
	private long totalIdQueriesTime = 0;
	
	private long maxIdQueryTime = 0;
	
	private long totalUpdateQueriesTime = 0;
	
	private long maxUpdateQueryTime = 0;

	public SolrQueriesServicer(final int userId) {
		this.userId = userId;
	}

	@Override
	public void run() {
		// Execute multiple queries like sort by id, sort by creation date, time range queries and facet queries.
		try {
			CloudSolrClient server = SolrUtils.getSolrServer();
			SolrQuery query = new SolrQuery();
			if (this.userId % 10 == 0) {
				query.setQuery("((intercept : 1) OR (intercept : 2) OR (intercept : 3) OR (intercept : 4) OR (intercept : 5)) AND (date:[NOW-2HOURS TO NOW])");
			} else if (this.userId % 10 == 1) {
				query.setQuery("((intercept : 6) OR (intercept : 7) OR (intercept : 8) OR (intercept : 9) OR (intercept : 10)) AND (date:[NOW-2HOURS TO NOW])");
			} else if (this.userId % 10 == 2) {
				query.setQuery("((intercept : 11) OR (intercept : 12) OR (intercept : 13) OR (intercept : 14) OR (intercept : 15)) AND (date:[NOW-2HOURS TO NOW])");
			} else if (this.userId % 10 == 3) {
				query.setQuery("((intercept : 16) OR (intercept : 17) OR (intercept : 18) OR (intercept : 19) OR (intercept : 20)) AND (date:[NOW-2HOURS TO NOW])");
			} else if (this.userId % 10 == 4) {
				query.setQuery("((intercept : 21) OR (intercept : 22) OR (intercept : 23) OR (intercept : 24) OR (intercept : 25)) AND (date:[NOW-2HOURS TO NOW])");
			} else if (this.userId % 10 == 5) {
				query.setQuery("((intercept : 26) OR (intercept : 27) OR (intercept : 28) OR (intercept : 29) OR (intercept : 30)) AND (date:[NOW-2HOURS TO NOW])");
			} else if (this.userId % 10 == 6) {
				query.setQuery("((intercept : 31) OR (intercept : 32) OR (intercept : 33) OR (intercept : 34) OR (intercept : 35)) AND (date:[NOW-2HOURS TO NOW])");
			} else if (this.userId % 10 == 7) {
				query.setQuery("((intercept : 36) OR (intercept : 37) OR (intercept : 38) OR (intercept : 39) OR (intercept : 40)) AND (date:[NOW-2HOURS TO NOW])");
			} else if (this.userId % 10 == 8) {
				query.setQuery("((intercept : 41) OR (intercept : 42) OR (intercept : 43) OR (intercept : 44) OR (intercept : 45)) AND (date:[NOW-2HOURS TO NOW])");
				query.addFacetField("provider");
				query.addFacetField("sender");
				query.addFacetField("recipient");
				query.addFacetField("type");
				query.addFacetField("status");
				query.addFacetField("interceptName");
				query.setFacetMinCount(1);
				query.setFacetSort("true");
				query.setFacetLimit(-1); // max limit
				query.setRows(0);
				QueryResponse response = server.query(query, SolrRequest.METHOD.POST);
				if (response.getQTime() > 1000) {
					System.out.println(new Date() + " SolrQueriesServicer :: Facet Query time from solr - " + response.getQTime());
				}
				totalFacetQueriesTime+= response.getQTime();
				if (maxFacetQueryTime < response.getQTime()) {
					maxFacetQueryTime = response.getQTime();
				}
				totalFacetQueries++;
				// Total queries executed per category. Log every 15 minutes.
				if ((System.currentTimeMillis() - queryLogStartTime) >= (900 * 1000)) {
					System.out.println(new Date() + " SolrMySqlQueriesServicer :: Total facet queries executed so far for User-" + this.userId + " -----> " + totalFacetQueries);
					double averageFacetQueriesTime = totalFacetQueriesTime/totalFacetQueries;
					System.out.println(new Date() + " SolrMySqlQueriesServicer :: Average facet queries time that took for the User-" + this.userId + " is -----> " + averageFacetQueriesTime);
					System.out.println(new Date() + " SolrMySqlQueriesServicer :: Max facet query time that took for the User-" + this.userId + " is -----> " + maxFacetQueryTime);
					queryLogStartTime = new Date().getTime();
				}
				// Sleep for 30 seconds before returning.
				Thread.sleep(30000);
				return;
			} else if (this.userId % 10 == 9) {
				query.setQuery("((intercept : 46) OR (intercept : 47) OR (intercept : 48) OR (intercept : 49) OR (intercept : 50)) AND (date:[NOW-2HOURS TO NOW])");
				query.addFacetField("provider");
				query.addFacetField("sender");
				query.addFacetField("recipient");
				query.addFacetField("type");
				query.addFacetField("status");
				query.addFacetField("interceptName");
				query.setFacetMinCount(1);
				query.setFacetSort("true");
				query.setFacetLimit(-1); // max limit
				query.setRows(0);
				QueryResponse response = server.query(query, SolrRequest.METHOD.POST);
				if (response.getQTime() > 1000) {
					System.out.println(new Date() + " SolrQueriesServicer :: Facet Query time from solr - " + response.getQTime());
				}
				totalFacetQueriesTime+= response.getQTime();
				if (maxFacetQueryTime < response.getQTime()) {
					maxFacetQueryTime = response.getQTime();
				}
				totalFacetQueries++;
				// Total queries executed per category. Log every 15 minutes.
				if ((System.currentTimeMillis() - queryLogStartTime) >= (900 * 1000)) {
					System.out.println(new Date() + " SolrMySqlQueriesServicer :: Total facet queries executed so far for User-" + this.userId + " -----> " + totalFacetQueries);
					double averageFacetQueriesTime = totalFacetQueriesTime/totalFacetQueries;
					System.out.println(new Date() + " SolrMySqlQueriesServicer :: Average facet queries time that took for the User-" + this.userId + " is -----> " + averageFacetQueriesTime);
					System.out.println(new Date() + " SolrMySqlQueriesServicer :: Max facet query time that took for the User-" + this.userId + " is -----> " + maxFacetQueryTime);
					queryLogStartTime = new Date().getTime();
				}
				// Sleep for 30 seconds before returning.
				Thread.sleep(30000);
				return;
			}

			query.addSort("date", ORDER.desc);
			query.setRows(1000);
			query.setFields("id");
			QueryResponse response = server.query(query, SolrRequest.METHOD.POST);
			if (response.getQTime() > 1000) {
				System.out.println(new Date() + " SolrQueriesServicer :: Query time for getting data for 1000 ids from solr - " + response.getQTime());
			}
			totalIdsSolrQueries++;
			totalIdsSolrQueriesTime+= response.getQTime();
			if (maxIdsSolrQueryTime < response.getQTime()) {
				maxIdsSolrQueryTime = response.getQTime();
			}
			// Get the documents for the 1000 rows by batch.
			ArrayList<Long> batch1 = new ArrayList<Long>();
			ArrayList<Long> batch2 = new ArrayList<Long>();
			ArrayList<Long> batch3 = new ArrayList<Long>();
			int totalIndex = 1;
			for (SolrDocument solrDocument : response.getResults()) {
				if (totalIndex <= 300) {
					batch1.add((long) solrDocument.getFieldValue(Document.FIELD_ID));
				} else if (totalIndex > 300 && totalIndex <= 600) {
					batch2.add((long) solrDocument.getFieldValue(Document.FIELD_ID));
				} else {
					batch3.add((long) solrDocument.getFieldValue(Document.FIELD_ID));
				}
				totalIndex++;
			}

			// System.out.println(new Date() + " - SOLR total docs size " +
			// response.getResults().size());

			// Get the results from Solr.
			SolrDocumentList docList = null;
			if (batch1.size() > 0) {
				String batch1Query = "(";
				for (int index = 0; index < batch1.size(); index++) {
					batch1Query += "id : " + batch1.get(index);
					if (index != (batch1.size() - 1)) {
						batch1Query += " OR ";
					}
				}
				batch1Query += ")";
				// System.out.println("Query - " + batch1Query);
				query = new SolrQuery();
				query.setQuery(batch1Query);
				query.setRows(batch1.size());
				query.setFields("id", "callStatus", "callType", "clientIP", "clientPort", "clientEncoding", "contentUrl", 
						"date", "correlation", "deleted", "imei", "imeiCallee", "indexed", "imsi", "interceptName", "location", "name", "locationJSON", "media", "recipient", "relevance", "resume");
				response = server.query(query, SolrRequest.METHOD.POST);
				if (response.getQTime() > 1000) {
					System.out.println(new Date() + " SolrQueriesServicer :: Batch Query to fetch " + batch1.size() + " took - " + response.getQTime() + " milliseconds");
				}
				totalBatchQueries++;
				totalBatchQueriesTime+= response.getQTime();
				if (maxBatchQueryTime < response.getQTime()) {
					maxBatchQueryTime = response.getQTime();
				}
				// Get the top 100 rows and then change from new to visited and also trash and tag few events.
				docList = response.getResults();
			}

			if (batch2.size() > 0) {
				String batch2Query = "(";
				for (int index = 0; index < batch2.size(); index++) {
					batch2Query += "id : " + batch2.get(index);
					if (index != (batch2.size() - 1)) {
						batch2Query += " OR ";
					}
				}
				batch2Query += ")";
				query = new SolrQuery();
				query.setQuery(batch2Query);
				query.setRows(batch2.size());
				query.setFields("id", "callStatus", "callType", "clientIP", "clientPort", "clientEncoding", "contentUrl", 
						"date", "correlation", "deleted", "imei", "imeiCallee", "indexed", "imsi", "interceptName", "location", "name", "locationJSON", "media", "recipient", "relevance", "resume");
				response = server.query(query, SolrRequest.METHOD.POST);
				if (response.getQTime() > 1000) {
					System.out.println(new Date() + " SolrQueriesServicer :: Batch Query to fetch " + batch2.size() + " took - " + response.getQTime() + " milliseconds");
				}
				totalBatchQueries++;
				totalBatchQueriesTime+= response.getQTime();
				if (maxBatchQueryTime < response.getQTime()) {
					maxBatchQueryTime = response.getQTime();
				}
			}

			if (batch3.size() > 0) {
				String batch3Query = "(";
				for (int index = 0; index < batch3.size(); index++) {
					batch3Query += "id : " + batch3.get(index);
					if (index != (batch3.size() - 1)) {
						batch3Query += " OR ";
					}
				}
				batch3Query += ")";
				query = new SolrQuery();
				query.setQuery(batch3Query);
				query.setRows(batch3.size());
				query.setFields("id", "callStatus", "callType", "clientIP", "clientPort", "clientEncoding", "contentUrl", 
						"date", "correlation", "deleted", "imei", "imeiCallee", "indexed", "imsi", "interceptName", "location", "name", "locationJSON", "media", "recipient", "relevance", "resume");
				response = server.query(query, SolrRequest.METHOD.POST);
				if (response.getQTime() > 1000) {
					System.out.println(new Date() + " SolrQueriesServicer :: Batch Query to fetch " + batch3.size() + " took - " + response.getQTime() + " milliseconds");
				}
				totalBatchQueries++;
				totalBatchQueriesTime+= response.getQTime();
				if (maxBatchQueryTime < response.getQTime()) {
					maxBatchQueryTime = response.getQTime();
				}
			}

			if (docList != null && docList.size() > 0) {
				int index = 0;
				for (SolrDocument solrDocument : docList) {
					if (index >= 100) {
						break;
					}
					query = new SolrQuery();
					query.setQuery("(id : " + (long) solrDocument.getFieldValue(Document.FIELD_ID) + " )");
					response = server.query(query, SolrRequest.METHOD.POST);
					if (response.getQTime() > 1000) {
						System.out.println(new Date() + " SolrQueriesServicer :: Query time for getting data for 1 id - " + response.getQTime());
					}
					totalIdQueries++;
					totalIdQueriesTime+= response.getQTime();
					if (maxIdQueryTime < response.getQTime()) {
						maxIdQueryTime = response.getQTime();
					}
					Thread.sleep(5000);
					SolrInputDocument solrInputDocument = new SolrInputDocument();
					solrInputDocument.addField(Document.FIELD_ID, (long) solrDocument.getFieldValue(Document.FIELD_ID));
					HashMap<String, Object> oper = new HashMap<String, Object>();
					if (index < 50) {
						oper.put("set", 1);
					} else if (index >= 50 && index <= 80) {
						oper.put("set", 3);
						HashMap<String, Object> trashOperation = new HashMap<String, Object>();
						trashOperation.put("set", new Date());
						solrInputDocument.removeField(Document.FIELD_TRASHED_TIME);
						solrInputDocument.addField(Document.FIELD_TRASHED_TIME, trashOperation);
					} else {
						oper.put("set", 2);
						HashMap<String, Object> tagOperation = new HashMap<String, Object>();
						tagOperation.put("set", "highmark");
						solrInputDocument.removeField(Document.FIELD_TAGNAMES);
						solrInputDocument.addField(Document.FIELD_TAGNAMES, tagOperation);
					}
					solrInputDocument.removeField(Document.FIELD_DOCUMENT_STATUS);
					solrInputDocument.addField(Document.FIELD_DOCUMENT_STATUS, oper);
					// update time
					HashMap<String, Object> updateTimeOperation = new HashMap<String, Object>();
					updateTimeOperation.put("set", new Date());
					solrInputDocument.removeField("updateDate");
					solrInputDocument.addField("updateDate", updateTimeOperation);
					long startTime = System.currentTimeMillis();
					server.add(solrInputDocument);
					long endTime = System.currentTimeMillis();
					totalUpdateQueries++;
					totalUpdateQueriesTime+= (endTime - startTime);
					if (maxUpdateQueryTime < (endTime - startTime)) {
						maxUpdateQueryTime = (endTime - startTime);
					}
					index++;
				}
			}
			// Total queries executed per category. Log every 15 minutes.
			if ((System.currentTimeMillis() - queryLogStartTime) >= (900 * 1000)) {
				double averageIdsSolrQueriesTime = totalIdsSolrQueriesTime/totalIdsSolrQueries;
				System.out.println(new Date() + " SolrQueriesServicer :: Average ids queries to solr that took for the User-" + this.userId + " is -----> " + averageIdsSolrQueriesTime);
				System.out.println(new Date() + " SolrQueriesServicer :: Max solr ids query time that took for the User-" + this.userId + " is -----> " + maxIdsSolrQueryTime);
				System.out.println(new Date() + " SolrQueriesServicer :: Total ids queries to solr executed so far for User-" + this.userId + " -----> " + totalIdsSolrQueries);
				double averageBatchQueriesTime = totalBatchQueriesTime/totalBatchQueries;
				System.out.println(new Date() + " SolrQueriesServicer :: Average batch queries time that took for the User-" + this.userId + " is -----> " + averageBatchQueriesTime);
				System.out.println(new Date() + " SolrQueriesServicer :: Max batch query time that took for the User-" + this.userId + " is -----> " + maxBatchQueryTime);
				System.out.println(new Date() + " SolrQueriesServicer :: Total Batch queries executed so far for User-" + this.userId + " -----> " + totalBatchQueries);
				double averageIdQueriesTime = totalIdQueriesTime/totalIdQueries;
				System.out.println(new Date() + " SolrQueriesServicer :: Average id queries time that took for the User-" + this.userId + " is -----> " + averageIdQueriesTime);
				System.out.println(new Date() + " SolrQueriesServicer :: Max id query time that took for the User-" + this.userId + " is -----> " + maxIdQueryTime);
				System.out.println(new Date() + " SolrQueriesServicer :: Total id queries executed so far for User-" + this.userId + " -----> " + totalIdQueries);
				double averageUpdateQueriesTime = totalUpdateQueriesTime/totalUpdateQueries;
				System.out.println(new Date() + " SolrQueriesServicer :: Average update queries time that took for the User-" + this.userId + " is -----> " + averageUpdateQueriesTime);
				System.out.println(new Date() + " SolrQueriesServicer :: Max update query time that took for the User-" + this.userId + " is -----> " + maxUpdateQueryTime);
				System.out.println(new Date() + " SolrQueriesServicer :: Total update queries executed so far for User-" + this.userId + " -----> " + totalUpdateQueries);
				queryLogStartTime = new Date().getTime();
			}
		} catch (Exception e) {
			System.out.println(new Date() + " SolrQueriesServicer :: Exception occurred with the stack trace - " + e.getLocalizedMessage());
			e.printStackTrace();
		}
	}
}
