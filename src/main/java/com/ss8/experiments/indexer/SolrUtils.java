/******************************************************************************
 *
 *
 * Copyright (C) 2013 SS8, INC. All Rights Reserved.
 * Web: http://www.ss8.com
 *
 *
 ******************************************************************************/
package com.ss8.experiments.indexer;

import org.apache.solr.client.solrj.impl.CloudSolrClient;

public class SolrUtils {
    private static String solrCollection = "intellego-test";
    private static String zkHostString = "10.0.156.191:2181";
    private static final int ZK_CLIENT_TIMEOUT = 30000;
    private static final int ZK_CONNECT_TIMEOUT = 30000;
    private static CloudSolrClient server = null;

    static {
        if(server == null) {
            try {
            	String zkHost = System.getProperty("zkHostString");
            	if (zkHost != null && !zkHost.isEmpty()) {
            		zkHostString = zkHost;
            	}
            	System.out.println("zkHostString " + zkHostString);
                server = new CloudSolrClient.Builder().withZkHost(zkHostString).build();
                server.setDefaultCollection(solrCollection);
                server.setZkClientTimeout(ZK_CLIENT_TIMEOUT);
                server.setZkConnectTimeout(ZK_CONNECT_TIMEOUT);
                server.connect();
            } catch (Exception e) {
                server = null;
                e.printStackTrace();
            }
        }
    }

    /**
     * Create a solr server for a core
     * @return SolrServer
     *
     */
    public static CloudSolrClient getSolrServer() throws Exception {
        if(server != null)
            return server;

        synchronized (SolrUtils.class){
            if(server == null){
                try {
                    server = new CloudSolrClient.Builder().withZkHost(zkHostString).build();
                    server.setDefaultCollection(solrCollection);
                    server.setZkClientTimeout(ZK_CLIENT_TIMEOUT);
                    server.setZkConnectTimeout(ZK_CONNECT_TIMEOUT);
                    server.connect();
                }
                catch (Exception e){
                    server = null;
                    throw e;
                }
            }
        }
        return server;
    }
}
