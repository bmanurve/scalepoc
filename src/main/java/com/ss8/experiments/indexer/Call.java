package com.ss8.experiments.indexer;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonCreator;

public class Call extends Document {
	
	// Call Solr fields
	public static final String FIELD_CALLEE = "callee";
	public static final String FIELD_CALLER = "caller";
	public static final String FIELD_CALLEE_NICK_NAME = "calleeNickname";
	public static final String FIELD_CALLER_NICK_NAME = "callerNickname";
	public static final String FIELD_CALLEE_USER_AGENT = "calleeUserAgent";
	public static final String FIELD_CALLER_USER_AGENT = "callerUserAgent";
	
	private String caller;
	
	private String callee;
	
	private String callerNickname;
	
	private String calleeNickname;
	
	private String callerUserAgent;
	
	private String calleeUserAgent;
	
	private String callerIp;
	
	private String calleeIp;
	
	private int callerPort;
	
	private int calleePort;
	
	private Date startTime;
	
	private Date endTime;
	
	private String audioEncoding;
	
	private String videoEncoding;
	
	private String callerE164;
	
	private String calleeE164;
	
	private String codec;
	
	private int ringDuration;
	
	private int conversationDuration;
	
	private String liveListenPipe;
	
	private String callInfo;
	
	private String callLocations;
	
	private String callParticipants;
	
	private String callProducts;
	
	private int callState;
	
	private int mediaType;
	
	private int mediaChunkID;
	
	private int mediaChunkStreamID;
	
	private int cseq;
	
	private String msisdn;
	
	private String dialed;
	
	private String other;
	
	private String imei;
	
	private String imsi;
	
	private Date connectTime;
	
	private Date disconnectTime;
	
	private int callType;
	
	private String serviceType;
	
	private int conditionCode;
	
	private int disconnectCode;
	
	private String pin;
	
	private String chargingID;
	
	private String sgsnAddress;
	
	private String ggsnAddress;
	
	private String operatorID;
	
	private int countryCallingCode;
	
	private String timezone;
	
	private String accessCode;
	
	private String trunkID;
	
	private String incomingCktID;
	
	private String outgoingCktID;
	
	private String routingArea;
	
	private String locationArea;
	
	private String cellIdentifier;
	
	private String finalPartyNumber;
	
	private String accountCode;
	
	private String teluri;
	
	private String sipurl;
	
	private String teluriCallee;
	
	private String sipurlCallee;
	
	private String teluriForwarded;
	
	private String sipurlForwarded;
	
	private String msisdnCallee;
	
	private String imsiCallee;
	
	private String imeiCallee;
	
	private String msisdnForwarded;
	
	private String imsiForwarded;
	
	private String imeiForwarded;
	
	private String forwarded;
	
	private int callStatus;

	@JsonCreator
	public Call(long id, String name, int type, String provider, String title,
			Date date, String contentUrl, String contentEncoding,
			String contentType, String serverIP, String clientIP,
			int relevance, String correlation, int capture, int clientPort,
			int serverPort, int timezoneOffset, String threats,
			String enrichedData, Date trashedTime, String userAgent,
			int contentSize, String transport, int direction, String subType,
			Date ingestDate, String globalIdentificationNumber,
			String versionNumber, String geofence, String appSessionID,
			long intercept, String interceptName, String interceptDesc,
			long soi, String soiName, String subject, String locations,
			String tap, String genre, String lang, String caller,
			String callee, String callerNickname, String calleeNickname,
			String callerUserAgent, String calleeUserAgent, String callerIp,
			String calleeIp, int callerPort, int calleePort, Date startTime,
			Date endTime, String audioEncoding, String videoEncoding,
			String callerE164, String calleeE164, String codec,
			int ringDuration, int conversationDuration, String liveListenPipe,
			String callInfo, String callLocations, String callParticipants,
			String callProducts, int callState, int mediaType,
			int mediaChunkID, int mediaChunkStreamID, int cseq, String msisdn,
			String dialed, String other, String imei, String imsi,
			Date connectTime, Date disconnectTime, int callType,
			String serviceType, int conditionCode, int disconnectCode,
			String pin, String chargingID, String sgsnAddress,
			String ggsnAddress, String operatorID, int countryCallingCode,
			String timezone, String accessCode, String trunkID,
			String incomingCktID, String outgoingCktID, String routingArea,
			String locationArea, String cellIdentifier,
			String finalPartyNumber, String accountCode, String teluri,
			String sipurl, String teluriCallee, String sipurlCallee,
			String teluriForwarded, String sipurlForwarded,
			String msisdnCallee, String imsiCallee, String imeiCallee,
			String msisdnForwarded, String imsiForwarded, String imeiForwarded,
			String forwarded, int callStatus) {
		super(id, name, type, provider, title, date, contentUrl,
				contentEncoding, contentType, serverIP, clientIP, relevance,
				correlation, capture, clientPort, serverPort, timezoneOffset,
				threats, enrichedData, trashedTime, userAgent, contentSize,
				transport, direction, subType, ingestDate,
				globalIdentificationNumber, versionNumber, geofence,
				appSessionID, intercept, interceptName, interceptDesc, soi,
				soiName, subject, locations, tap, genre, lang);
		this.caller = caller;
		this.callee = callee;
		this.callerNickname = callerNickname;
		this.calleeNickname = calleeNickname;
		this.callerUserAgent = callerUserAgent;
		this.calleeUserAgent = calleeUserAgent;
		this.callerIp = callerIp;
		this.calleeIp = calleeIp;
		this.callerPort = callerPort;
		this.calleePort = calleePort;
		this.startTime = startTime;
		this.endTime = endTime;
		this.audioEncoding = audioEncoding;
		this.videoEncoding = videoEncoding;
		this.callerE164 = callerE164;
		this.calleeE164 = calleeE164;
		this.codec = codec;
		this.ringDuration = ringDuration;
		this.conversationDuration = conversationDuration;
		this.liveListenPipe = liveListenPipe;
		this.callInfo = callInfo;
		this.callLocations = callLocations;
		this.callParticipants = callParticipants;
		this.callProducts = callProducts;
		this.callState = callState;
		this.mediaType = mediaType;
		this.mediaChunkID = mediaChunkID;
		this.mediaChunkStreamID = mediaChunkStreamID;
		this.cseq = cseq;
		this.msisdn = msisdn;
		this.dialed = dialed;
		this.other = other;
		this.imei = imei;
		this.imsi = imsi;
		this.connectTime = connectTime;
		this.disconnectTime = disconnectTime;
		this.callType = callType;
		this.serviceType = serviceType;
		this.conditionCode = conditionCode;
		this.disconnectCode = disconnectCode;
		this.pin = pin;
		this.chargingID = chargingID;
		this.sgsnAddress = sgsnAddress;
		this.ggsnAddress = ggsnAddress;
		this.operatorID = operatorID;
		this.countryCallingCode = countryCallingCode;
		this.timezone = timezone;
		this.accessCode = accessCode;
		this.trunkID = trunkID;
		this.incomingCktID = incomingCktID;
		this.outgoingCktID = outgoingCktID;
		this.routingArea = routingArea;
		this.locationArea = locationArea;
		this.cellIdentifier = cellIdentifier;
		this.finalPartyNumber = finalPartyNumber;
		this.accountCode = accountCode;
		this.teluri = teluri;
		this.sipurl = sipurl;
		this.teluriCallee = teluriCallee;
		this.sipurlCallee = sipurlCallee;
		this.teluriForwarded = teluriForwarded;
		this.sipurlForwarded = sipurlForwarded;
		this.msisdnCallee = msisdnCallee;
		this.imsiCallee = imsiCallee;
		this.imeiCallee = imeiCallee;
		this.msisdnForwarded = msisdnForwarded;
		this.imsiForwarded = imsiForwarded;
		this.imeiForwarded = imeiForwarded;
		this.forwarded = forwarded;
		this.callStatus = callStatus;
	}

	public Call(long id) {
		super(id);
	}

	/**
	 * @return the caller
	 */
	public String getCaller() {
		return caller;
	}




	/**
	 * @param caller the caller to set
	 */
	public void setCaller(String caller) {
		this.caller = caller;
	}




	/**
	 * @return the callee
	 */
	public String getCallee() {
		return callee;
	}




	/**
	 * @param callee the callee to set
	 */
	public void setCallee(String callee) {
		this.callee = callee;
	}




	/**
	 * @return the callerNickname
	 */
	public String getCallerNickname() {
		return callerNickname;
	}




	/**
	 * @param callerNickname the callerNickname to set
	 */
	public void setCallerNickname(String callerNickname) {
		this.callerNickname = callerNickname;
	}




	/**
	 * @return the calleeNickname
	 */
	public String getCalleeNickname() {
		return calleeNickname;
	}




	/**
	 * @param calleeNickname the calleeNickname to set
	 */
	public void setCalleeNickname(String calleeNickname) {
		this.calleeNickname = calleeNickname;
	}




	/**
	 * @return the callerUserAgent
	 */
	public String getCallerUserAgent() {
		return callerUserAgent;
	}




	/**
	 * @param callerUserAgent the callerUserAgent to set
	 */
	public void setCallerUserAgent(String callerUserAgent) {
		this.callerUserAgent = callerUserAgent;
	}




	/**
	 * @return the calleeUserAgent
	 */
	public String getCalleeUserAgent() {
		return calleeUserAgent;
	}




	/**
	 * @param calleeUserAgent the calleeUserAgent to set
	 */
	public void setCalleeUserAgent(String calleeUserAgent) {
		this.calleeUserAgent = calleeUserAgent;
	}




	/**
	 * @return the callerIp
	 */
	public String getCallerIp() {
		return callerIp;
	}




	/**
	 * @param callerIp the callerIp to set
	 */
	public void setCallerIp(String callerIp) {
		this.callerIp = callerIp;
	}




	/**
	 * @return the calleeIp
	 */
	public String getCalleeIp() {
		return calleeIp;
	}




	/**
	 * @param calleeIp the calleeIp to set
	 */
	public void setCalleeIp(String calleeIp) {
		this.calleeIp = calleeIp;
	}




	/**
	 * @return the callerPort
	 */
	public int getCallerPort() {
		return callerPort;
	}




	/**
	 * @param callerPort the callerPort to set
	 */
	public void setCallerPort(int callerPort) {
		this.callerPort = callerPort;
	}




	/**
	 * @return the calleePort
	 */
	public int getCalleePort() {
		return calleePort;
	}




	/**
	 * @param calleePort the calleePort to set
	 */
	public void setCalleePort(int calleePort) {
		this.calleePort = calleePort;
	}




	/**
	 * @return the startTime
	 */
	public Date getStartTime() {
		return startTime;
	}




	/**
	 * @param startTime the startTime to set
	 */
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}




	/**
	 * @return the endTime
	 */
	public Date getEndTime() {
		return endTime;
	}




	/**
	 * @param endTime the endTime to set
	 */
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}




	/**
	 * @return the audioEncoding
	 */
	public String getAudioEncoding() {
		return audioEncoding;
	}




	/**
	 * @param audioEncoding the audioEncoding to set
	 */
	public void setAudioEncoding(String audioEncoding) {
		this.audioEncoding = audioEncoding;
	}




	/**
	 * @return the videoEncoding
	 */
	public String getVideoEncoding() {
		return videoEncoding;
	}




	/**
	 * @param videoEncoding the videoEncoding to set
	 */
	public void setVideoEncoding(String videoEncoding) {
		this.videoEncoding = videoEncoding;
	}




	/**
	 * @return the callerE164
	 */
	public String getCallerE164() {
		return callerE164;
	}




	/**
	 * @param callerE164 the callerE164 to set
	 */
	public void setCallerE164(String callerE164) {
		this.callerE164 = callerE164;
	}




	/**
	 * @return the calleeE164
	 */
	public String getCalleeE164() {
		return calleeE164;
	}




	/**
	 * @param calleeE164 the calleeE164 to set
	 */
	public void setCalleeE164(String calleeE164) {
		this.calleeE164 = calleeE164;
	}




	/**
	 * @return the codec
	 */
	public String getCodec() {
		return codec;
	}




	/**
	 * @param codec the codec to set
	 */
	public void setCodec(String codec) {
		this.codec = codec;
	}




	/**
	 * @return the ringDuration
	 */
	public int getRingDuration() {
		return ringDuration;
	}




	/**
	 * @param ringDuration the ringDuration to set
	 */
	public void setRingDuration(int ringDuration) {
		this.ringDuration = ringDuration;
	}




	/**
	 * @return the conversationDuration
	 */
	public int getConversationDuration() {
		return conversationDuration;
	}




	/**
	 * @param conversationDuration the conversationDuration to set
	 */
	public void setConversationDuration(int conversationDuration) {
		this.conversationDuration = conversationDuration;
	}




	/**
	 * @return the liveListenPipe
	 */
	public String getLiveListenPipe() {
		return liveListenPipe;
	}




	/**
	 * @param liveListenPipe the liveListenPipe to set
	 */
	public void setLiveListenPipe(String liveListenPipe) {
		this.liveListenPipe = liveListenPipe;
	}




	/**
	 * @return the callInfo
	 */
	public String getCallInfo() {
		return callInfo;
	}




	/**
	 * @param callInfo the callInfo to set
	 */
	public void setCallInfo(String callInfo) {
		this.callInfo = callInfo;
	}




	/**
	 * @return the callLocations
	 */
	public String getCallLocations() {
		return callLocations;
	}




	/**
	 * @param callLocations the callLocations to set
	 */
	public void setCallLocations(String callLocations) {
		this.callLocations = callLocations;
	}




	/**
	 * @return the callParticipants
	 */
	public String getCallParticipants() {
		return callParticipants;
	}




	/**
	 * @param callParticipants the callParticipants to set
	 */
	public void setCallParticipants(String callParticipants) {
		this.callParticipants = callParticipants;
	}




	/**
	 * @return the callProducts
	 */
	public String getCallProducts() {
		return callProducts;
	}




	/**
	 * @param callProducts the callProducts to set
	 */
	public void setCallProducts(String callProducts) {
		this.callProducts = callProducts;
	}




	/**
	 * @return the callState
	 */
	public int getCallState() {
		return callState;
	}




	/**
	 * @param callState the callState to set
	 */
	public void setCallState(int callState) {
		this.callState = callState;
	}




	/**
	 * @return the mediaType
	 */
	public int getMediaType() {
		return mediaType;
	}




	/**
	 * @param mediaType the mediaType to set
	 */
	public void setMediaType(int mediaType) {
		this.mediaType = mediaType;
	}




	/**
	 * @return the mediaChunkID
	 */
	public int getMediaChunkID() {
		return mediaChunkID;
	}




	/**
	 * @param mediaChunkID the mediaChunkID to set
	 */
	public void setMediaChunkID(int mediaChunkID) {
		this.mediaChunkID = mediaChunkID;
	}




	/**
	 * @return the mediaChunkStreamID
	 */
	public int getMediaChunkStreamID() {
		return mediaChunkStreamID;
	}




	/**
	 * @param mediaChunkStreamID the mediaChunkStreamID to set
	 */
	public void setMediaChunkStreamID(int mediaChunkStreamID) {
		this.mediaChunkStreamID = mediaChunkStreamID;
	}




	/**
	 * @return the cseq
	 */
	public int getCseq() {
		return cseq;
	}




	/**
	 * @param cseq the cseq to set
	 */
	public void setCseq(int cseq) {
		this.cseq = cseq;
	}




	/**
	 * @return the dialed
	 */
	public String getDialed() {
		return dialed;
	}




	/**
	 * @param dialed the dialed to set
	 */
	public void setDialed(String dialed) {
		this.dialed = dialed;
	}




	/**
	 * @return the other
	 */
	public String getOther() {
		return other;
	}




	/**
	 * @param other the other to set
	 */
	public void setOther(String other) {
		this.other = other;
	}




	/**
	 * @return the connectTime
	 */
	public Date getConnectTime() {
		return connectTime;
	}




	/**
	 * @param connectTime the connectTime to set
	 */
	public void setConnectTime(Date connectTime) {
		this.connectTime = connectTime;
	}




	/**
	 * @return the disconnectTime
	 */
	public Date getDisconnectTime() {
		return disconnectTime;
	}




	/**
	 * @param disconnectTime the disconnectTime to set
	 */
	public void setDisconnectTime(Date disconnectTime) {
		this.disconnectTime = disconnectTime;
	}




	/**
	 * @return the callType
	 */
	public int getCallType() {
		return callType;
	}




	/**
	 * @param callType the callType to set
	 */
	public void setCallType(int callType) {
		this.callType = callType;
	}




	/**
	 * @return the serviceType
	 */
	public String getServiceType() {
		return serviceType;
	}




	/**
	 * @param serviceType the serviceType to set
	 */
	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}




	/**
	 * @return the conditionCode
	 */
	public int getConditionCode() {
		return conditionCode;
	}




	/**
	 * @param conditionCode the conditionCode to set
	 */
	public void setConditionCode(int conditionCode) {
		this.conditionCode = conditionCode;
	}




	/**
	 * @return the disconnectCode
	 */
	public int getDisconnectCode() {
		return disconnectCode;
	}




	/**
	 * @param disconnectCode the disconnectCode to set
	 */
	public void setDisconnectCode(int disconnectCode) {
		this.disconnectCode = disconnectCode;
	}




	/**
	 * @return the pin
	 */
	public String getPin() {
		return pin;
	}




	/**
	 * @param pin the pin to set
	 */
	public void setPin(String pin) {
		this.pin = pin;
	}




	/**
	 * @return the chargingID
	 */
	public String getChargingID() {
		return chargingID;
	}




	/**
	 * @param chargingID the chargingID to set
	 */
	public void setChargingID(String chargingID) {
		this.chargingID = chargingID;
	}




	/**
	 * @return the sgsnAddress
	 */
	public String getSgsnAddress() {
		return sgsnAddress;
	}




	/**
	 * @param sgsnAddress the sgsnAddress to set
	 */
	public void setSgsnAddress(String sgsnAddress) {
		this.sgsnAddress = sgsnAddress;
	}




	/**
	 * @return the ggsnAddress
	 */
	public String getGgsnAddress() {
		return ggsnAddress;
	}




	/**
	 * @param ggsnAddress the ggsnAddress to set
	 */
	public void setGgsnAddress(String ggsnAddress) {
		this.ggsnAddress = ggsnAddress;
	}




	/**
	 * @return the operatorID
	 */
	public String getOperatorID() {
		return operatorID;
	}




	/**
	 * @param operatorID the operatorID to set
	 */
	public void setOperatorID(String operatorID) {
		this.operatorID = operatorID;
	}




	/**
	 * @return the countryCallingCode
	 */
	public int getCountryCallingCode() {
		return countryCallingCode;
	}




	/**
	 * @param countryCallingCode the countryCallingCode to set
	 */
	public void setCountryCallingCode(int countryCallingCode) {
		this.countryCallingCode = countryCallingCode;
	}




	/**
	 * @return the timezone
	 */
	public String getTimezone() {
		return timezone;
	}




	/**
	 * @param timezone the timezone to set
	 */
	public void setTimezone(String timezone) {
		this.timezone = timezone;
	}




	/**
	 * @return the accessCode
	 */
	public String getAccessCode() {
		return accessCode;
	}




	/**
	 * @param accessCode the accessCode to set
	 */
	public void setAccessCode(String accessCode) {
		this.accessCode = accessCode;
	}




	/**
	 * @return the trunkID
	 */
	public String getTrunkID() {
		return trunkID;
	}




	/**
	 * @param trunkID the trunkID to set
	 */
	public void setTrunkID(String trunkID) {
		this.trunkID = trunkID;
	}




	/**
	 * @return the incomingCktID
	 */
	public String getIncomingCktID() {
		return incomingCktID;
	}




	/**
	 * @param incomingCktID the incomingCktID to set
	 */
	public void setIncomingCktID(String incomingCktID) {
		this.incomingCktID = incomingCktID;
	}




	/**
	 * @return the outgoingCktID
	 */
	public String getOutgoingCktID() {
		return outgoingCktID;
	}




	/**
	 * @param outgoingCktID the outgoingCktID to set
	 */
	public void setOutgoingCktID(String outgoingCktID) {
		this.outgoingCktID = outgoingCktID;
	}




	/**
	 * @return the routingArea
	 */
	public String getRoutingArea() {
		return routingArea;
	}




	/**
	 * @param routingArea the routingArea to set
	 */
	public void setRoutingArea(String routingArea) {
		this.routingArea = routingArea;
	}




	/**
	 * @return the locationArea
	 */
	public String getLocationArea() {
		return locationArea;
	}




	/**
	 * @param locationArea the locationArea to set
	 */
	public void setLocationArea(String locationArea) {
		this.locationArea = locationArea;
	}




	/**
	 * @return the cellIdentifier
	 */
	public String getCellIdentifier() {
		return cellIdentifier;
	}




	/**
	 * @param cellIdentifier the cellIdentifier to set
	 */
	public void setCellIdentifier(String cellIdentifier) {
		this.cellIdentifier = cellIdentifier;
	}




	/**
	 * @return the finalPartyNumber
	 */
	public String getFinalPartyNumber() {
		return finalPartyNumber;
	}




	/**
	 * @param finalPartyNumber the finalPartyNumber to set
	 */
	public void setFinalPartyNumber(String finalPartyNumber) {
		this.finalPartyNumber = finalPartyNumber;
	}




	/**
	 * @return the accountCode
	 */
	public String getAccountCode() {
		return accountCode;
	}




	/**
	 * @param accountCode the accountCode to set
	 */
	public void setAccountCode(String accountCode) {
		this.accountCode = accountCode;
	}




	/**
	 * @return the msisdn
	 */
	public String getMsisdn() {
		return msisdn;
	}




	/**
	 * @param msisdn the msisdn to set
	 */
	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}




	/**
	 * @return the imei
	 */
	public String getImei() {
		return imei;
	}




	/**
	 * @param imei the imei to set
	 */
	public void setImei(String imei) {
		this.imei = imei;
	}




	/**
	 * @return the imsi
	 */
	public String getImsi() {
		return imsi;
	}




	/**
	 * @param imsi the imsi to set
	 */
	public void setImsi(String imsi) {
		this.imsi = imsi;
	}




	/**
	 * @return the teluri
	 */
	public String getTeluri() {
		return teluri;
	}




	/**
	 * @param teluri the teluri to set
	 */
	public void setTeluri(String teluri) {
		this.teluri = teluri;
	}




	/**
	 * @return the sipurl
	 */
	public String getSipurl() {
		return sipurl;
	}




	/**
	 * @param sipurl the sipurl to set
	 */
	public void setSipurl(String sipurl) {
		this.sipurl = sipurl;
	}




	/**
	 * @return the teluriCallee
	 */
	public String getTeluriCallee() {
		return teluriCallee;
	}




	/**
	 * @param teluriCallee the teluriCallee to set
	 */
	public void setTeluriCallee(String teluriCallee) {
		this.teluriCallee = teluriCallee;
	}




	/**
	 * @return the sipurlCallee
	 */
	public String getSipurlCallee() {
		return sipurlCallee;
	}




	/**
	 * @param sipurlCallee the sipurlCallee to set
	 */
	public void setSipurlCallee(String sipurlCallee) {
		this.sipurlCallee = sipurlCallee;
	}




	/**
	 * @return the teluriForwarded
	 */
	public String getTeluriForwarded() {
		return teluriForwarded;
	}




	/**
	 * @param teluriForwarded the teluriForwarded to set
	 */
	public void setTeluriForwarded(String teluriForwarded) {
		this.teluriForwarded = teluriForwarded;
	}




	/**
	 * @return the sipurlForwarded
	 */
	public String getSipurlForwarded() {
		return sipurlForwarded;
	}




	/**
	 * @param sipurlForwarded the sipurlForwarded to set
	 */
	public void setSipurlForwarded(String sipurlForwarded) {
		this.sipurlForwarded = sipurlForwarded;
	}




	/**
	 * @return the msisdnCallee
	 */
	public String getMsisdnCallee() {
		return msisdnCallee;
	}




	/**
	 * @param msisdnCallee the msisdnCallee to set
	 */
	public void setMsisdnCallee(String msisdnCallee) {
		this.msisdnCallee = msisdnCallee;
	}




	/**
	 * @return the imsiCallee
	 */
	public String getImsiCallee() {
		return imsiCallee;
	}




	/**
	 * @param imsiCallee the imsiCallee to set
	 */
	public void setImsiCallee(String imsiCallee) {
		this.imsiCallee = imsiCallee;
	}




	/**
	 * @return the imeiCallee
	 */
	public String getImeiCallee() {
		return imeiCallee;
	}




	/**
	 * @param imeiCallee the imeiCallee to set
	 */
	public void setImeiCallee(String imeiCallee) {
		this.imeiCallee = imeiCallee;
	}




	/**
	 * @return the msisdnForwarded
	 */
	public String getMsisdnForwarded() {
		return msisdnForwarded;
	}




	/**
	 * @param msisdnForwarded the msisdnForwarded to set
	 */
	public void setMsisdnForwarded(String msisdnForwarded) {
		this.msisdnForwarded = msisdnForwarded;
	}




	/**
	 * @return the imsiForwarded
	 */
	public String getImsiForwarded() {
		return imsiForwarded;
	}




	/**
	 * @param imsiForwarded the imsiForwarded to set
	 */
	public void setImsiForwarded(String imsiForwarded) {
		this.imsiForwarded = imsiForwarded;
	}




	/**
	 * @return the imeiForwarded
	 */
	public String getImeiForwarded() {
		return imeiForwarded;
	}




	/**
	 * @param imeiForwarded the imeiForwarded to set
	 */
	public void setImeiForwarded(String imeiForwarded) {
		this.imeiForwarded = imeiForwarded;
	}




	/**
	 * @return the forwarded
	 */
	public String getForwarded() {
		return forwarded;
	}




	/**
	 * @param forwarded the forwarded to set
	 */
	public void setForwarded(String forwarded) {
		this.forwarded = forwarded;
	}




	/**
	 * @return the callStatus
	 */
	public int getCallStatus() {
		return callStatus;
	}




	/**
	 * @param callStatus the callStatus to set
	 */
	public void setCallStatus(int callStatus) {
		this.callStatus = callStatus;
	}
}
