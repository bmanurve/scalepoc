package com.ss8.experiments;

import com.ss8.experiments.indexer.QueueLoader;

public class SolrLoadGenerator {
	public static void main(String[] args) {
		try {
			QueueLoader loader = new QueueLoader(true);
			loader.startIngest();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
